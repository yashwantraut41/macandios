//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
using namespace vmath;


enum
{
    AMC_ATRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

     GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

     GLuint vaoVerticalLines;
    GLuint vboVerticalLinesPosition;
    GLuint vboVerticalLinesColor;


    GLuint vaoHorizontalLines;
    GLuint vboHorizontalLinesPosition;
    GLuint vboHorizontalLinesColor;

    GLuint vaoRedHorizontalLine;
    GLuint vboRedHorizontalLinePosition;
    GLuint vboRedHorizontalLineColor;

    GLuint vaoGreenVerticalLine;
    GLuint vboGreenVerticalLinePosition;
    GLuint vboGreenVerticalLineColor;

    GLuint mvpUniform;

    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
		void initGraphShadersAndVaoVbos();
    	[self initGraphShadersAndVaoVbos];

    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
   
    
        //clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    



    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}

-(void) initGraphShadersAndVaoVbos
{
     //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
   "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_color=vColor;" \
		"}";
    glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
	"precision highp float;" \
    "in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor=out_color;" \
		"}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
   glBindAttribLocation(gShaderProgramObject, AMC_ATRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
    mvpUniform=glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    
    //vertices ,colors, shader attribs, vbo, vao initializations
    
    const GLfloat redHorizontalLine[]=
	{
		-1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f
	};

	const GLfloat greenVerticalLine[]=
	{
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};

	const GLfloat redHorizontalLineColor[] =
	{
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f

	};

	const GLfloat greenVerticalLineColor[] =
	{
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f
	};

    const GLfloat blueColor[] =
	{
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f
	};

	
   //create vao RED LINE X AXIS
	glGenVertexArrays(1, &vaoRedHorizontalLine);
	glBindVertexArray(vaoRedHorizontalLine);

	glGenBuffers(1, &vboRedHorizontalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboRedHorizontalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(redHorizontalLine), redHorizontalLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//create vbo for color 
	glGenBuffers(1, &vboRedHorizontalLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboRedHorizontalLineColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(redHorizontalLineColor), redHorizontalLineColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);



	//create vao GREEN LINE Y AXIS
	glGenVertexArrays(1, &vaoGreenVerticalLine);
	glBindVertexArray(vaoGreenVerticalLine);

	glGenBuffers(1, &vboGreenVerticalLinePosition);  
	glBindBuffer(GL_ARRAY_BUFFER, vboGreenVerticalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(greenVerticalLine), greenVerticalLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//create vbo for  color 
	glGenBuffers(1, &vboGreenVerticalLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboGreenVerticalLineColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(greenVerticalLineColor), greenVerticalLineColor, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao vertical lines
	glGenVertexArrays(1, &vaoVerticalLines);
	glBindVertexArray(vaoVerticalLines);

	glGenBuffers(1, &vboVerticalLinesPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboVerticalLinesPosition);

	glBufferData(GL_ARRAY_BUFFER, 3*2*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboVerticalLinesColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboVerticalLinesColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(blueColor),blueColor , GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao horizontal lines
	glGenVertexArrays(1, &vaoHorizontalLines);
	glBindVertexArray(vaoHorizontalLines);

	glGenBuffers(1, &vboHorizontalLinesPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLinesPosition);
	glBufferData(GL_ARRAY_BUFFER, 3*2*sizeof(GLfloat),NULL , GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &vboHorizontalLinesColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLinesColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(blueColor), blueColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
    glBindVertexArray(0);
   
   
}



/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    void draw();

    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
     [self draw];
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
       
}
-(void) draw
{

    //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    //declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	

	//blue vertical lines
 	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -1.5f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	GLfloat X = -1.0f;
	//bind with vao
	for (int i = 0; i < 41; i++)
	{
		GLfloat verticalline[] =
		{
			X,1.0f,0.0f,
			X,-1.0f,0.0f
		};
		glBindVertexArray(vaoVerticalLines);
		glBindBuffer(GL_ARRAY_BUFFER, vboVerticalLinesPosition);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verticalline), verticalline, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
		X = X + 0.05f;

	}


	//blue horizontal line
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -1.5f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	
	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	GLfloat Y = -1.0f;
	//bind with vao
	for (int i = 0; i < 40; i++)
	{
		GLfloat horizontaline[] =
		{
			-1.0f,Y,0.0f,
			1.0f,Y,0.0f
		};
		glBindVertexArray(vaoHorizontalLines);
		glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLinesPosition);
		glBufferData(GL_ARRAY_BUFFER, sizeof(horizontaline), horizontaline, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
		Y = Y + 0.05f;
	}
	
    //red x axis line
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -1.5f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRedHorizontalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glLineWidth(1.0f);
	glBindVertexArray(0);

	//green y axis line
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -1.5f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoGreenVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glLineWidth(1.0f);
	glBindVertexArray(0);


    //stop using openGL pROGRAM object
    glUseProgram(0);

}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

     if (vboRedHorizontalLinePosition)
	{
		glDeleteBuffers(1, &vboRedHorizontalLinePosition);
		vboRedHorizontalLinePosition = 0;
	}
	if (vboRedHorizontalLineColor)
	{
		glDeleteBuffers(1, &vboRedHorizontalLineColor);
		vboRedHorizontalLineColor = 0;
	}

	
	if (vaoRedHorizontalLine)
	{
		glDeleteVertexArrays(1, &vaoRedHorizontalLine);
		vaoRedHorizontalLine = 0;
	}

	if (vboGreenVerticalLineColor)
	{
		glDeleteBuffers(1, &vboGreenVerticalLineColor);
		vboGreenVerticalLineColor = 0;
	}
	if (vboGreenVerticalLinePosition)
	{
		glDeleteBuffers(1, &vboGreenVerticalLinePosition);
		vboGreenVerticalLinePosition = 0;
	}


	if (vaoGreenVerticalLine)
	{
		glDeleteVertexArrays(1, &vaoGreenVerticalLine);
		vaoGreenVerticalLine = 0;
	}



	if (vboVerticalLinesPosition)
	{
		glDeleteBuffers(1, &vboVerticalLinesPosition);
		vboVerticalLinesPosition = 0;
	}


	if (vaoVerticalLines)
	{
		glDeleteVertexArrays(1, &vaoVerticalLines);
		vaoVerticalLines = 0;
	}

	if (vboHorizontalLinesPosition)
	{
		glDeleteBuffers(1, &vboHorizontalLinesPosition);
		vboHorizontalLinesPosition = 0;
	}


	if (vaoHorizontalLines)
	{
		glDeleteVertexArrays(1, &vaoHorizontalLines);
		vaoHorizontalLines = 0;
	}

    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
