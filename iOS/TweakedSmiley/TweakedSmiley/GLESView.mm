//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

     GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vaoRectangle;
    GLuint vboRectanglePosition;
    GLuint vboRectangleTexcoord;
    GLuint mvpUniform;
    GLuint samplerUniform;
    //for texture
    GLuint texture_smiley;

    int pressDigit;

   

    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
      "in vec4 vPosition;" \
    "in vec2 vTexCoord;" \
    "out vec2 out_TexCoord;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_TexCoord=vTexCoord;" \
    "}";
    glShaderSource(vertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
    "precision highp float;" \
    "in vec2 out_TexCoord;" \
    "uniform sampler2D u_texture_sampler;"\
    "out vec4 FragColor;"\
    "void main(void)" \
    "{" \
        "vec3 tex=vec3(texture(u_texture_sampler,out_TexCoord));" \
        "FragColor=vec4(tex,1.0);" \
    "}";
    //provide fragment shader source code to fragement shader
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    shaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");

    //link shader
    glLinkProgram(shaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
    mvpUniform=glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    samplerUniform=glGetUniformLocation(shaderProgramObject, "u_texture_sampler");

    texture_smiley=[self loadTextureFromBMPFile:@"Smiley" :@"bmp"];

    //vertices ,colors, shader attribs, vbo, vao initializations
   const GLfloat rectangleVertices[] =
	{
		1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
        1.0f,-1.0f,0.0f
	};

	

	
    glGenVertexArrays(1, &vaoRectangle);
	glBindVertexArray(vaoRectangle);

	//create buffer for rectangle position
	glGenBuffers(1, &vboRectanglePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboRectanglePosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//create vbo buffer for Rectangle TEXTURE
	glGenBuffers(1, &vboRectangleTexcoord);
	glBindBuffer(GL_ARRAY_BUFFER, vboRectangleTexcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*8, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	//unbind vbo buffer of rectangle TEXTURE
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//unbind pyramid vao
	glBindVertexArray(0);

    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
   
    
        //clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    


    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}
-(GLuint)loadTextureFromBMPFile:(NSString*)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];

    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];

    if(!bmpImage)
    {
        NSLog(@"Cant find %@",textureFileNameWithPath);
        return(0);
    }

    CGImageRef cgImage=bmpImage.CGImage;
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    CFDataRef imageData= CGDataProviderCopyData((CGImageGetDataProvider(cgImage)));
    void* pixels=(void*)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1,&bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bmpTexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);

    glGenerateMipmap(GL_TEXTURE_2D);
    CFRelease(imageData);
    return(bmpTexture);             

}


/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
       
    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix=vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix=vmath::mat4::identity();
    vmath::mat4 translationMatrix=vmath::mat4::identity();
    

    translationMatrix=vmath::translate(0.0f,0.0f,-6.0f);

    modelViewMatrix= modelViewMatrix * translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind vao
    glBindTexture(GL_TEXTURE_2D,texture_smiley);

    glBindVertexArray(vaoRectangle);
    if(pressDigit == 1)
	{
		const GLfloat rectangleTexCoord[] =
		{
			0.5f,0.5f,
			0.0f,0.5f,
			0.0f,0.0f,
			0.5f,0.0
		}; 

		glBindBuffer(GL_ARRAY_BUFFER, vboRectangleTexcoord);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTexCoord), rectangleTexCoord, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (pressDigit == 2)
	{
		const GLfloat rectangleTexCoord[] =
		{
			1.0f,1.0f,
			0.0f,1.0,
			0.0f,0.0f,
			1.0f,0.0f
		};
		glBindBuffer(GL_ARRAY_BUFFER, vboRectangleTexcoord);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTexCoord), rectangleTexCoord, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
	else if (pressDigit == 3)
	{
		const GLfloat rectangleTexCoord[] =
		{
			2.0f,2.0f,
			0.0f,2.0f,
			0.0f,0.0f,
			2.0f,0.0
		};
		glBindBuffer(GL_ARRAY_BUFFER, vboRectangleTexcoord);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTexCoord), rectangleTexCoord, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if(pressDigit==4)
	{
		const GLfloat rectangleTexCoord[] =
		{
			0.5f,0.5f,
			0.5f,0.5f,
			0.5f,0.5f,
			0.5f,0.5f
		};
		glBindBuffer(GL_ARRAY_BUFFER, vboRectangleTexcoord);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTexCoord), rectangleTexCoord, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

    //draw eith by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    //UNBIND VAO
    glBindVertexArray(0);

    //stop using openGL pROGRAM object
    glUseProgram(0);
   
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
       
}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
    pressDigit+=1;

    if(pressDigit>4)
        pressDigit=1;

}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

    if(vaoRectangle)
    {
        glDeleteVertexArrays(1, &vaoRectangle);
        vaoRectangle=0;
    }

    if(vboRectangleTexcoord)
    {
        glDeleteBuffers(1, &vboRectangleTexcoord);
        vboRectangleTexcoord=0;
    }
    
    if(vboRectanglePosition)
    {
        glDeleteBuffers(1,&vboRectanglePosition);
        vboRectanglePosition=0;
    }

    


     //detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
