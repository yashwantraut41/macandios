//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};


@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao;
    GLuint vbo;

    GLuint vaoRGBSquare;
    GLuint vboRGBSquare;

    GLuint vaoLine;
    GLuint vboLine;

    GLuint vaoGrid;
    GLuint vboGrid;

    GLuint vaoGridDiagonals;
    GLuint vboGridDiagonals;

    GLuint vaoSquareDiagonals;
    GLuint vboSquareDiagonals;
    GLuint mvpUniform;

    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
	"out vec4 outColor;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "outColor = vColor;" \

    "}";
    glShaderSource(vertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
    "precision highp float;" \
    "in vec4 outColor;" \
    "out vec4 FragColor;"\
    "void main(void)" \
    "{" \
        "FragColor = outColor;" \
    "}";
    //provide fragment shader source code to fragement shader
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    shaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
    
    //link shader
    glLinkProgram(shaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
    mvpUniform=glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    
    //vertices ,colors, shader attribs, vbo, vao initializations
     //different shapes
	//First Shape we creating vao vbo for is following figure
	/*  . . . . 
		. . . . 
	    . . . . 
	    . . . . */
	GLfloat dotVertices[]=
	{
		//first column
		-0.4f, 0.3f,0.0f,
		-0.4f, 0.1f,0.0f,
		- 0.4f, -0.1f,0.0f,
		- 0.4f, -0.3f,0.0f,

		//second column
		-0.2f, 0.3f,0.0f,
		-0.2f, 0.1f,0.0f,
		-0.2f, -0.1f,0.0f,
		-0.2f, -0.3f,0.0f,

		//third column
		0.0f, 0.3f,0.0f,
		0.0f, 0.1f,0.0f,
		0.0f, -0.1f,0.0f,
		0.0f, -0.3f,0.0f,

		//fourth column
		0.2f, 0.3f,0.0f,
		0.2f, 0.1f,0.0f,
		0.2f, -0.1f,0.0f,
		0.2f, -0.3f,0.0f,
	};
	 

	//create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(dotVertices) , dotVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//second Shape 
	//square within square with some diagonal line
	GLfloat meshShapeTwo[] =
	{
		//HORIZONTAL LINES
		-0.3f, 0.3f,0.0f,
		0.3f, 0.3f,0.0f,

		-0.3f, 0.1f,0.0f,
		0.3f, 0.1f,0.0f,
		
		-0.3f,-0.1f,0.0f,
		0.3f, -0.1f,0.0f,
		
		//VERTICAL LINES
		-0.3f, 0.3f,0.0f,
		-0.3f, -0.3f,0.0f,
		
		-0.1f, 0.3f,0.0f,
		-0.1f, -0.3f,0.0f,
		
		0.1f, 0.3f,0.0f,
		0.1f, -0.3f,0.0f,

		//diagonal line
		0.3f, 0.3f,0.0f,
		-0.3f, -0.3f,0.0f,

		0.1f, 0.3f,0.0f,
		-0.3f, -0.1f,0.0f,

		- 0.1f, 0.3f,0.0f,
		- 0.3f, 0.1f,0.0f,

		0.3f, 0.1f,0.0f,
		- 0.1f, -0.3f,0.0f,

		0.3f, -0.1f,0.0f,
		0.1f, -0.3f,0.0f,
	};
	glGenVertexArrays(1, &vaoLine);
	glBindVertexArray(vaoLine);
	glGenBuffers(1, &vboLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboLine);
	glBufferData(GL_ARRAY_BUFFER, sizeof(meshShapeTwo), meshShapeTwo, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//Third Shape 
	//square Grid
	GLfloat squareGrid[] =
	{
		//HORIZONTAL LINES
		-0.3f, 0.3f,0.0f,
		0.3f, 0.3f,0.0f,

		-0.3f, 0.1f,0.0f,
		0.3f, 0.1f,0.0f,

		-0.3f,-0.1f,0.0f,
		0.3f, -0.1f,0.0f,

		-0.3f,-0.3f,0.0f,
		0.3f, -0.3f,0.0f,

		//VERTICAL LINES
		-0.3f, 0.3f,0.0f,
		-0.3f, -0.3f,0.0f,

		-0.1f, 0.3f,0.0f,
		-0.1f, -0.3f,0.0f,

		0.1f, 0.3f,0.0f,
		0.1f, -0.3f,0.0f,

		0.3f, 0.3f,0.0f,
		0.3f, -0.3f,0.0f,

		
	};
	glGenVertexArrays(1, &vaoGrid);
	glBindVertexArray(vaoGrid);
	glGenBuffers(1, &vboGrid);
	glBindBuffer(GL_ARRAY_BUFFER, vboGrid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareGrid), squareGrid, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//fourth shape square within squares with diagonal line
	GLfloat squareGridDiagonals[] =
	{
		//HORIZONTAL LINES
		-0.3f, 0.3f,0.0f,
		0.3f, 0.3f,0.0f,

		-0.3f, 0.1f,0.0f,
		0.3f, 0.1f,0.0f,

		-0.3f,-0.1f,0.0f,
		0.3f, -0.1f,0.0f,

		-0.3f,-0.3f,0.0f,
		0.3f, -0.3f,0.0f,

		//VERTICAL LINES
		-0.3f, 0.3f,0.0f,
		-0.3f, -0.3f,0.0f,

		-0.1f, 0.3f,0.0f,
		-0.1f, -0.3f,0.0f,

		0.1f, 0.3f,0.0f,
		0.1f, -0.3f,0.0f,

		0.3f, 0.3f,0.0f,
		0.3f, -0.3f,0.0f,

		//diagonal line
		0.3f, 0.3f,0.0f,
		-0.3f, -0.3f,0.0f,

		0.1f, 0.3f,0.0f,
		-0.3f, -0.1f,0.0f,

		-0.1f, 0.3f,0.0f,
		-0.3f, 0.1f,0.0f,

		0.3f, 0.1f,0.0f,
		-0.1f, -0.3f,0.0f,

		0.3f, -0.1f,0.0f,
		0.1f, -0.3f,0.0f,

	};
	glGenVertexArrays(1, &vaoGridDiagonals);
	glBindVertexArray(vaoGridDiagonals);
	glGenBuffers(1, &vboGridDiagonals);
	glBindBuffer(GL_ARRAY_BUFFER, vboGridDiagonals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareGridDiagonals), squareGridDiagonals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	//fifth shape //left to right diagonal lines
	
	//fourth shape square within squares with diagonal line
	GLfloat  sqx1 = 0.3f;
	GLfloat  sqy1 = -0.3f;
	GLfloat squareDiagonals[] =
	{
		//HORIZONTAL LINES
		-0.3f, 0.3f,0.0f,
		0.3f, 0.3f,0.0f,

		-0.3f,-0.3f,0.0f,
		0.3f, -0.3f,0.0f,

		//VERTICAL LINES
		-0.3f, 0.3f,0.0f,
		-0.3f, -0.3f,0.0f,

		0.3f, 0.3f,0.0f,
		0.3f, -0.3f,0.0f,

		//diagonals line
			-0.3f, 0.3f,0.0f,
			 sqx1, sqy1,0.0f,

			 -0.3f, 0.3f,0.0f,
			 sqx1, -0.1f,0.0f,

			 -0.3f, 0.3f,0.0f,
			 sqx1, 0.1f,0.0f,

			 -0.3f, 0.3f,0.0f,
			 0.1f, sqy1,0.0f,

			 -0.3f, 0.3f,0.0f,
			 -0.1f, sqy1,0.0f,
		 
	};
	glGenVertexArrays(1, &vaoSquareDiagonals);
	glBindVertexArray(vaoSquareDiagonals);
	glGenBuffers(1, &vboSquareDiagonals);
	glBindBuffer(GL_ARRAY_BUFFER, vboSquareDiagonals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareDiagonals), squareDiagonals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);
	glBindVertexArray(0);


	//RGB SQUARE
	GLfloat rgbSquare[] =
	{	
		//position					//color
		-0.1f, 0.3f,0.0f,        1.0f,0.0f,0.0f,
		-0.3f, 0.3f,0.0f,		 1.0f,0.0f,0.0f,
		-0.3f, -0.3f,0.0f,		 1.0f,0.0f,0.0f,
		-0.1f, -0.3f,0.0f,		 1.0f,0.0f,0.0f,

		 0.1f, 0.3f,0.0f,		0.0f,1.0f,0.0f,
		-0.1f, 0.3f,0.0f,		0.0f,1.0f,0.0f,
		-0.1f,-0.3f,0.0f,		0.0f,1.0f,0.0f,
		 0.1f,-0.3f,0.0f,	    0.0f,1.0f,0.0f,
		
		
		0.3f,  0.3f,0.0f,    0.0f,0.0f,1.0f,
		0.1f, 0.3f,0.0f,	0.0f,0.0f,1.0f,
		0.1f, -0.3f,0.0f,	0.0f,0.0f,1.0f,
		0.3f, -0.3f,0.0f,	0.0f,0.0f,1.0f,
		
		 
		 
	};



	glGenVertexArrays(1, &vaoRGBSquare);
	glBindVertexArray(vaoRGBSquare);
	glGenBuffers(1, &vboRGBSquare);
	glBindBuffer(GL_ARRAY_BUFFER, vboRGBSquare);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rgbSquare), rgbSquare, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 6, (void*)0);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 6, (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
   
    
        //clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    


    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}



/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
     //declaration of matrices
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;

    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(-1.0f, 0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	
	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, 16);
	glBindVertexArray(0);
	
	//second shape //square within square with some diagonal line
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(0.0f, 0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLine);
	glDrawArrays(GL_LINES, 0, 22);
	glBindVertexArray(0);
	
	
	/*third shape
	square grid */
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(1.0f, 0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoGrid);
	glDrawArrays(GL_LINES, 0, 16);
	glBindVertexArray(0);
	
	
	/*fourth shape */
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(-1.2f, -0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoGridDiagonals);
	glDrawArrays(GL_LINES, 0, 26);
	glBindVertexArray(0);
	
	

	//fifth shape 	//left to right diagonal lines

    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(0.0f, -0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoSquareDiagonals);
	glDrawArrays(GL_LINES, 0, 18);
	glBindVertexArray(0);

	//SIXTH SHAPE  RGB SQUARE
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(1.0f, -0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRGBSquare);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glBindVertexArray(0);
	
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(1.0f, -0.6f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoGrid);
	glDrawArrays(GL_LINES, 0, 16);
	glBindVertexArray(0);
	
    
    //stop using openGL pROGRAM object
    glUseProgram(0);

    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,(fwidth/fheight),0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

     if(vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao=0;
    }
    
    if(vbo)
    {
        glDeleteBuffers(1, &vbo);
        vbo=0;
    }


     //detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
