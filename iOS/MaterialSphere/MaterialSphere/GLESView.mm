//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
#import "createSphere.h"
using namespace vmath;


enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};


   GLfloat lightAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
    GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0,1.0f };
    GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightPosition[4] = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat lightAngleZero ;
    GLfloat lightAngleOne ;
    GLfloat lightAngleTwo ;
    GLuint keypress;
@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vaoSphere;
    GLuint vboSpherePosition;
    GLuint vboSphereNormals;
    GLuint vboSphereTexture;
    GLuint vboSphereIndices;
   
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;
    //LIGHT Uniforms
    GLuint lightDiffuseUniform;
    GLuint lightAmbientUniform;
    GLuint lightSpecularUniform;
    GLuint lightPositionUniform;
   //Material Uniform
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;
   
    GLuint lightRotationaMatrixUniform;
    GLuint lKeyPressUniform;
   
    struct sMaterial
    {
	    GLfloat MaterialAmbient[4];
    	GLfloat MaterialDiffuse[4];
	    GLfloat MaterialSpecular[4];
	    GLfloat MaterialShininess;
    	GLfloat TranslateValues[3];
    }sphereMaterial[24];



    bool bLight;
    mat4 orthographicProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
     void materialInit();
    void init24Sphere();
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
         [self init24Sphere];
         [self materialInit];
              createSphere();

        glGenVertexArrays(1, &vaoSphere);
        glBindVertexArray(vaoSphere);
    
         glGenBuffers(1, &vboSpherePosition);
         glBindBuffer(GL_ARRAY_BUFFER, vboSpherePosition);
         glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);
         glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
         glBindBuffer(GL_ARRAY_BUFFER, 0);

         glGenBuffers(1, &vboSphereNormals);
         glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormals);
         glBufferData(GL_ARRAY_BUFFER, sizeof(model_normals), model_normals, GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
         glBindBuffer(GL_ARRAY_BUFFER, 0);

         glGenBuffers(1, &vboSphereTexture);
         glBindBuffer(GL_ARRAY_BUFFER, vboSphereTexture);
         glBufferData(GL_ARRAY_BUFFER, sizeof(model_textures), model_textures, GL_STATIC_DRAW);
         glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
         glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
         glBindBuffer(GL_ARRAY_BUFFER, 0);

         glGenBuffers(1,&vboSphereIndices);
         glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboSphereIndices);
         glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(model_elements),model_elements,GL_STATIC_DRAW);
    
         glBindVertexArray(0);

    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
   
    
        //clear color
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        
    bLight=false;
      lightAngleZero = 0.0f;
     lightAngleOne = 0.0f;
     lightAngleTwo = 0.0f;
     keypress=0;

     //set projection matrix to identity matrix
    orthographicProjectionMatrix=vmath::mat4::identity();
  
    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}

-(void) init24Sphere
{
     //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
    "in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform mat4 u_rotational_matrix ;" \
		"uniform vec4 u_light_position;" \
		"uniform mediump int lKeyPress;" \
		"out vec3 lightdirection;" \
		"out vec3 tnorm;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(lKeyPress==1)" \
		"{" \
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition * u_rotational_matrix  ;" \
		"tnorm = mat3(u_view_matrix * u_model_matrix) * vNormal;"\
		"lightdirection=vec3(u_light_position - eye_coordinates );" \
		"viewer_vector=vec3(-eye_coordinates.xyz);" \
		"}" \
		"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;" \
		"}";

	
   glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
    "precision highp float;"\
    "in vec3 lightdirection;" \
		"in vec3 tnorm;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_lightambient;" \
		"uniform vec3 u_lightdiffuse;" \
		"uniform vec3 u_lightspecular;" \
		"uniform vec3 u_materialambient;" \
		"uniform vec3 u_materialdiffuse;" \
		"uniform vec3 u_materialspecular;" \
		"uniform float materialshinines;" \
		"uniform int lKeyPress;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ADS_light;" \
		"if(lKeyPress==1)" \
		"{" \
		"vec3 normalizetnorm=normalize(tnorm);" \
		"vec3 normalizelightdirection=normalize(lightdirection);" \
		"vec3 normalizeviewervector=normalize(viewer_vector);" \
		"float tn_dot_ld=max(dot(normalizelightdirection,normalizetnorm),0.0);" \
		"vec3 reflection_vector=reflect(-normalizelightdirection,normalizetnorm);" \
		"vec3 ambient=u_lightambient*u_materialambient;" \
		"vec3 diffuse=u_lightdiffuse*u_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_lightspecular*u_materialspecular*pow(max(dot(reflection_vector,normalizeviewervector),0.0),materialshinines);" \
		"phong_ADS_light=ambient+diffuse+specular;" \
		"}"	\
		"else" \
		"{" \
		"phong_ADS_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"FragColor=vec4(phong_ADS_light,1.0);" \
		"}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
   glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
   modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	lightRotationaMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_rotational_matrix");

	lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightambient");
	lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightdiffuse");
	lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightspecular");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialspecular");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "materialshinines");

	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "lKeyPress");


   
}
-(void) materialInit
{

	//emerald 1
	sphereMaterial[0].MaterialAmbient[0] = 0.0215f;
	sphereMaterial[0].MaterialAmbient[1] = 0.1745f;
	sphereMaterial[0].MaterialAmbient[2] = 0.0215f;
	sphereMaterial[0].MaterialAmbient[3] = 1.0f;

	sphereMaterial[0].MaterialDiffuse[0] = 0.07568f;
	sphereMaterial[0].MaterialDiffuse[1] = 0.61424f;
	sphereMaterial[0].MaterialDiffuse[2] = 0.07568f;
	sphereMaterial[0].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[0].MaterialSpecular[0] = 0.633f;
	sphereMaterial[0].MaterialSpecular[1] = 0.727811f;
	sphereMaterial[0].MaterialSpecular[2] = 0.633f;
	sphereMaterial[0].MaterialSpecular[3] = 1.0f;

	sphereMaterial[0].MaterialShininess = 0.6 * 128.0f;
		
		sphereMaterial[0].TranslateValues[0] = 1.5f;
		sphereMaterial[0].TranslateValues[1] = 14.0f;
		sphereMaterial[0].TranslateValues[2] = 0.0f;
	

	//jade 2
	sphereMaterial[1].MaterialAmbient[0] = 0.135f;
	sphereMaterial[1].MaterialAmbient[1] = 0.2225f;
	sphereMaterial[1].MaterialAmbient[2] = 0.1575f;
	sphereMaterial[1].MaterialAmbient[3] = 1.0f;

	sphereMaterial[1].MaterialDiffuse[0] = 0.54f;
	sphereMaterial[1].MaterialDiffuse[1] = 0.89f;
	sphereMaterial[1].MaterialDiffuse[2] = 0.63f;
	sphereMaterial[1].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[1].MaterialSpecular[0] = 0.316228f;
	sphereMaterial[1].MaterialSpecular[1] = 0.316228f;
	sphereMaterial[1].MaterialSpecular[2] = 0.316228f;
	sphereMaterial[1].MaterialSpecular[3] = 1.0f;

	sphereMaterial[1].MaterialShininess = 0.1 * 128.0f;
	
		
		sphereMaterial[1].TranslateValues[0] = 1.5f;
		sphereMaterial[1].TranslateValues[1] = 11.5f;
		sphereMaterial[1].TranslateValues[2] = 0.0f;


	//obsidian 3
	sphereMaterial[2].MaterialAmbient[0] = 0.05375f;
	sphereMaterial[2].MaterialAmbient[1] = 0.05f;
	sphereMaterial[2].MaterialAmbient[2] = 0.06625f;
	sphereMaterial[2].MaterialAmbient[3] = 1.0f;

	sphereMaterial[2].MaterialDiffuse[0] = 0.18275f;
	sphereMaterial[2].MaterialDiffuse[1] = 0.17f;
	sphereMaterial[2].MaterialDiffuse[2] = 0.22525f;
	sphereMaterial[2].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[2].MaterialSpecular[0] = 0.332741f;
	sphereMaterial[2].MaterialSpecular[1] = 0.328634f;
	sphereMaterial[2].MaterialSpecular[2] = 0.346435f;
	sphereMaterial[2].MaterialSpecular[3] = 1.0f;

	sphereMaterial[2].MaterialShininess = 0.3 * 128.0f;
		
		sphereMaterial[2].TranslateValues[0] = 1.5f;
		sphereMaterial[2].TranslateValues[1] = 9.0f;
		sphereMaterial[2].TranslateValues[2] = 0.0f;


	//pearl 4
	sphereMaterial[3].MaterialAmbient[0] = 0.25f;
	sphereMaterial[3].MaterialAmbient[1] = 0.20725f;
	sphereMaterial[3].MaterialAmbient[2] = 0.20725f;
	sphereMaterial[3].MaterialAmbient[3] = 1.0f;


	sphereMaterial[3].MaterialDiffuse[0] = 1.0f;
	sphereMaterial[3].MaterialDiffuse[1] = 0.829f;
	sphereMaterial[3].MaterialDiffuse[2] = 0.829f;
	sphereMaterial[3].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[3].MaterialSpecular[0] = 0.296648f;
	sphereMaterial[3].MaterialSpecular[1] = 0.296648f;
	sphereMaterial[3].MaterialSpecular[2] = 0.296648f;
	sphereMaterial[3].MaterialSpecular[3] = 1.0f;

	sphereMaterial[3].MaterialShininess = 0.088 * 128.0f;
	
	sphereMaterial[3].TranslateValues[0] = 1.5f;
	sphereMaterial[3].TranslateValues[1] = 6.5f;
	sphereMaterial[3].TranslateValues[2] = 0.0f;


	//ruby 5
	sphereMaterial[4].MaterialAmbient[0] = 0.1745f;
	sphereMaterial[4].MaterialAmbient[1] = 0.01175f;
	sphereMaterial[4].MaterialAmbient[2] = 0.01175f;
	sphereMaterial[4].MaterialAmbient[3] = 1.0f;

	sphereMaterial[4].MaterialDiffuse[0] = 0.61424f;
	sphereMaterial[4].MaterialDiffuse[1] = 0.04136f;
	sphereMaterial[4].MaterialDiffuse[2] = 0.04136f;
	sphereMaterial[4].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[4].MaterialSpecular[0] = 0.727811f;
	sphereMaterial[4].MaterialSpecular[1] = 0.626959f;
	sphereMaterial[4].MaterialSpecular[2] = 0.626959f;
	sphereMaterial[4].MaterialSpecular[3] = 1.0f;

	sphereMaterial[4].MaterialShininess = 0.6 * 128.0f;
	
	sphereMaterial[4].TranslateValues[0] = 1.5f;
	sphereMaterial[4].TranslateValues[1] = 4.0f;
	sphereMaterial[4].TranslateValues[2] = 0.0f;

	//turquoise 6
	sphereMaterial[5].MaterialAmbient[0] = 0.1f;
	sphereMaterial[5].MaterialAmbient[1] = 0.18725f;
	sphereMaterial[5].MaterialAmbient[2] = 0.1745f;
	sphereMaterial[5].MaterialAmbient[3] = 1.0f;

	sphereMaterial[5].MaterialDiffuse[0] = 0.396f;
	sphereMaterial[5].MaterialDiffuse[1] = 0.74151f;
	sphereMaterial[5].MaterialDiffuse[2] = 0.69102f;
	sphereMaterial[5].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[5].MaterialSpecular[0] = 0.297254f;
	sphereMaterial[5].MaterialSpecular[1] = 0.30829f;
	sphereMaterial[5].MaterialSpecular[2] = 0.306678f;
	sphereMaterial[5].MaterialSpecular[3] = 1.0f;

	sphereMaterial[5].MaterialShininess = 0.1 * 128.0f;

	sphereMaterial[5].TranslateValues[0] = 1.5f;
	sphereMaterial[5].TranslateValues[1] = 1.5f;
	sphereMaterial[5].TranslateValues[2] = 0.0f;

	//material of metals started

	//brass 7
	sphereMaterial[6].MaterialAmbient[0] = 0.329412f;
	sphereMaterial[6].MaterialAmbient[1] = 0.223529f;
	sphereMaterial[6].MaterialAmbient[2] = 0.027451f;
	sphereMaterial[6].MaterialAmbient[3] = 1.0f;

	sphereMaterial[6].MaterialDiffuse[0] = 0.780392f;
	sphereMaterial[6].MaterialDiffuse[1] = 0.568627f;
	sphereMaterial[6].MaterialDiffuse[2] = 0.113725f;
	sphereMaterial[6].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[6].MaterialSpecular[0] = 0.992157f;
	sphereMaterial[6].MaterialSpecular[1] = 0.941176f;
	sphereMaterial[6].MaterialSpecular[2] = 0.807843f;
	sphereMaterial[6].MaterialSpecular[3] = 1.0f;

	sphereMaterial[6].MaterialShininess = 0.21794872 * 128.0f;
	
	sphereMaterial[6].TranslateValues[0] = 9.0f;
	sphereMaterial[6].TranslateValues[1] = 14.0f;
	sphereMaterial[6].TranslateValues[2] = 0.0f;

	//bronze 8
	sphereMaterial[7].MaterialAmbient[0] = 0.2125f;
	sphereMaterial[7].MaterialAmbient[1] = 0.1275f;
	sphereMaterial[7].MaterialAmbient[2] = 0.054f;
	sphereMaterial[7].MaterialAmbient[3] = 1.0f;

	sphereMaterial[7].MaterialDiffuse[0] = 0.714f;
	sphereMaterial[7].MaterialDiffuse[1] = 0.4284f;
	sphereMaterial[7].MaterialDiffuse[2] = 0.18144f;
	sphereMaterial[7].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[7].MaterialSpecular[0] = 0.393548f;
	sphereMaterial[7].MaterialSpecular[1] = 0.271906f;
	sphereMaterial[7].MaterialSpecular[2] = 0.166721f;
	sphereMaterial[7].MaterialSpecular[3] = 1.0f;

	sphereMaterial[7].MaterialShininess = 0.2 * 128.0f;

	sphereMaterial[7].TranslateValues[0] = 9.0f;
	sphereMaterial[7].TranslateValues[1] = 11.5f;
	sphereMaterial[7].TranslateValues[2] = 0.0f;

	//chrome 9
	sphereMaterial[8].MaterialAmbient[0] = 0.25;
	sphereMaterial[8].MaterialAmbient[1] = 0.25f;
	sphereMaterial[8].MaterialAmbient[2] = 0.25f;
	sphereMaterial[8].MaterialAmbient[3] = 1.0f;

	sphereMaterial[8].MaterialDiffuse[0] = 0.4f;
	sphereMaterial[8].MaterialDiffuse[1] = 0.4f;
	sphereMaterial[8].MaterialDiffuse[2] = 0.4f;
	sphereMaterial[8].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[8].MaterialSpecular[0] = 0.774597f;
	sphereMaterial[8].MaterialSpecular[1] = 0.774597f;
	sphereMaterial[8].MaterialSpecular[2] = 0.774597f;
	sphereMaterial[8].MaterialSpecular[3] = 1.0f;

	sphereMaterial[8].MaterialShininess = 0.6 * 128.0f;

	sphereMaterial[8].TranslateValues[0] = 9.0f;
	sphereMaterial[8].TranslateValues[1] = 9.0f;
	sphereMaterial[8].TranslateValues[2] = 0.0f;

	//copper 10
	sphereMaterial[9].MaterialAmbient[0] = 0.19125f;
	sphereMaterial[9].MaterialAmbient[1] = 0.0735f;
	sphereMaterial[9].MaterialAmbient[2] = 0.0225f;
	sphereMaterial[9].MaterialAmbient[3] = 1.0f;

	sphereMaterial[9].MaterialDiffuse[0] = 0.7038f;
	sphereMaterial[9].MaterialDiffuse[1] = 0.27048f;
	sphereMaterial[9].MaterialDiffuse[2] = 0.0828f;
	sphereMaterial[9].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[9].MaterialSpecular[0] = 0.256777f;
	sphereMaterial[9].MaterialSpecular[1] = 0.137622f;
	sphereMaterial[9].MaterialSpecular[2] = 0.086014f;
	sphereMaterial[9].MaterialSpecular[3] = 1.0f;

	sphereMaterial[9].MaterialShininess = 0.1 * 128.0f;
	
	sphereMaterial[9].TranslateValues[0] = 9.0f;
	sphereMaterial[9].TranslateValues[1] = 6.5f;
	sphereMaterial[9].TranslateValues[2] = 0.0f;

	//Gold 11
	sphereMaterial[10].MaterialAmbient[0] = 0.24725f;
	sphereMaterial[10].MaterialAmbient[1] = 0.1995f;
	sphereMaterial[10].MaterialAmbient[2] = 0.0745f;
	sphereMaterial[10].MaterialAmbient[3] = 1.0f;

	sphereMaterial[10].MaterialDiffuse[0] = 0.75164f;
	sphereMaterial[10].MaterialDiffuse[1] = 0.60648f;
	sphereMaterial[10].MaterialDiffuse[2] = 0.22648f;
	sphereMaterial[10].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[10].MaterialSpecular[0] = 0.628281f;
	sphereMaterial[10].MaterialSpecular[1] = 0.555802f;
	sphereMaterial[10].MaterialSpecular[2] = 0.366065f;
	sphereMaterial[10].MaterialSpecular[3] = 1.0f;

	sphereMaterial[10].MaterialShininess = 0.4 * 128.0f;

	sphereMaterial[10].TranslateValues[0] = 9.0f;
	sphereMaterial[10].TranslateValues[1] = 4.0f;
	sphereMaterial[10].TranslateValues[2] = 0.0f;

	//Silver metal 12
	sphereMaterial[11].MaterialAmbient[0] = 0.19225f;
	sphereMaterial[11].MaterialAmbient[1] = 0.19225f;
	sphereMaterial[11].MaterialAmbient[2] = 0.19225f;
	sphereMaterial[11].MaterialAmbient[3] = 1.0f;

	sphereMaterial[11].MaterialDiffuse[0] = 0.50754f;
	sphereMaterial[11].MaterialDiffuse[1] = 0.50754f;
	sphereMaterial[11].MaterialDiffuse[2] = 0.50754f;
	sphereMaterial[11].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[11].MaterialSpecular[0] = 0.508273f;
	sphereMaterial[11].MaterialSpecular[1] = 0.508273f;
	sphereMaterial[11].MaterialSpecular[2] = 0.508273f;
	sphereMaterial[11].MaterialSpecular[3] = 1.0f;

	sphereMaterial[11].MaterialShininess = 0.4 * 128.0f;
	
	sphereMaterial[11].TranslateValues[0] = 9.0f;
	sphereMaterial[11].TranslateValues[1] = 1.5f;
	sphereMaterial[11].TranslateValues[2] = 0.0f;

	//material for Plastic Started 
	//black-Plastic 13
	sphereMaterial[12].MaterialAmbient[0] = 0.0f;
	sphereMaterial[12].MaterialAmbient[1] = 0.0f;
	sphereMaterial[12].MaterialAmbient[2] = 0.0f;
	sphereMaterial[12].MaterialAmbient[3] = 1.0f;

	sphereMaterial[12].MaterialDiffuse[0] = 0.01f;
	sphereMaterial[12].MaterialDiffuse[1] = 0.01f;
	sphereMaterial[12].MaterialDiffuse[2] = 0.01f;
	sphereMaterial[12].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[12].MaterialSpecular[0] = 0.50f;
	sphereMaterial[12].MaterialSpecular[1] = 0.50f;
	sphereMaterial[12].MaterialSpecular[2] = 0.50f;
	sphereMaterial[12].MaterialSpecular[3] = 1.0f;

	sphereMaterial[12].MaterialShininess = 0.25 * 128.0f;
	
	sphereMaterial[12].TranslateValues[0] = 17.25f;
	sphereMaterial[12].TranslateValues[1] = 14.0f;
	sphereMaterial[12].TranslateValues[2] = 0.0f;

	//Cyan-Plastic 14
	sphereMaterial[13].MaterialAmbient[0] = 0.0f;
	sphereMaterial[13].MaterialAmbient[1] = 0.1f;
	sphereMaterial[13].MaterialAmbient[2] = 0.06f;
	sphereMaterial[13].MaterialAmbient[3] = 1.0f;

	sphereMaterial[13].MaterialDiffuse[0] = 0.0f;
	sphereMaterial[13].MaterialDiffuse[1] = 0.50980392f;
	sphereMaterial[13].MaterialDiffuse[2] = 0.50980392f;
	sphereMaterial[13].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[13].MaterialSpecular[0] = 0.50196078f;
	sphereMaterial[13].MaterialSpecular[1] = 0.50196078f;
	sphereMaterial[13].MaterialSpecular[2] = 0.50196078f;
	sphereMaterial[13].MaterialSpecular[3] = 1.0f;

	sphereMaterial[13].MaterialShininess = 0.25 * 128.0f;
	
	sphereMaterial[13].TranslateValues[0] = 17.25f;
	sphereMaterial[13].TranslateValues[1] = 11.5f;
	sphereMaterial[13].TranslateValues[2] = 0.0f;

	//Green Plastic 15
	sphereMaterial[14].MaterialAmbient[0] = 0.0f;
	sphereMaterial[14].MaterialAmbient[1] = 0.0f;
	sphereMaterial[14].MaterialAmbient[2] = 0.0f;
	sphereMaterial[14].MaterialAmbient[3] = 1.0f;

	sphereMaterial[14].MaterialDiffuse[0] = 0.01f;
	sphereMaterial[14].MaterialDiffuse[1] = 0.35f;
	sphereMaterial[14].MaterialDiffuse[2] = 0.1f;
	sphereMaterial[14].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[14].MaterialSpecular[0] = 0.45f;
	sphereMaterial[14].MaterialSpecular[1] = 0.55f;
	sphereMaterial[14].MaterialSpecular[2] = 0.45f;
	sphereMaterial[14].MaterialSpecular[3] = 1.0f;

	sphereMaterial[14].MaterialShininess = 0.25 * 128.0f;

	sphereMaterial[14].TranslateValues[0] = 17.25f;
	sphereMaterial[14].TranslateValues[1] = 9.0f;
	sphereMaterial[14].TranslateValues[2] = 0.0f;

	//Red Plastic 16 
	sphereMaterial[15].MaterialAmbient[0] = 0.0f;
	sphereMaterial[15].MaterialAmbient[1] = 0.0f;
	sphereMaterial[15].MaterialAmbient[2] = 0.0f;
	sphereMaterial[15].MaterialAmbient[3] = 1.0f;

	sphereMaterial[15].MaterialDiffuse[0] = 0.5f;
	sphereMaterial[15].MaterialDiffuse[1] = 0.0;
	sphereMaterial[15].MaterialDiffuse[2] = 0.01f;
	sphereMaterial[15].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[15].MaterialSpecular[0] = 0.7f;
	sphereMaterial[15].MaterialSpecular[1] = 0.6f;
	sphereMaterial[15].MaterialSpecular[2] = 0.6f;
	sphereMaterial[15].MaterialSpecular[3] = 1.0f;

	sphereMaterial[15].MaterialShininess = 0.25 * 128.0f;

	sphereMaterial[15].TranslateValues[0] = 17.25f;
	sphereMaterial[15].TranslateValues[1] = 6.5f;
	sphereMaterial[15].TranslateValues[2] = 0.0f;

	//White Plastic 17 
	sphereMaterial[16].MaterialAmbient[0] = 0.0f;
	sphereMaterial[16].MaterialAmbient[1] = 0.0f;
	sphereMaterial[16].MaterialAmbient[2] = 0.0f;
	sphereMaterial[16].MaterialAmbient[3] = 1.0f;

	sphereMaterial[16].MaterialDiffuse[0] = 0.55f;
	sphereMaterial[16].MaterialDiffuse[1] = 0.55f;
	sphereMaterial[16].MaterialDiffuse[2] = 0.55f;
	sphereMaterial[16].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[16].MaterialSpecular[0] = 0.70f;
	sphereMaterial[16].MaterialSpecular[1] = 0.70f;
	sphereMaterial[16].MaterialSpecular[2] = 0.70f;
	sphereMaterial[16].MaterialSpecular[3] = 1.0f;

	sphereMaterial[16].MaterialShininess = 0.25 * 128.0f;
	
	sphereMaterial[16].TranslateValues[0] = 17.25f;
	sphereMaterial[16].TranslateValues[1] = 4.0f;
	sphereMaterial[16].TranslateValues[2] = 0.0f;

	//Yellow Plastic 18
	sphereMaterial[17].MaterialAmbient[0] = 0.0f;
	sphereMaterial[17].MaterialAmbient[1] = 0.0f;
	sphereMaterial[17].MaterialAmbient[2] = 0.0f;
	sphereMaterial[17].MaterialAmbient[3] = 1.0f;

	sphereMaterial[17].MaterialDiffuse[0] = 0.5f;
	sphereMaterial[17].MaterialDiffuse[1] = 0.5;
	sphereMaterial[17].MaterialDiffuse[2] = 0.0f;
	sphereMaterial[17].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[17].MaterialSpecular[0] = 0.60f;
	sphereMaterial[17].MaterialSpecular[1] = 0.60f;
	sphereMaterial[17].MaterialSpecular[2] = 0.50f;
	sphereMaterial[17].MaterialSpecular[3] = 1.0f;

	sphereMaterial[17].MaterialShininess = 0.25 * 128.0f;

	sphereMaterial[17].TranslateValues[0] = 17.25f;
	sphereMaterial[17].TranslateValues[1] = 1.5f;
	sphereMaterial[17].TranslateValues[2] = 0.0f;


	//Material for Rubber Started
	//BLACK-RUBBER 19
	sphereMaterial[18].MaterialAmbient[0] = 0.02f;
	sphereMaterial[18].MaterialAmbient[1] = 0.02f;
	sphereMaterial[18].MaterialAmbient[2] = 0.02f;
	sphereMaterial[18].MaterialAmbient[3] = 1.0f;

	sphereMaterial[18].MaterialDiffuse[0] = 0.01f;
	sphereMaterial[18].MaterialDiffuse[1] = 0.01f;
	sphereMaterial[18].MaterialDiffuse[2] = 0.01f;
	sphereMaterial[18].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[18].MaterialSpecular[0] = 0.4f;
	sphereMaterial[18].MaterialSpecular[1] = 0.4f;
	sphereMaterial[18].MaterialSpecular[2] = 0.4f;
	sphereMaterial[18].MaterialSpecular[3] = 1.0f;

	sphereMaterial[18].MaterialShininess = 0.078125 * 128.0f;
	
	sphereMaterial[18].TranslateValues[0] = 23.0f;
	sphereMaterial[18].TranslateValues[1] = 14.0f;
	sphereMaterial[18].TranslateValues[2] = 0.0f;

	//Cyan Rubber 20
	sphereMaterial[19].MaterialAmbient[0] = 0.0f;
	sphereMaterial[19].MaterialAmbient[1] = 0.05f;
	sphereMaterial[19].MaterialAmbient[2] = 0.05f;
	sphereMaterial[19].MaterialAmbient[3] = 1.0f;

	sphereMaterial[19].MaterialDiffuse[0] = 0.4f;
	sphereMaterial[19].MaterialDiffuse[1] = 0.5;
	sphereMaterial[19].MaterialDiffuse[2] = 0.5f;
	sphereMaterial[19].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[19].MaterialSpecular[0] = 0.04f;
	sphereMaterial[19].MaterialSpecular[1] = 0.7f;
	sphereMaterial[19].MaterialSpecular[2] = 0.7f;
	sphereMaterial[19].MaterialSpecular[3] = 1.0f;

	sphereMaterial[19].MaterialShininess = 0.078125 * 128;

	sphereMaterial[19].TranslateValues[0] = 23.0f;
	sphereMaterial[19].TranslateValues[1] = 11.5f;
	sphereMaterial[19].TranslateValues[2] = 0.0f;

	//Rubber Green 21
	sphereMaterial[20].MaterialAmbient[0] = 0.0f;
	sphereMaterial[20].MaterialAmbient[1] = 0.05f;
	sphereMaterial[20].MaterialAmbient[2] = 0.0f;
	sphereMaterial[20].MaterialAmbient[3] = 1.0f;

	sphereMaterial[20].MaterialDiffuse[0] = 0.4f;
	sphereMaterial[20].MaterialDiffuse[1] = 0.5;
	sphereMaterial[20].MaterialDiffuse[2] = 0.4f;
	sphereMaterial[20].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[20].MaterialSpecular[0] = 0.04f;
	sphereMaterial[20].MaterialSpecular[1] = 0.7f;
	sphereMaterial[20].MaterialSpecular[2] = 0.04f;
	sphereMaterial[20].MaterialSpecular[3] = 1.0f;

	sphereMaterial[20].MaterialShininess = 0.078125 * 128;

	sphereMaterial[20].TranslateValues[0] = 23.0f;
	sphereMaterial[20].TranslateValues[1] = 9.0f;
	sphereMaterial[20].TranslateValues[2] = 0.0f;


	//RUBBER RED 22

	sphereMaterial[21].MaterialAmbient[0] = 0.05f;
	sphereMaterial[21].MaterialAmbient[1] = 0.0f;
	sphereMaterial[21].MaterialAmbient[2] = 0.0f;
	sphereMaterial[21].MaterialAmbient[3] = 1.0f;

	sphereMaterial[21].MaterialDiffuse[0] = 0.5f;
	sphereMaterial[21].MaterialDiffuse[1] = 0.4f;
	sphereMaterial[21].MaterialDiffuse[2] = 0.4f;
	sphereMaterial[21].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[21].MaterialSpecular[0] = 0.7f;
	sphereMaterial[21].MaterialSpecular[1] = 0.04f;
	sphereMaterial[21].MaterialSpecular[2] = 0.04f;
	sphereMaterial[21].MaterialSpecular[3] = 1.0f;

	sphereMaterial[21].MaterialShininess = 0.078125 * 128;
	
	sphereMaterial[21].TranslateValues[0] = 23.0f;
	sphereMaterial[21].TranslateValues[1] = 6.5f;
	sphereMaterial[21].TranslateValues[2] = 0.0f;

	//rubber White 23 
	sphereMaterial[22].MaterialAmbient[0] = 0.05f;
	sphereMaterial[22].MaterialAmbient[1] = 0.05f;
	sphereMaterial[22].MaterialAmbient[2] = 0.05f;
	sphereMaterial[22].MaterialAmbient[3] = 1.0f;

	sphereMaterial[22].MaterialDiffuse[0] = 0.5f;
	sphereMaterial[22].MaterialDiffuse[1] = 0.5;
	sphereMaterial[22].MaterialDiffuse[2] = 0.5f;
	sphereMaterial[22].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[22].MaterialSpecular[0] = 0.7f;
	sphereMaterial[22].MaterialSpecular[1] = 0.7f;
	sphereMaterial[22].MaterialSpecular[2] = 0.7f;
	sphereMaterial[22].MaterialSpecular[3] = 1.0f;

	sphereMaterial[22].MaterialShininess = 0.078125 * 128;

	sphereMaterial[22].TranslateValues[0] = 23.0f;
	sphereMaterial[22].TranslateValues[1] = 4.0f;
	sphereMaterial[22].TranslateValues[2] = 0.0f;

	//RUBBER YELLOW 24 
	sphereMaterial[23].MaterialAmbient[0] = 0.05f;
	sphereMaterial[23].MaterialAmbient[1] = 0.05f;
	sphereMaterial[23].MaterialAmbient[2] = 0.0f;
	sphereMaterial[23].MaterialAmbient[3] = 1.0f;

	sphereMaterial[23].MaterialDiffuse[0] = 0.5f;
	sphereMaterial[23].MaterialDiffuse[1] = 0.5;
	sphereMaterial[23].MaterialDiffuse[2] = 0.4f;
	sphereMaterial[23].MaterialDiffuse[3] = 1.0f;

	sphereMaterial[23].MaterialSpecular[0] = 0.7f;
	sphereMaterial[23].MaterialSpecular[1] = 0.7f;
	sphereMaterial[23].MaterialSpecular[2] = 0.04f;
	sphereMaterial[23].MaterialSpecular[3] = 1.0f;

	sphereMaterial[23].MaterialShininess = 0.078125 * 128;
	
	sphereMaterial[23].TranslateValues[0] = 23.0f;
	sphereMaterial[23].TranslateValues[1] = 1.5f;
	sphereMaterial[23].TranslateValues[2] = 0.0f;

}
void createSphere()
{
   // fprintf(gpFile,"Inside Create Sphere \n");
    maxElements=0;
    numElements=0;
    numVertices=0;
   processSphereData();
  
}

/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    void draw();

    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
     [self draw];
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    update();
}

-(void) draw
{
     //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    mat4 modelMatrix;
	mat4 ViewMatrix;
	mat4 rotationMatrix;
	
	for(int i=0;i<24;i++)
	{
		modelMatrix = mat4::identity();
		ViewMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
	
		if (keypress == 1)
		{
			rotationMatrix = rotate(lightAngleZero, 1.0f, 0.0f, 0.0f);
			rotationMatrix = rotate(lightAngleZero, 0.0f, 0.0f, 1.0f);
         
		}
		else if (keypress == 2)
		{
			rotationMatrix = rotate(lightAngleOne, 0.0f, 0.0f, 1.0f);
			//rotationMatrix = rotate(lightAngleOne, 1.0f, 0.0f, 0.0f);

		}
		else if (keypress == 3)
		{
			rotationMatrix = rotate(lightAngleTwo, 1.0f, 0.0f, 0.0f);
			//rotationMatrix = rotate(lightAngleTwo, 0.0f, 1.0f, 0.0f);

		}
		if (bLight == true)
		{
			glUniform1i(lKeyPressUniform, 1);

			glUniform3fv(lightAmbientUniform, 1, lightAmbient);
			glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
			glUniform3fv(lightSpecularUniform, 1, lightSpecular);
			glUniform4fv(lightPositionUniform, 1, lightPosition);
			
			glUniform3fv(materialAmbientUniform, 1, sphereMaterial[i].MaterialAmbient);
			glUniform3fv(materialDiffuseUniform, 1, sphereMaterial[i].MaterialDiffuse);
			glUniform3fv(materialSpecularUniform, 1, sphereMaterial[i].MaterialSpecular);
			glUniform1f(materialShininessUniform, sphereMaterial[i].MaterialShininess);
		}
		else
		{
			glUniform1i(lKeyPressUniform, 0);
		}


		modelMatrix = translate(sphereMaterial[i].TranslateValues[0], sphereMaterial[i].TranslateValues[1], sphereMaterial[i].TranslateValues[2]);

		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
		glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, orthographicProjectionMatrix);
		glUniformMatrix4fv(lightRotationaMatrixUniform, 1, GL_FALSE, rotationMatrix);

		glBindVertexArray(vaoSphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);
        glDrawElements(GL_TRIANGLES,numElements,GL_UNSIGNED_SHORT,0);

		glBindVertexArray(0);

	}
	glUseProgram(0);

}

void update()
{
	if (lightAngleZero <= 360.0f && keypress == 1)
	{
		lightAngleZero = lightAngleZero + 0.2f;
	}
	else
	{
		lightAngleZero = 0.0f;
	}

	if (lightAngleOne <= 360.0f && keypress == 2)
	{
		lightAngleOne = lightAngleOne + 2.5f;
	}
	else
	{
		lightAngleOne = 0.0f;
	}


	if (lightAngleTwo <= 360.0f && keypress == 3)
	{
		lightAngleTwo = lightAngleTwo + 2.5f;
	}
	else
	{
		lightAngleTwo = 0.0f;
	}

}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

   if (fwidth <= fheight)
	{
		orthographicProjectionMatrix = ortho(0.0f,
			15.5f,
			0.0f,
			(15.5f * fwidth / fheight),
			-10.0f,
			10.0f);
	}
	else
	{
		orthographicProjectionMatrix = ortho(0.0f,
			(15.5f * fwidth / fheight),
			0.01f,
			15.5f,
			-10.0f,
			10.0f);
	}
     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
    if (bLight == false)
    {
        bLight = true;
    }
    else
    {
        bLight = false;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    keypress=keypress+1;
    if(keypress>3)
        keypress=0;
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

    if(vaoSphere)
    {
        glDeleteVertexArrays(1, &vaoSphere);
        vaoSphere=0;
    }
    
    if(vboSpherePosition)
    {
        glDeleteBuffers(1, &vboSpherePosition);
        vboSpherePosition=0;
    }

     if(vboSphereNormals)
    {
        glDeleteBuffers(1, &vboSphereNormals);
        vboSphereNormals=0;
    }

     if(vboSphereTexture)
    {
        glDeleteBuffers(1, &vboSphereTexture);
        vboSphereTexture=0;
    }

     if(vboSphereIndices)
    {
        glDeleteBuffers(1, &vboSphereIndices);
        vboSphereIndices=0;
    }
    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
