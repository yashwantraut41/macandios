//
//  main.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    NSString * appDelegateClassName;
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
        int ret=UIApplicationMain(argc, argv, nil, appDelegateClassName);
    [pPool release];
    return (ret);
}
