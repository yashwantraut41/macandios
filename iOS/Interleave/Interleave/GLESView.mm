//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};
    GLfloat lightAmbient[4] = { 0.25f,0.25f,0.25f,0.25f };
    GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0,1.0f };
    GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
    float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };

    GLfloat materialAmbient[4] = { 0.25f,0.25f,0.25f,0.25f };
    GLfloat materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat materialShininess = 128.0f;


@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint samplerUniform;


    //LIGHT Uniforms    
    GLuint lightDiffuseUniform;
    GLuint lightAmbientUniform;
    GLuint lightSpecularUniform;
    GLuint lightPositionUniform;

    //Material Uniform
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;
    GLuint lKeyPressUniform;

    GLuint vaoCube;
    GLuint vboVCNT;
    //for texture
    GLuint texture_MARBLE;

    //for animation
    GLfloat angleCube;
    bool bLight;
    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    void initInterleaveShadersAndVaoVbo();

    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
        [self initInterleaveShadersAndVaoVbo];
        bLight=false;
        angleCube=0.0f;
        //enable depth testing
         glEnable(GL_DEPTH_TEST);
         glEnable(GL_TEXTURE_2D);
         //DEPTH test to do following
        glDepthFunc(GL_LEQUAL);
        //clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        //set projection matrix to identity matrix
        perspectiveProjectionMatrix=vmath::mat4::identity();
    


    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}

-(void) initInterleaveShadersAndVaoVbo
{
    //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
    "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"in vec3 vNormal;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform mediump int lKeyPress;" \
		"out vec3 lightdirection;" \
		"out vec3 tnorm;" \
		"out vec3 viewer_vector;" \
		"out vec4 out_color;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
		"	if(lKeyPress==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;" \
		"tnorm=mat3(u_view_matrix*u_model_matrix)*vNormal;"\
		"lightdirection=vec3(u_light_position - eye_coordinates );" \
		"viewer_vector=vec3(-eye_coordinates.xyz);" \
		"}" \
		"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;" \
		"out_color=vColor;" \
		"out_TexCoord=vTexCoord;" \
		"}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
    "precision highp float;" \
    "in vec3 lightdirection;" \
		"in vec3 tnorm;" \
		"in vec3 viewer_vector;" \
		"in vec2 out_TexCoord;" \
		"in vec4 out_color;" \
		"uniform sampler2D u_sampler;" \
		"uniform vec3 u_lightambient;" \
		"uniform vec3 u_lightdiffuse;" \
		"uniform vec3 u_lightspecular;" \
		"uniform vec3 u_materialambient;" \
		"uniform vec3 u_materialdiffuse;" \
		"uniform vec3 u_materialspecular;" \
		"uniform float materialshinines;" \
		"uniform int lKeyPress;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ADS_light;" \
		"vec4 tex=texture(u_sampler,out_TexCoord);" \
		"\n" \
		"if(lKeyPress==1)" \
		"{" \
		"vec3 normalizetnorm=normalize(tnorm);" \
		"vec3 normalizelightdirection=normalize(lightdirection);" \
		"vec3 normalizeviewervector=normalize(viewer_vector);" \
		"float tn_dot_ld=max(dot(normalizelightdirection,normalizetnorm),0.0);" \
		"vec3 reflection_vector=reflect(-normalizelightdirection,normalizetnorm);" \
		"vec3 ambient=u_lightambient*u_materialambient;" \
		"vec3 diffuse=u_lightdiffuse*u_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_lightspecular*u_materialspecular*pow(max(dot(reflection_vector,normalizeviewervector),0.0),materialshinines);" \
		"phong_ADS_light=ambient+diffuse+specular;" \
		"}"	\
		"else" \
		"{" \
		"phong_ADS_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"FragColor=vec4(vec3(tex)*vec3(out_color)*phong_ADS_light,1.0);" \
		"}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	
    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");


	lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightambient");
	lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightdiffuse");
	lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightspecular");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialspecular");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "materialshinines");

	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "lKeyPress");

	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

    texture_MARBLE=[self loadTextureFromBMPFile:@"marble" :@"bmp"];

    //vertices ,colors, shader attribs, vbo, vao initializations
    
    const GLfloat cubeVCNT[] =
	{
		1.0f,1.0f,-1.0f,
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,

		-1.0f,1.0f,-1.0f,
		1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,0.0f,

			-1.0f,1.0f,1.0f,
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,1.0f,
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			1.0f,1.0f,

			1.0f,-1.0f,-1.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f,
				1.0f,1.0f,

				-1.0f,-1.0f,-1.0f,
				0.0f,1.0f,0.0f,
				0.0f,-1.0f,0.0f,
				0.0f,1.0f,

				-1.0f,-1.0f,1.0f,
				0.0f,1.0f,0.0f,
				0.0f,-1.0f,0.0f,
				0.0f,0.0f,

				1.0f,-1.0f,1.0f,
				0.0f,1.0f,0.0f,
				0.0f,-1.0f,0.0f,
				1.0f,0.0f,

				1.0f,1.0f,1.0f,
				0.0f,0.0f,1.0f,
				0.0f,0.0f,1.0f,
				0.0f,0.0f,


				-1.0f,1.0f,1.0f,
				0.0f,0.0f,1.0f,
					0.0f,0.0f,1.0f,
					1.0f,0.0f,


		-1.0f,-1.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		1.0f,1.0f,

		1.0f,-1.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,1.0f,

		1.0f,1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		1.0f,0.0f,

		-1.0f,1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		1.0f,1.0f,

		-1.0f,-1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		0.0f,1.0f,

		1.0f,-1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		0.0f,0.0f,

		1.0f,1.0f,-1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,


		1.0f,1.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,0.0f,
		1.0f,1.0f,

		1.0f,-1.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,0.0f,
			0.0f,1.0f,


			1.0f, -1.0f, -1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f,

			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
				0.0f, 0.0f,

				-1.0f, 1.0f, -1.0f,
				1.0f, 1.0f, 0.0f,
				-1.0f, 0.0f, 0.0f,
				1.0f, 0.0f,

				-1.0f, -1.0f, -1.0f,
				1.0f, 1.0f, 0.0f,
				-1.0f, 0.0f, 0.0f,
				1.0f, 1.0f,

				-1.0f, -1.0f, 1.0f,
				1.0f, 1.0f, 0.0f,
				-1.0f, 0.0f, 0.0f,
				0.0f, 1.0f

	};

	
	
    //create vao for cube
	glGenVertexArrays(1, &vaoCube);
	glBindVertexArray(vaoCube);

	//create vbo buffer  
	glGenBuffers(1, &vboVCNT);
	glBindBuffer(GL_ARRAY_BUFFER, vboVCNT);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVCNT), cubeVCNT, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*11, (void*)0);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 11, (void*)(3*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 11,(void*)(6*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 11, (void*)(9*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	//unbind cube  buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//unbind vao for cube
	glBindVertexArray(0);

}
-(GLuint)loadTextureFromBMPFile:(NSString*)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];

    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];

    if(!bmpImage)
    {
        NSLog(@"Cant find %@",textureFileNameWithPath);
        return(0);
    }

    CGImageRef cgImage=bmpImage.CGImage;
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    CFDataRef imageData= CGDataProviderCopyData((CGImageGetDataProvider(cgImage)));
    void* pixels=(void*)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1,&bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bmpTexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);

    glGenerateMipmap(GL_TEXTURE_2D);
    CFRelease(imageData);
    return(bmpTexture);             

}


/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
     void draw();

    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    [self draw];
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
        angleCube=angleCube+1.0f;
}


-(void) draw
{
        //start Using OpenGL program Object

     glUseProgram(gShaderProgramObject);
    

    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix=vmath::mat4::identity();
    vmath::mat4 ViewMatrix=vmath::mat4::identity();
    vmath::mat4 rotationMatrix=vmath::mat4::identity();
    vmath::mat4 translationMatrix=vmath::mat4::identity();
    

    translationMatrix=vmath::translate(0.0f,0.0f,-6.0f);
    rotationMatrix=vmath::rotate(angleCube,angleCube,angleCube);

    modelMatrix=translationMatrix*rotationMatrix;
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE,modelMatrix );
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE,ViewMatrix );
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,perspectiveProjectionMatrix );


    if (bLight == true)
	{
		glUniform1i(lKeyPressUniform, 1);
		glUniform3fv(lightAmbientUniform, 1, lightAmbient);
		glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
		glUniform3fv(lightSpecularUniform, 1, lightSpecular);
		glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
		glUniform3fv(materialAmbientUniform, 1, materialAmbient);
		glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
		glUniform3fv(materialSpecularUniform, 1, materialSpecular);
		glUniform1f(materialShininessUniform, materialShininess);
	}
	else
	{
		glUniform1i(lKeyPressUniform, 0);
	}



    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_MARBLE);
	glUniform1i(samplerUniform, 0);

	glBindVertexArray(vaoCube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

     //stop using openGL pROGRAM object
    glUseProgram(0);

   
}
-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
    if(bLight==false)
    {
        bLight=true;
    }
    else
    {
        bLight=false;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

    if(vaoCube)
    {
        glDeleteVertexArrays(1,&vaoCube);
        vaoCube=0;
    }
     if(vboVCNT)
    {
        glDeleteBuffers(1,&vboVCNT);
        vboVCNT=0;
    }

    if(texture_MARBLE)
    {
        glDeleteTextures(1,&texture_MARBLE);
        texture_MARBLE=0;
    }
    
   
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
