//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
#import "createSphere.h"
using namespace vmath;


enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

    
GLfloat materialAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;
GLfloat lightAngleZero = 0.0f;
GLfloat lightAngleOne = 0.0f;
GLfloat lightAngleTwo = 0.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    
    GLuint vaoSphere;
    GLuint vboSpherePosition;
    GLuint vboSphereNormals;
    GLuint vboSphereTexture;
    GLuint vboSphereIndices;
   
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;


    GLuint rotationalMatrixUniformred;
    GLuint rotationalMatrixUniformgreen;
    GLuint rotationalMatrixUniformblue;

    //LIGHT Uniforms
    GLuint lightDiffuseUniformRedLight;
    GLuint lightAmbientUniformRedLight;
    GLuint lightSpecularUniformRedLight;
    GLuint lightPositionUniformRedLight;

    GLuint lightDiffuseUniformBlueLight;
    GLuint lightAmbientUniformBlueLight;
    GLuint lightSpecularUniformBlueLight;
    GLuint lightPositionUniformBlueLight;

    GLuint lightDiffuseUniformGreenLight;
    GLuint lightAmbientUniformGreenLight;
    GLuint lightSpecularUniformGreenLight;
    GLuint lightPositionUniformGreenLight;
   //Material Uniform
   GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;
    GLuint lKeyPressUniform;

     struct Light
    {
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
    }light[3];
   
   
    bool bLight;

    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
    void initThreeLightShaders();
    [self initThreeLightShaders];


    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
   
    
        //clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    bLight=false;



    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}

-(void) initThreeLightShaders
{

        light[0].Ambient[0] = 0.0f;
	light[0].Ambient[1] = 0.0f;
	light[0].Ambient[2] = 0.0f;
	light[0].Ambient[3] = 0.0f;

	light[0].Diffuse[0] = 1.0f;
	light[0].Diffuse[1] = 0.0f;
	light[0].Diffuse[2] = 0.0f;
	light[0].Diffuse[3] = 1.0f;

	light[0].Specular[0] = 1.0f;
	light[0].Specular[1] = 0.0f;
	light[0].Specular[2] = 0.0f;
	light[0].Specular[3] = 1.0f;

	light[0].Position[0] = 0.0f;
	light[0].Position[1] = 0.0f;
	light[0].Position[2] = 0.0f;
	light[0].Position[3] = 1.0f;

	//initialize light parameteres for Green light
	light[1].Ambient[0] = 0.0f;
	light[1].Ambient[1] = 0.0f;
	light[1].Ambient[2] = 0.0f;
	light[1].Ambient[3] = 0.0f;

	light[1].Diffuse[0] = 0.0f;
	light[1].Diffuse[1] = 1.0f;
	light[1].Diffuse[2] = 0.0f;
	light[1].Diffuse[3] = 1.0f;

	light[1].Specular[0] = 0.0f;
	light[1].Specular[1] = 1.0f;
	light[1].Specular[2] = 0.0f;
	light[1].Specular[3] = 1.0f;

	light[1].Position[0] = 0.0f;
	light[1].Position[1] = 0.0f;
	light[1].Position[2] = 0.0f;
	light[1].Position[3] = 1.0f;

	//initialize light parameteres for Blue light
	light[2].Ambient[0] = 0.0f;
	light[2].Ambient[1] = 0.0f;
	light[2].Ambient[2] = 0.0f;
	light[2].Ambient[3] = 0.0f;

	light[2].Diffuse[0] = 0.0f;
	light[2].Diffuse[1] = 0.0f;
	light[2].Diffuse[2] = 1.0f;
	light[2].Diffuse[3] = 1.0f;

	light[2].Specular[0] = 0.0f;
	light[2].Specular[1] = 0.0f;
	light[2].Specular[2] = 1.0f;
	light[2].Specular[3] = 1.0f;

	light[2].Position[0] = 0.0f;
	light[2].Position[1] = 0.0f;
	light[2].Position[2] = 0.0f;
	light[2].Position[3] = 1.0f;

    //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
    "in vec4 Per_Vertex_vPosition;" \
		"in vec3 Per_Vertex_vNormal;" \
		"uniform mat4 u_Per_Vertex_model_matrix ;" \
		"uniform mat4 u_Per_Vertex_view_matrix ;" \
		"uniform mat4 u_Per_Vertex_projection_matrix ;" \
		"uniform mat4 u_Per_Vertex_rotational_matrix_red ;" \
		"uniform mat4 u_Per_Vertex_rotational_matrix_green ;" \
		"uniform mat4 u_Per_Vertex_rotational_matrix_blue ;" \
		"uniform mediump int u_Per_Vertex_lKeyPress ;" \
		"uniform vec3 u_Per_Vertex_lightambient_red ;" \
		"uniform vec3 u_Per_Vertex_lightambient_blue ;" \
		"uniform vec3 u_Per_Vertex_lightambient_green ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse_red ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse_blue ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse_green ;" \
		"uniform vec3 u_Per_Vertex_lightspecular_red ;" \
		"uniform vec3 u_Per_Vertex_lightspecular_blue ;" \
		"uniform vec3 u_Per_Vertex_lightspecular_green ;" \
		"uniform vec4 u_Per_Vertex_light_position_red ;" \
		"uniform vec4 u_Per_Vertex_light_position_blue ;" \
		"uniform vec4 u_Per_Vertex_light_position_green ;" \
		"uniform vec3 u_Per_Vertex_materialambient ;" \
		"uniform vec3 u_Per_Vertex_materialdiffuse ;" \
		"uniform vec3 u_Per_Vertex_materialspecular ;" \
		"uniform float u_Per_Vertex_material_shininess ;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_Per_Vertex_lKeyPress==1)"\
		"{"\
		"vec4 eye_coordinates=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"vec4 eye_coordinatesred=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition*u_Per_Vertex_rotational_matrix_red;" \
		"vec4 eye_coordinatesgreen=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition*u_Per_Vertex_rotational_matrix_green;" \
		"vec4 eye_coordinatesblue=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition*u_Per_Vertex_rotational_matrix_blue;" \
		"vec3 tnorm=normalize(mat3(u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix)*Per_Vertex_vNormal);" \
		"vec4 rotationalLightPositionred=u_Per_Vertex_light_position_red*u_Per_Vertex_rotational_matrix_red;"\
		"vec4 rotationalLightPositiongreen=u_Per_Vertex_light_position_green*u_Per_Vertex_rotational_matrix_green;"\
		"vec4 rotationalLightPositionblue=u_Per_Vertex_light_position_blue*u_Per_Vertex_rotational_matrix_blue;"\
		"vec3 lightdirection_red=normalize(vec3(rotationalLightPositionred-eye_coordinatesred));" \
		"vec3 lightdirection_blue=normalize(vec3(rotationalLightPositionblue-eye_coordinatesblue));" \
		"vec3 lightdirection_green=normalize(vec3(rotationalLightPositiongreen-eye_coordinatesgreen));" \
		"float tn_dot_ld_red=max(dot(lightdirection_red,tnorm),0.0);" \
		"float tn_dot_ld_blue=max(dot(lightdirection_blue,tnorm),0.0);" \
		"float tn_dot_ld_green=max(dot(lightdirection_green,tnorm),0.0);" \
		" vec3 reflection_vector_red=reflect(-lightdirection_red,tnorm);" \
		" vec3 reflection_vector_blue=reflect(-lightdirection_blue,tnorm);" \
		" vec3 reflection_vector_green=reflect(-lightdirection_green,tnorm);" \
		" vec3 viewer_vector=normalize(vec3(- eye_coordinates.xyz));" \
		" vec3 ambient=u_Per_Vertex_lightambient_red*u_Per_Vertex_materialambient;" \
		" vec3 diffuse=u_Per_Vertex_lightdiffuse_red*u_Per_Vertex_materialdiffuse*tn_dot_ld_red;" \
		" vec3 specular=u_Per_Vertex_lightspecular_red*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector_red,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		" vec3 ambientblue=u_Per_Vertex_lightambient_blue*u_Per_Vertex_materialambient;" \
		" vec3 diffuseblue=u_Per_Vertex_lightdiffuse_blue*u_Per_Vertex_materialdiffuse*tn_dot_ld_blue;" \
		" vec3 specularblue=u_Per_Vertex_lightspecular_blue*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector_blue,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		" vec3 ambientgreen=u_Per_Vertex_lightambient_green*u_Per_Vertex_materialambient;" \
		" vec3 diffusegreen=u_Per_Vertex_lightdiffuse_green*u_Per_Vertex_materialdiffuse*tn_dot_ld_green;" \
		" vec3 speculargreen=u_Per_Vertex_lightspecular_green*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector_green,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		"phong_ads_light=ambient+ambientblue+ambientgreen+diffuse+diffusegreen+diffuseblue+specular+specularblue+speculargreen;" \
		"}"\
		"else" \
		"{" \
		"phong_ads_light=vec3(1.0,1.0,1.0);" \
		"}"\
		"gl_Position=u_Per_Vertex_projection_matrix*u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"}";
	
   glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
    "precision highp float;" \
    "in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor=vec4(phong_ads_light,1.0);" \
		"}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "Per_Vertex_vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "Per_Vertex_vNormal");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_projection_matrix");
	rotationalMatrixUniformred = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_rotational_matrix_red");
	rotationalMatrixUniformgreen = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_rotational_matrix_green");
	rotationalMatrixUniformblue = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_rotational_matrix_blue");


	lightAmbientUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient_red");
	lightDiffuseUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse_red");
	lightSpecularUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular_red");

	lightAmbientUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient_blue");
	lightDiffuseUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse_blue");
	lightSpecularUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular_blue");

	lightAmbientUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient_green");
	lightDiffuseUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse_green");
	lightSpecularUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular_green");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialspecular");

	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_material_shininess");

	lightPositionUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position_red");
	lightPositionUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position_blue");
	lightPositionUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position_green");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lKeyPress");


    //create sphere
    createSphere();
    //vertices ,colors, shader attribs, vbo, vao initializations
      

    

    glGenVertexArrays(1, &vaoSphere);
    glBindVertexArray(vaoSphere);
    
    glGenBuffers(1, &vboSpherePosition);
    glBindBuffer(GL_ARRAY_BUFFER, vboSpherePosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vboSphereNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_normals), model_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vboSphereTexture);
    glBindBuffer(GL_ARRAY_BUFFER, vboSphereTexture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_textures), model_textures, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1,&vboSphereIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboSphereIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(model_elements),model_elements,GL_STATIC_DRAW);
    
    glBindVertexArray(0);
    


}

void createSphere()
{
   // fprintf(gpFile,"Inside Create Sphere \n");
    maxElements=0;
    numElements=0;
    numVertices=0;
   processSphereData();
  
}

/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    void draw();

    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
     [self draw];
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    lightAngleZero = lightAngleZero + 1.5f;
	lightAngleOne = lightAngleOne + 1.5f;
	lightAngleTwo = lightAngleTwo + 1.5f;

}

-(void) draw
{
     glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix=vmath::mat4::identity();
    vmath::mat4 ViewMatrix=vmath::mat4::identity();
    vmath::mat4 translationMatrix=vmath::mat4::identity();
    vmath::mat4 rotationalMatrixred=vmath::mat4::identity();
    vmath::mat4 rotationalMatrixgreen=vmath::mat4::identity();
    vmath::mat4 rotationalMatrixblue=vmath::mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
	rotationalMatrixred = vmath::rotate(lightAngleZero, 1.0f, 0.0f, 0.0f);
	rotationalMatrixgreen = vmath::rotate(lightAngleOne, 0.0f, 1.0f, 0.0f);
	rotationalMatrixblue = vmath::rotate(lightAngleTwo, 1.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * translationMatrix;
    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	glUniformMatrix4fv(rotationalMatrixUniformred, 1, GL_FALSE, rotationalMatrixred);
	glUniformMatrix4fv(rotationalMatrixUniformgreen, 1, GL_FALSE, rotationalMatrixgreen);
	glUniformMatrix4fv(rotationalMatrixUniformblue, 1, GL_FALSE, rotationalMatrixblue);

    if (bLight == true)
	{
		glUniform1i(lKeyPressUniform, 1);

		glUniform3fv(lightAmbientUniformRedLight, 1, light[0].Ambient);
		glUniform3fv(lightDiffuseUniformRedLight, 1, light[0].Diffuse);
		glUniform3fv(lightSpecularUniformRedLight, 1, light[0].Specular);
		glUniform4fv(lightPositionUniformRedLight, 1, light[0].Position);


		glUniform3fv(lightAmbientUniformGreenLight, 1, light[1].Ambient);
		glUniform3fv(lightDiffuseUniformGreenLight, 1, light[1].Diffuse);
		glUniform3fv(lightSpecularUniformGreenLight, 1, light[1].Specular);
		glUniform4fv(lightPositionUniformGreenLight, 1, light[1].Position);

		glUniform3fv(lightAmbientUniformBlueLight, 1, light[2].Ambient);
		glUniform3fv(lightDiffuseUniformBlueLight, 1, light[2].Diffuse);
		glUniform3fv(lightSpecularUniformBlueLight, 1, light[2].Specular);
		glUniform4fv(lightPositionUniformBlueLight, 1, light[2].Position);

		glUniform3fv(materialAmbientUniform, 1, materialAmbient);
		glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
		glUniform3fv(materialSpecularUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);

	}
	else
	{
		glUniform1i(lKeyPressUniform, 0);
	}
    //bind vao
    glBindVertexArray(vaoSphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);

    //draw eith by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawElements(GL_TRIANGLES,numElements,GL_UNSIGNED_SHORT,0);

    //UNBIND VAO
    glBindVertexArray(0);
    
    //stop using openGL pROGRAM object
    glUseProgram(0);

}


-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
    if (bLight == false)
    {
        bLight = true;
    }
    else
    {
        bLight = false;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

    if(vaoSphere)
    {
        glDeleteVertexArrays(1, &vaoSphere);
        vaoSphere=0;
    }
    
    if(vboSpherePosition)
    {
        glDeleteBuffers(1, &vboSpherePosition);
        vboSpherePosition=0;
    }

     if(vboSphereNormals)
    {
        glDeleteBuffers(1, &vboSphereNormals);
        vboSphereNormals=0;
    }

     if(vboSphereTexture)
    {
        glDeleteBuffers(1, &vboSphereTexture);
        vboSphereTexture=0;
    }

     if(vboSphereIndices)
    {
        glDeleteBuffers(1, &vboSphereIndices);
        vboSphereIndices=0;
    }
      //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
