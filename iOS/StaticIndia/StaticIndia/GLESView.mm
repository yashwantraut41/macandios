//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
using namespace vmath;


enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

     GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

     GLuint vaoHorizonatalLine;
    GLuint vaoVerticalLine;
    GLuint vaoLeftSlantLine;
    GLuint vaoRightSlantLine;

    GLuint vaoLeftSlantLineForD;
    GLuint vaoRightSlantLineForD;

    GLuint vboHorizontalLinePosition;
    GLuint vboVerticalLinePosition;
    GLuint vboLeftSlantLinePosition;
    GLuint vboRightSlantLinePosition;
    GLuint vboLeftSlantLinePositionForD;
    GLuint vboRightSlantLinePositionForD;

    GLuint vboHorizontalLineColor;
    GLuint vboLeftSlantLineForDColor;
    GLuint vboRightSlantLineForDColor;

    GLuint vboColor;
    GLuint mvpUniform;

    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
       /* vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
   "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_Color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_Color=vColor;" \
		"}";
    glShaderSource(vertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
    "precision highp float;" \
      "out vec4 FragColor;" \
		"in vec4 out_Color;" \
		"void main(void)" \
		"{" \
		"FragColor=out_Color;" \
		"}";
    //provide fragment shader source code to fragement shader
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    shaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

    //link shader
    glLinkProgram(shaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }*/
    
    //get mvpUniform location
    void initStaticIndiaShaders();
    void initStaticIndiaVaoVbos();
    [self initStaticIndiaShaders];
    [self initStaticIndiaVaoVbos];


    //vertices ,colors, shader attribs, vbo, vao initializations
  

	
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
   
    
        //clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    



    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}

-(void) initStaticIndiaShaders
{
     gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
    "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_Color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_Color=vColor;" \
		"}";
    glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es"
    "\n" \
        "precision highp float;" \
        "out vec4 FragColor;" \
		"in vec4 out_Color;" \
		"void main(void)" \
		"{" \
		"FragColor=out_Color;" \
		"}";
    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
   
}

-(void) initStaticIndiaVaoVbos
{
    //vertices ,colors, shader attribs, vbo, vao initializations
    const GLfloat verticalLine[] =
	{
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};

	const GLfloat horizontalLine[] =
	{
		-0.7f,1.0f,0.0f,
		0.7f,1.0f,0.0f
	};

	const GLfloat leftSlantLine[] =
	{
		-0.5f,1.0f,0.0f,
		0.5f,-1.0f,0.0f
	};

	const GLfloat rightSlantLine[] =
	{
		0.5f,1.0f,0.0f,
		-0.5f,-1.0f,0.0f
	};

	const GLfloat leftSlantLineForD[] =
	{
		-0.5f,1.0f,0.0f,
		0.7f,0.5f,0.0f
	};

	const GLfloat rightSlantLineForD[] =
	{
		0.7f,-0.5f,0.0f,
		-0.5f,-1.0f,0.0f
	};

	const GLfloat color[] =
	{
		1.0f, 0.501f, 0.0f,
		0.0f, 0.501f, 0.0f
	};

	const GLfloat leftSlantLineForDColor[]=
	{
				1.0f, 0.501f, 0.0f,
				1.0f, 0.501f, 0.0f
	};
	
	const GLfloat rightSlantLineForDColor[]=
	{
				0.0f, 0.501f, 0.0f,
				0.0f, 0.501f, 0.0f
	};

    
    //create vao vertical line
	glGenVertexArrays(1, &vaoVerticalLine);
	glBindVertexArray(vaoVerticalLine);

	glGenBuffers(1, &vboVerticalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboVerticalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLine), verticalLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color 
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao horizontal line
	glGenVertexArrays(1, &vaoHorizonatalLine);
	glBindVertexArray(vaoHorizonatalLine);

	glGenBuffers(1, &vboHorizontalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLine), horizontalLine, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboHorizontalLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);

	glBufferData(GL_ARRAY_BUFFER, 3*2*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	
	glBindVertexArray(0);


	//create vao leftSlantLine
	glGenVertexArrays(1, &vaoLeftSlantLine);
	glBindVertexArray(vaoLeftSlantLine);

	glGenBuffers(1, &vboLeftSlantLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLine), leftSlantLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color 
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao leftSlantLineForD
	glGenVertexArrays(1, &vaoLeftSlantLineForD);
	glBindVertexArray(vaoLeftSlantLineForD);

	glGenBuffers(1, &vboLeftSlantLinePositionForD);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLinePositionForD);
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLineForD), leftSlantLineForD, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboLeftSlantLineForDColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLineForDColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLineForDColor),leftSlantLineForDColor , GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//create vao rightSlantLineForD
	glGenVertexArrays(1, &vaoRightSlantLineForD);
	glBindVertexArray(vaoRightSlantLineForD);

	glGenBuffers(1, &vboRightSlantLinePositionForD);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLinePositionForD);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLineForD), rightSlantLineForD, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboRightSlantLineForDColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLineForDColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLineForDColor), rightSlantLineForDColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//create vao for rightSlantLine
	glGenVertexArrays(1, &vaoRightSlantLine);
	glBindVertexArray(vaoRightSlantLine);

	glGenBuffers(1, &vboRightSlantLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLine), rightSlantLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);




}


/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    void draw();

    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
     [self draw];
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
       
}
-(void) draw
{
     //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix=vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix=vmath::mat4::identity();
    modelViewMatrix=vmath::translate(-3.0f,0.0f,-6.0f);

    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I

    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    modelViewMatrix=vmath::translate(-3.0f,0.0f,-6.0f);
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glLineWidth(5.0f);
	glBindBuffer( GL_ARRAY_BUFFER, vboHorizontalLineColor);
	 GLfloat colorHorizontalLine[] =
	{
		1.0f, 0.501f, 0.0f,
		1.0f, 0.501f, 0.0f
	};
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


    //horizontal line for I

    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    modelViewMatrix=vmath::translate(-3.0f,-2.0f,-6.0f);
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
		colorHorizontalLine[0] = 0.0f;
		colorHorizontalLine[1] = 0.501f;
		colorHorizontalLine[2] = 0.0f;
		colorHorizontalLine[3] = 0.0f;
		colorHorizontalLine[4] = 0.501f;
		colorHorizontalLine[5] = 0.0f;
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(5.0f);

	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

    //vertical line For N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-1.8f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//leftSlantLine for N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-1.3f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//vertical line For N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-0.8f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//vertical line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-0.5f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//leftslant line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLineForD);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
	
	//rightSlant line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRightSlantLineForD);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


	//vertical line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -12.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//for second I
	//vertical line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, -2.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//right slant line For A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(2.6f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRightSlantLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//left slant line for A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(3.58f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLine);
	glLineWidth(5.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line Orange
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.6f, -1.0f, -9.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glLineWidth(5.0f);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line white
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.1f, -1.1f, -8.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 1.0f;
	colorHorizontalLine[2] = 1.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 1.0f;
	colorHorizontalLine[5] = 1.0f;
	glLineWidth(5.0f);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line Green
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.0f, -1.2f, -7.8f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;
	glLineWidth(5.0f);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);





    //stop using openGL pROGRAM object
    glUseProgram(0);
}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

    if (vboColor)
	{
		glDeleteBuffers(1, &vboColor);
		vboColor = 0;
	}
	if (vboHorizontalLineColor)
	{
		glDeleteBuffers(1, &vboHorizontalLineColor);
		vboHorizontalLineColor = 0;
	}
	if (vboHorizontalLinePosition)
	{
		glDeleteBuffers(1, &vboHorizontalLinePosition);
		vboHorizontalLinePosition = 0;
	}

	if (vboVerticalLinePosition)
	{
		glDeleteBuffers(1, &vboVerticalLinePosition);
		vboVerticalLinePosition = 0;
	}

	if (vboLeftSlantLinePosition)
	{
		glDeleteBuffers(1, &vboLeftSlantLinePosition);
		vboLeftSlantLinePosition = 0;
	}

	if (vboLeftSlantLinePositionForD)
	{
		glDeleteBuffers(1, &vboLeftSlantLinePositionForD);
		vboLeftSlantLinePositionForD = 0;
	}

	if (vboRightSlantLinePosition)
	{
		glDeleteBuffers(1, &vboRightSlantLinePosition);
		vboRightSlantLinePosition = 0;
	}

	if (vboRightSlantLinePositionForD)
	{
		glDeleteBuffers(1, &vboRightSlantLinePositionForD);
		vboRightSlantLinePositionForD = 0;
	}

	if (vaoLeftSlantLine)
	{
		glDeleteVertexArrays(1, &vaoLeftSlantLine);
		vaoLeftSlantLine = 0;
	}
	if (vaoLeftSlantLineForD)
	{
		glDeleteVertexArrays(1, &vaoLeftSlantLineForD);
		vaoLeftSlantLineForD = 0;
	}

	if (vaoRightSlantLine)
	{
		glDeleteVertexArrays(1, &vaoRightSlantLine);
		vaoRightSlantLine = 0;
	}

	if (vaoRightSlantLineForD)
	{
		glDeleteVertexArrays(1, &vaoRightSlantLineForD);
		vaoRightSlantLineForD = 0;
	}

	if (vaoHorizonatalLine)
	{
		glDeleteVertexArrays(1, &vaoHorizonatalLine);
		vaoHorizonatalLine = 0;
	}

	if (vaoVerticalLine)
	{
		glDeleteVertexArrays(1, &vaoVerticalLine);
		vaoVerticalLine = 0;
	}

    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
