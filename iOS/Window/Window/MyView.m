//
//  MyView.m
//  Window
//
//  Created by yashwant raut on 18/12/19.
//

#import "MyView.h"

@implementation MyView
{
    NSString *centralText;
}

-(id)initWithFrame:(CGRect)frameRect
{
    self=[super initWithFrame:frameRect];
    if(self)
    {
    //set scenes background color
    [self setBackgroundColor:[UIColor whiteColor]];
    
    centralText=@"Hello World From iOS!!";
    
    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}



- (void)drawRect:(CGRect)rect
{
    // Drawing code
    //black Background
    UIColor *fillColor=[UIColor blackColor];
    [fillColor set];
    UIRectFill(rect);
    
    //dictionary with kvc
    NSDictionary *dictionaryForTextAttributes=[NSDictionary
                                               dictionaryWithObjectsAndKeys:
                                               [UIFont fontWithName:@"Helvetica" size:24],NSFontAttributeName,
                                               [UIColor greenColor],
                                                        NSForegroundColorAttributeName,
                                               nil];
    
    CGSize textSize=[centralText sizeWithAttributes:dictionaryForTextAttributes];
    CGPoint point;
    point.x=(rect.size.width/2)-(textSize.width/2);
    point.y=(rect.size.height/2)-(textSize.height/2)+12;
    
    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
    
}

//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    centralText=@" 'touchesBegan' Event occured";
    [self setNeedsDisplay];
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
    centralText=@" 'onSingleTap' Event Occured";
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    centralText=@"onDoubleTap'Event Occured";
    [self setNeedsDisplay];
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
    centralText=@" 'onLongPress' Event Occured";
    [self setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{
    [super dealloc];
}

@end
