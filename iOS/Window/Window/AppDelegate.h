//
//  AppDelegate.h
//  Window
//
//  Created by yashwant raut on 18/12/19.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

