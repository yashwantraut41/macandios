//
//  GLESView.m
//  BlueWindow
//
//  Created by yashwant raut on 21/12/19.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"
#import "createSphere.h"
using namespace vmath;


enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

    GLfloat lightAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
    GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0,1.0f };
    GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
    float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };

   GLfloat materialAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
   GLfloat materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
   GLfloat materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
   GLfloat materialShininess =  128.0f;


@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameTimeInterval;
    BOOL isAnimating;

      GLuint gVertexShaderObject;
      GLuint gFragmentShaderObject;
      GLuint gShaderProgramObject;
     
     GLuint Per_Fragment_VertexShaderObject;
    GLuint Per_Fragment_FragmentShaderObject;
    GLuint Per_Fragment_ShaderProgramObject;

    
    GLuint vaoSphere;
    GLuint vboSpherePosition;
    GLuint vboSphereNormals;
    GLuint vboSphereTexture;
    GLuint vboSphereIndices;
   
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;
    //LIGHT Uniforms
    GLuint lightDiffuseUniform;
    GLuint lightAmbientUniform;
    GLuint lightSpecularUniform;
    GLuint lightPositionUniform;
   //Material Uniform
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;  
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;

    GLuint lKeyPressUniform;

//for per fragment
    GLuint  Per_Fragment_modelMatrixUniform;
    GLuint  Per_Fragment_viewMatrixUniform;
    GLuint Per_Fragment_projectionMatrixUniform;

//LIGHT Uniforms perfragment
    GLuint Per_Fragment_lightDiffuseUniform;
    GLuint Per_Fragment_lightAmbientUniform;
    GLuint Per_Fragment_lightSpecularUniform;
    GLuint Per_Fragment_lightPositionUniform;

//Material Uniform perfragment
    GLuint Per_Fragment_materialAmbientUniform;
    GLuint Per_Fragment_materialDiffuseUniform;
    GLuint Per_Fragment_materialSpecularUniform;
    GLuint Per_Fragment_materialShininessUniform;
    GLuint Per_Fragment_lKeyPressUniform;

    bool bLight;
    bool vKeyPress ;
    bool fKeyPress ;
   
    vmath::mat4 perspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        //keep alpha component 1.0
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("Cannot Obtain The EAGL Context \n");
            [self release];
            return (nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("initWithFrame Failed To Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initialization
        isAnimating=NO;
        animationFrameTimeInterval=60;
        
         void perVertexLighting(void);
        void perFragmentLighting(void);
        [self perVertexLighting];
	    [self perFragmentLighting];
	
        //create sphere
         createSphere();

        glGenVertexArrays(1, &vaoSphere);
        glBindVertexArray(vaoSphere);
    
        glGenBuffers(1, &vboSpherePosition);
        glBindBuffer(GL_ARRAY_BUFFER, vboSpherePosition);
        glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
         glBindBuffer(GL_ARRAY_BUFFER, 0);

         glGenBuffers(1, &vboSphereNormals);
         glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormals);
            glBufferData(GL_ARRAY_BUFFER, sizeof(model_normals), model_normals, GL_STATIC_DRAW);
         glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
         glBindBuffer(GL_ARRAY_BUFFER, 0);

         glGenBuffers(1, &vboSphereTexture);
        glBindBuffer(GL_ARRAY_BUFFER, vboSphereTexture);
        glBufferData(GL_ARRAY_BUFFER, sizeof(model_textures), model_textures, GL_STATIC_DRAW);
         glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenBuffers(1,&vboSphereIndices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboSphereIndices);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(model_elements),model_elements,GL_STATIC_DRAW);
    
        glBindVertexArray(0);
    




    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
        //clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    bLight=false;
    vKeyPress=true;
    fKeyPress=false;



    //GESTURE recognititon
    //TAP gesture code
    UITapGestureRecognizer *singleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSinglTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    //i:e touch of 1 finger
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];
    
    UITapGestureRecognizer *doubleTapGestureRecognizer=
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    //touch of 1 finger
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];
    
    //this will allow to differentiate between single tap and double tap
    [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    //swipe Gesture
    UISwipeGestureRecognizer *swipeGestureRecognizer=
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
    [self addGestureRecognizer:swipeGestureRecognizer];
    
    //long press Gesture
    UILongPressGestureRecognizer *longPressGestureRecognizer=
        [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
    [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return (self);
}

-(void) perVertexLighting
{
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 300 es" \
    "\n" \
    "in vec4 Per_Vertex_vPosition;" \
		"in vec3 Per_Vertex_vNormal;" \
		"uniform mat4 u_Per_Vertex_model_matrix ;" \
		"uniform mat4 u_Per_Vertex_view_matrix ;" \
		"uniform mat4 u_Per_Vertex_projection_matrix ;" \
		"uniform  mediump int u_Per_Vertex_lKeyPress ;" \
		"uniform vec3 u_Per_Vertex_lightambient ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse ;" \
		"uniform vec3 u_Per_Vertex_lightspecular ;" \
		"uniform vec4 u_Per_Vertex_light_position ;" \
		"uniform vec3 u_Per_Vertex_materialambient ;" \
		"uniform vec3 u_Per_Vertex_materialdiffuse ;" \
		"uniform vec3 u_Per_Vertex_materialspecular ;" \
		"uniform float u_Per_Vertex_material_shininess ;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_Per_Vertex_lKeyPress==1)"\
		"{"\
		"vec4 eye_coordinates=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"vec3 tnorm=normalize(mat3(u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix)*Per_Vertex_vNormal);" \
		"vec3 lightdirection=normalize(vec3(u_Per_Vertex_light_position-eye_coordinates));" \
		"float tn_dot_ld=max(dot(lightdirection,tnorm),0.0);" \
		"vec3 reflection_vector=reflect(-lightdirection,tnorm);" \
		"vec3 viewer_vector=normalize(vec3(- eye_coordinates.xyz));" \
		"vec3 ambient=u_Per_Vertex_lightambient*u_Per_Vertex_materialambient;" \
		"vec3 diffuse=u_Per_Vertex_lightdiffuse*u_Per_Vertex_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_Per_Vertex_lightspecular*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		"phong_ads_light=ambient+diffuse+specular;" \
		"}"\
		"else" \
		"{" \
		"phong_ads_light=vec3(1.0,1.0,1.0);" \
		"}"\
		"gl_Position=u_Per_Vertex_projection_matrix*u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"}";
	
   glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 300 es "
    "\n" \
    "precision highp float;" \
    "in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor=vec4(phong_ads_light,1.0);" \
		"}";
    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "Per_Vertex_vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "Per_Vertex_vNormal");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf( "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_projection_matrix");


	lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient");
	lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse");
	lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialspecular");

	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_material_shininess");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lKeyPress");



}

-(void) perFragmentLighting
{
	Per_Fragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//vertex shader code 
	const  GLchar* Per_Fragment_vertexShaderSourceCode =
        "#version 300 es" \
		"\n" \
		"in vec4 Per_Fragment_vPosition;" \
		"in vec3 Per_Fragment_vNormal;" \
		"uniform mat4 u_Per_Fragment_model_matrix;" \
		"uniform mat4 u_Per_Fragment_view_matrix;" \
		"uniform mat4 u_Per_Fragment_projection_matrix;" \
		"uniform vec4 u_Per_Fragment_light_position;" \
		"uniform mediump  int Per_Fragment_lKeyPress;" \
		"out vec3 Per_Fragment_lightdirection;" \
		"out vec3 Per_Fragment_tnorm;" \
		"out vec3 Per_Fragment_viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(Per_Fragment_lKeyPress==1)" \
		"{" \
		"vec4 eye_coordinates=u_Per_Fragment_view_matrix*u_Per_Fragment_model_matrix*Per_Fragment_vPosition;" \
		"Per_Fragment_tnorm=mat3(u_Per_Fragment_view_matrix*u_Per_Fragment_model_matrix)*Per_Fragment_vNormal;"\
		"Per_Fragment_lightdirection=vec3(u_Per_Fragment_light_position - eye_coordinates );" \
		"Per_Fragment_viewer_vector=vec3(-eye_coordinates.xyz);" \
		"}" \
		"gl_Position=u_Per_Fragment_projection_matrix*u_Per_Fragment_view_matrix*u_Per_Fragment_model_matrix*Per_Fragment_vPosition;" \
		"}";


	//specify above shader source code to vertexShaderObject 
	//give shader source code
	glShaderSource(Per_Fragment_VertexShaderObject, 1, (const GLchar * *)& Per_Fragment_vertexShaderSourceCode, NULL);

	//compile the vertex shader code 
	glCompileShader(Per_Fragment_VertexShaderObject);

	//error checking code for vertex shader 
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;
	glGetShaderiv(Per_Fragment_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(Per_Fragment_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(Per_Fragment_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf( "Per Fragment Vertex Shader Compilation Log :%s\n", szInfoLog);
				free(szInfoLog);
				[self release];
			}
		}
	}

	//WRITE fragment shader 
	Per_Fragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//shader code
	const GLchar* Per_Fragment_fragmentShaderSourceCode =
        "#version 300 es" \
		"\n" \
        "precision highp float;" \
		"in vec3 Per_Fragment_lightdirection;" \
		"in vec3 Per_Fragment_tnorm;" \
		"in vec3 Per_Fragment_viewer_vector;" \
		"uniform vec3 u_Per_Fragment_lightambient;" \
		"uniform vec3 u_Per_Fragment_lightdiffuse;" \
		"uniform vec3 u_Per_Fragment_lightspecular;" \
		"uniform vec3 u_Per_Fragment_materialambient;" \
		"uniform vec3 u_Per_Fragment_materialdiffuse;" \
		"uniform vec3 u_Per_Fragment_materialspecular;" \
		"uniform float Per_Fragment_materialshinines;" \
		"uniform int Per_Fragment_lKeyPress;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ADS_light;" \
		"if(Per_Fragment_lKeyPress==1)" \
		"{" \
		"vec3 normalizetnorm=normalize(Per_Fragment_tnorm);" \
		"vec3 normalizelightdirection=normalize(Per_Fragment_lightdirection);" \
		"vec3 normalizeviewervector=normalize(Per_Fragment_viewer_vector);" \
		"float tn_dot_ld=max(dot(normalizelightdirection,normalizetnorm),0.0);" \
		"vec3 reflection_vector=reflect(-normalizelightdirection,normalizetnorm);" \
		"vec3 ambient=u_Per_Fragment_lightambient*u_Per_Fragment_materialambient;" \
		"vec3 diffuse=u_Per_Fragment_lightdiffuse*u_Per_Fragment_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_Per_Fragment_lightspecular*u_Per_Fragment_materialspecular*pow(max(dot(reflection_vector,normalizeviewervector),0.0),Per_Fragment_materialshinines);" \
		"phong_ADS_light=ambient+diffuse+specular;" \
		"}"	\
		"else" \
		"{" \
		"phong_ADS_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"FragColor=vec4(phong_ADS_light,1.0);" \
		"}";



	//specify above shader source code to fragmentShaderObject
	//give shader source code
	glShaderSource(Per_Fragment_FragmentShaderObject, 1, (const GLchar * *)& Per_Fragment_fragmentShaderSourceCode, NULL);

	//COMpile fragment shader code
	glCompileShader(Per_Fragment_FragmentShaderObject);

	//error checking code for shader 
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(Per_Fragment_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(Per_Fragment_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(Per_Fragment_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf( "PERFragment Shader Compilation Log:%s\n", szInfoLog);
				free(szInfoLog);
				[self release];

			}
		}
	}

	//create shader program object
	Per_Fragment_ShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program 
	glAttachShader(Per_Fragment_ShaderProgramObject, Per_Fragment_VertexShaderObject);

	//attach fragment shader to  shader program 
	glAttachShader(Per_Fragment_ShaderProgramObject, Per_Fragment_FragmentShaderObject);

	glBindAttribLocation(Per_Fragment_ShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "Per_Fragment_vPosition");
	glBindAttribLocation(Per_Fragment_ShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "Per_Fragment_vNormal");

	//NOW Link shader program 
	glLinkProgram(Per_Fragment_ShaderProgramObject);

	//ERROR CHECKING FOR SHADER program 
	GLint iProgramLinkStaus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(Per_Fragment_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStaus);

	if (iProgramLinkStaus == GL_FALSE)
	{
		glGetProgramiv(Per_Fragment_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(Per_Fragment_ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link log:-%s\n", szInfoLog);
				free(szInfoLog);
				[self release];
			}
		}
	}

	Per_Fragment_modelMatrixUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_model_matrix");
	Per_Fragment_viewMatrixUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_view_matrix");
	Per_Fragment_projectionMatrixUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_projection_matrix");


	Per_Fragment_lightAmbientUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_lightambient");
	Per_Fragment_lightDiffuseUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_lightdiffuse");
	Per_Fragment_lightSpecularUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_lightspecular");

	Per_Fragment_materialAmbientUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_materialambient");
	Per_Fragment_materialDiffuseUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_materialdiffuse");
	Per_Fragment_materialSpecularUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_materialspecular");

	Per_Fragment_materialShininessUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "Per_Fragment_materialshinines");

	Per_Fragment_lightPositionUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_light_position");
	Per_Fragment_lKeyPressUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "Per_Fragment_lKeyPress");

}

void createSphere()
{
   // fprintf(gpFile,"Inside Create Sphere \n");
    maxElements=0;
    numElements=0;
    numVertices=0;
   processSphereData();
  
}

/*- (void)drawRect:(CGRect)rect
{
    
}*/

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    void draw();

    //code
    //to prevent screen tearing lock context
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
     [self draw];
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) draw
{
     //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix=vmath::mat4::identity();
    vmath::mat4 ViewMatrix=vmath::mat4::identity();
    
    modelMatrix=vmath::translate(0.0f,0.0f,-2.0f);

    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
   
   if (vKeyPress == true)
		{
			glUseProgram(gShaderProgramObject);
			if (bLight == true)
			{
				glUniform1i(lKeyPressUniform, 1);
	
				glUniform3fv(lightAmbientUniform, 1, lightAmbient);
				glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
				glUniform3fv(lightSpecularUniform, 1, lightSpecular);
				glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);

				glUniform3fv(materialAmbientUniform, 1, materialAmbient);
				glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
				glUniform3fv(materialSpecularUniform, 1, materialSpecular);

				glUniform1f(materialShininessUniform, materialShininess);
	

			}
			else
			{
				glUniform1i(lKeyPressUniform, 0);
			}
			glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
			glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
			glBindVertexArray(vaoSphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);
			glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
			glUseProgram(0);
		}

		if (fKeyPress == true)
		{
			glUseProgram(Per_Fragment_ShaderProgramObject);
			if (bLight == true)
			{
				glUniform1i(Per_Fragment_lKeyPressUniform, 1);

				glUniform3fv(Per_Fragment_lightAmbientUniform, 1, lightAmbient);
				glUniform3fv(Per_Fragment_lightDiffuseUniform, 1, lightDiffuse);
				glUniform3fv(Per_Fragment_lightSpecularUniform, 1, lightSpecular);
				glUniform4fv(Per_Fragment_lightPositionUniform, 1, (GLfloat*)lightPosition);

				glUniform3fv(Per_Fragment_materialAmbientUniform, 1, materialAmbient);
				glUniform3fv(Per_Fragment_materialDiffuseUniform, 1, materialDiffuse);
				glUniform3fv(Per_Fragment_materialSpecularUniform, 1, materialSpecular);
	
				glUniform1f(Per_Fragment_materialShininessUniform, materialShininess);
			}
			else
			{
			glUniform1i(Per_Fragment_lKeyPressUniform, 0);
			}
			glUniformMatrix4fv(Per_Fragment_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(Per_Fragment_viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
			glUniformMatrix4fv(Per_Fragment_projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
			glBindVertexArray(vaoSphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);
			glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
			glUseProgram(0);
		}

}


-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth=(GLfloat)width;
    GLfloat fheight=(GLfloat)height;

    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);

     
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf(" layoutSubviews Failed To Create Complete Framebuffer Object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameTimeInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}
//to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return YES;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    //code
    
}

-(void)onSinglTap:(UITapGestureRecognizer *)gr
{
    //code
    if (bLight == false)
    {
        bLight = true;
    }
    else
    {
        bLight = false;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    
    if(vKeyPress==true)
    {
        vKeyPress=false;
        fKeyPress=true;
    }
    else
    {
        vKeyPress=true;
        fKeyPress=false;
    }
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    //code
    [self release];
    exit(0);
}

-(void)dealloc
{

    if(vaoSphere)
    {
        glDeleteVertexArrays(1, &vaoSphere);
        vaoSphere=0;
    }
    
    if(vboSpherePosition)
    {
        glDeleteBuffers(1, &vboSpherePosition);
        vboSpherePosition=0;
    }

     if(vboSphereNormals)
    {
        glDeleteBuffers(1, &vboSphereNormals);
        vboSphereNormals=0;
    }

     if(vboSphereTexture)
    {
        glDeleteBuffers(1, &vboSphereTexture);
        vboSphereTexture=0;
    }

     if(vboSphereIndices)
    {
        glDeleteBuffers(1, &vboSphereIndices);
        vboSphereIndices=0;
    }
    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    

    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
