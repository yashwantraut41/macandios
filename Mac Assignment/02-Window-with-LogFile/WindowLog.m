
//
//  AppDelegate.m
//  window
//
//  Created by yashwant raut on 03/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

//headers
#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface MyView:NSView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    //fprintf(gpFile,"Inside main() \n");
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 MyView *view;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS Window with LogFile"];
    [window center];
    view=[[MyView alloc]initWithFrame:win_rect];

    [window setContentView:view];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
    fprintf(gpFile,"applicationDidFinishLaunching Complete \n");
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //fprintf(gpFile,"inside  windowWillClose()");
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [view release];
    [window release];
    [super dealloc];
}
@end

@implementation MyView
{
    NSString *centralText;

}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        centralText=@"Hello World!!!";
    }
    return(self);
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    //black background
    fprintf(gpFile,"inside drawRect() \n");
    NSColor *fillColor=[NSColor blackColor];
    [fillColor set];
    NSRectFill(dirtyRect);

    //dictionary with kvc
    NSDictionary *dictionaryForTextAttributes=[NSDictionary
                            dictionaryWithObjectsAndKeys:
                                            [NSFont fontWithName:@"Helvetica" size:32],NSFontAttributeName,
                                            [NSColor greenColor],NSForegroundColorAttributeName,nil];
    
    NSSize textSize=[centralText sizeWithAttributes:dictionaryForTextAttributes];
    NSPoint point;
    point.x=(dirtyRect.size.width/2)-(textSize.width/2);
    point.y=(dirtyRect.size.height/2)-(textSize.height/2)+12;
    
    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
    
}

-(BOOL)acceptsFirstResponder
{
    //code
    fprintf(gpFile,"Inside acceptsFirstResponder() \n");
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            centralText=@"'f' or 'F' Key is pressed";
            [[self window]toggleFullScreen:self];
            break;
            
            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    centralText=@"Left Mouse Button Is Clicked";
    [self setNeedsDisplay:YES];
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    centralText=@"Right Mouse Button Is Clicked";
    [self setNeedsDisplay:YES];
}

-(void)dealloc
{
    //code
    [super dealloc];
}
@end
