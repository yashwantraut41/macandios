//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;


enum
{
	AMC_ATRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};


//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);


//global declarations for animation
float translateXForI = -4.0f;
float translateYForI = -4.0f;
float translateYForILowerHorizontalLine = -6.0f;
float leftSlantX = 8.0f;
float rightSlantX = 7.0f;
float translateYForN = 4.0f;
float translatePlane = -5.5f;
float translatePlaneYA = 2.0f;
float translatePlaneYB = -2.0f;

int loadA = 0;
int loadN = 0;
int loadI = 0;
int loadD = 0;
int loadPlane = 0;
int loadFlag = 0;
float alphaColor = 1.0f;



//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,1000.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS DynamicIndia"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

	GLuint gVertexShaderObjectRectangle;
	GLuint gFragmentShaderObjectRectangle;
	GLuint gShaderProgramObjectRectangle;

	GLuint vaoRectangle;
	GLuint vboRectangle;
	GLuint vboRectangleColor;
    //for plane
	GLuint vaoPlaneRect;
	GLuint vboPlaneRect;

	GLuint vaoPlaneHead;
	GLuint vboPlaneHead;

	GLuint vaoPlaneWing;
	GLuint vboPlaneWing;

	GLuint vaoPlaneWingA;
	GLuint vboPlaneWingA;

	GLuint vaoPlaneTail;
	GLuint vboPlaneTail;

	GLuint vaoPlaneTailA;
	GLuint vboPlaneTailA;

	GLuint vaoFlagOrange;
	GLuint vboFlagOrange;
	GLuint vboFlagOrangeColor;

	GLuint vaoFlagGreen;
	GLuint vboFlagGreen;
	GLuint vboFlagGreenColor;

	GLuint vaoFlagWhite;
	GLuint vboFlagWhite;
	GLuint vboFlagWhiteColor;

	GLuint vaoHorizonatalLine;
    GLuint vaoVerticalLine;
    GLuint vaoLeftSlantLine;
    GLuint vaoRightSlantLine;

    GLuint vaoLeftSlantLineForD;
    GLuint vaoRightSlantLineForD;

    GLuint vboHorizontalLinePosition;
    GLuint vboVerticalLinePosition;
    GLuint vboLeftSlantLinePosition;
    GLuint vboRightSlantLinePosition;
    GLuint vboLeftSlantLinePositionForD;
    GLuint vboRightSlantLinePositionForD;

    GLuint vboHorizontalLineColor;
    GLuint vboLeftSlantLineForDColor;
    GLuint vboRightSlantLineForDColor;

    GLuint vboColor;
    GLuint mvpUniform;
	GLuint mvpUniformRectangle;


    vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    void initStaticIndiaShaders();
    void initStaticIndiaVaoVbos();
    [self initStaticIndiaShaders];
    [self initStaticIndiaVaoVbos];

    glClearDepth(1.0f);
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    //glEnable(GL_CULL_FACE);
    
    glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}

-(void) initStaticIndiaShaders
{
     gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_Color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_Color=vColor;" \
		"}";
    glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
        "out vec4 FragColor;" \
		"in vec4 out_Color;" \
		"void main(void)" \
		"{" \
		"FragColor=out_Color;" \
		"}";
    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
   
}
-(void) initStaticIndiaVaoVbos
{
	void shadersForRectangle();
    //vertices ,colors, shader attribs, vbo, vao initializations
    const GLfloat verticalLine[] =
	{
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};

	const GLfloat horizontalLine[] =
	{
		-0.7f,1.0f,0.0f,
		0.7f,1.0f,0.0f
	};

	const GLfloat leftSlantLine[] =
	{
		-0.5f,1.0f,0.0f,
		0.5f,-1.0f,0.0f
	};

	const GLfloat rightSlantLine[] =
	{
		0.5f,1.0f,0.0f,
		-0.5f,-1.0f,0.0f
	};

	const GLfloat leftSlantLineForD[] =
	{
		-0.5f,1.0f,0.0f,
		0.7f,0.5f,0.0f
	};

	const GLfloat rightSlantLineForD[] =
	{
		0.7f,-0.5f,0.0f,
		-0.5f,-1.0f,0.0f
	};

	const GLfloat color[] =
	{
		1.0f, 0.501f, 0.0f,
		0.0f, 0.501f, 0.0f
	};

	const GLfloat leftSlantLineForDColor[]=
	{
				1.0f, 0.501f, 0.0f,
				1.0f, 0.501f, 0.0f
	};
	
	const GLfloat rightSlantLineForDColor[]=
	{
				0.0f, 0.501f, 0.0f,
				0.0f, 0.501f, 0.0f
	};

    
    //create vao vertical line
	glGenVertexArrays(1, &vaoVerticalLine);
	glBindVertexArray(vaoVerticalLine);

	glGenBuffers(1, &vboVerticalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboVerticalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLine), verticalLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color 
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao horizontal line
	glGenVertexArrays(1, &vaoHorizonatalLine);
	glBindVertexArray(vaoHorizonatalLine);

	glGenBuffers(1, &vboHorizontalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLine), horizontalLine, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboHorizontalLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);

	glBufferData(GL_ARRAY_BUFFER, 3*2*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	
	glBindVertexArray(0);


	//create vao leftSlantLine
	glGenVertexArrays(1, &vaoLeftSlantLine);
	glBindVertexArray(vaoLeftSlantLine);

	glGenBuffers(1, &vboLeftSlantLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLine), leftSlantLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color 
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao leftSlantLineForD
	glGenVertexArrays(1, &vaoLeftSlantLineForD);
	glBindVertexArray(vaoLeftSlantLineForD);

	glGenBuffers(1, &vboLeftSlantLinePositionForD);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLinePositionForD);
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLineForD), leftSlantLineForD, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboLeftSlantLineForDColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLineForDColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLineForDColor),leftSlantLineForDColor , GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//create vao rightSlantLineForD
	glGenVertexArrays(1, &vaoRightSlantLineForD);
	glBindVertexArray(vaoRightSlantLineForD);

	glGenBuffers(1, &vboRightSlantLinePositionForD);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLinePositionForD);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLineForD), rightSlantLineForD, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboRightSlantLineForDColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLineForDColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLineForDColor), rightSlantLineForDColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//create vao for rightSlantLine
	glGenVertexArrays(1, &vaoRightSlantLine);
	glBindVertexArray(vaoRightSlantLine);

	glGenBuffers(1, &vboRightSlantLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLine), rightSlantLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//vao vbo and shaders for fading rectangle 
	[self shadersForRectangle];
	
	//vao and vbo for plane 
	GLfloat planeRect[] =
	{
		0.4f,0.1f,0.0f,
		-0.5f,0.1f,0.0f,
		-0.5f,-0.1f,0.0f,
		0.4f,-0.1f,0.0f
	};

	GLfloat planeHead[] =
	{
		 0.8f, 0.0f, 0.0f,
		 0.4f, 0.1f, 0.0f,
		 0.4f, -0.1f, 0.0f,
		 0.8f, 0.0f, 0.0f
	};




	

	//plane body
	glGenVertexArrays(1, &vaoPlaneRect);
	glBindVertexArray(vaoPlaneRect);
	glGenBuffers(1, &vboPlaneRect);
	glBindBuffer(GL_ARRAY_BUFFER, vboPlaneRect);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeRect), planeRect, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.729f, 0.886f, 0.921f);
	glBindVertexArray(0);


	//plane Head
	glGenVertexArrays(1, &vaoPlaneHead);
	glBindVertexArray(vaoPlaneHead);
	glGenBuffers(1, &vboPlaneHead);
	glBindBuffer(GL_ARRAY_BUFFER, vboPlaneHead);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeHead), planeHead, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.729f, 0.886f, 0.921f);
	glBindVertexArray(0);


	GLfloat planeUpperWing[] =
	{
		 0.1f, 0.4f, 0.0f ,
		 -0.1f, 0.4f, 0.0f ,
		-0.1f, 0.0f, 0.0f ,
		 0.4f, 0.0f, 0.0f
	};
	//plane Upper Wing
	glGenVertexArrays(1, &vaoPlaneWing);
	glBindVertexArray(vaoPlaneWing);
	glGenBuffers(1, &vboPlaneWing);
	glBindBuffer(GL_ARRAY_BUFFER, vboPlaneWing);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeUpperWing), planeUpperWing, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.729f, 0.886f, 0.921f);
	glBindVertexArray(0);

	GLfloat planeLowerWing[] =
	{
			 0.4f, 0.0f, 0.0f ,
			-0.1f, 0.0f, 0.0f,
			-0.1f, -0.4f, 0.0f ,
			0.1f, -0.4f, 0.0f ,
	};
	
	//plane Lower Wing
	glGenVertexArrays(1, &vaoPlaneWingA);
	glBindVertexArray(vaoPlaneWingA);
	glGenBuffers(1, &vboPlaneWingA);
	glBindBuffer(GL_ARRAY_BUFFER, vboPlaneWingA);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeLowerWing), planeLowerWing, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.729f, 0.886f, 0.921f);
	glBindVertexArray(0);


	GLfloat planeUpperTail[] =
	{
		 -0.4f, 0.4f, 0.0f ,
		 -0.5f, 0.4f, 0.0f ,
		-0.5f, 0.0f, 0.0f ,
		 -0.2f, 0.0f, 0.0f
	};

	//plane TAIL Upper
	glGenVertexArrays(1, &vaoPlaneTail);
	glBindVertexArray(vaoPlaneTail);
	glGenBuffers(1, &vboPlaneTail);
	glBindBuffer(GL_ARRAY_BUFFER, vboPlaneTail);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeUpperTail), planeUpperTail, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.729f, 0.886f, 0.921f);
	glBindVertexArray(0);
	
	GLfloat planeLowerTail[] =
	{
			 -0.2f, 0.0f, 0.0f ,
			-0.5f, 0.0f, 0.0f,
			-0.5f, -0.4f, 0.0f ,
			-0.4f, -0.4f, 0.0f ,

	};
	//plane Lower TAIL 
	glGenVertexArrays(1, &vaoPlaneTailA);
	glBindVertexArray(vaoPlaneTailA);
	glGenBuffers(1, &vboPlaneTailA);
	glBindBuffer(GL_ARRAY_BUFFER, vboPlaneTailA);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeLowerTail), planeLowerTail, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.729f, 0.886f, 0.921f);
	glBindVertexArray(0);


	//vao and vbo of FLAG

	//white flag
	GLfloat whiteFlag[] =
	{
		
		-0.5f,0.1f,0.0f,
		-0.9f,0.1f,0.0f,
		-0.9f,-0.1f,0.0f,
		-0.5f,-0.1f,0.0f
	};

	glGenVertexArrays(1, &vaoFlagWhite);
	glBindVertexArray(vaoFlagWhite);
	glGenBuffers(1, &vboFlagWhite);
	glBindBuffer(GL_ARRAY_BUFFER, vboFlagWhite);

	glBufferData(GL_ARRAY_BUFFER, sizeof(whiteFlag), whiteFlag, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
 
	GLfloat whiteFlagColor[] =
	{
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
	};
	glGenBuffers(1, &vboFlagWhiteColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboFlagWhiteColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(whiteFlagColor), whiteFlagColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



		//orange flag

		GLfloat orangeFlag[] =
		{
			-0.5f,0.3f,0.0f,
			-0.9f,0.3f,0.0f,
			-0.9f,0.1f,0.0f,
			-0.5f,0.1f,0.0f
		};

	glGenVertexArrays(1, &vaoFlagOrange);
	glBindVertexArray(vaoFlagOrange);
	glGenBuffers(1, &vboFlagOrange);
	glBindBuffer(GL_ARRAY_BUFFER, vboFlagOrange);

	glBufferData(GL_ARRAY_BUFFER, sizeof(orangeFlag), orangeFlag, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GLfloat orangeFlagColor[] =
	{
			1.0f, 0.501f, 0.0f,
			1.0f, 0.501f, 0.0f,
			1.0f, 0.501f, 0.0f,
			1.0f, 0.501f, 0.0f,
	};
	glGenBuffers(1, &vboFlagOrangeColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboFlagOrangeColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(orangeFlagColor), orangeFlagColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



	//GREEN FLAG
	//orange flag

	GLfloat greenFlag[] =
	{

		-0.5f,-0.1f,0.0f,
		-0.9f,-0.1f,0.0f,
		-0.9f,-0.3f,0.0f,
		-0.5f,-0.3f,0.0f
	};

	glGenVertexArrays(1, &vaoFlagGreen);
	glBindVertexArray(vaoFlagGreen);
	glGenBuffers(1, &vboFlagGreen);
	glBindBuffer(GL_ARRAY_BUFFER, vboFlagGreen);

	glBufferData(GL_ARRAY_BUFFER, sizeof(greenFlag), greenFlag, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GLfloat greenFlagColor[] =
	{
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f
	};
	glGenBuffers(1, &vboFlagGreenColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboFlagGreenColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(greenFlagColor), greenFlagColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);




}
-(void) shadersForRectangle
{
	void uninitialize();

	gVertexShaderObjectRectangle = glCreateShader(GL_VERTEX_SHADER);

	//vertex shader code 
	const  GLchar* vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vColor;" \
		"out vec3 out_Color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_Color=vColor;" \
		"}";

	//specify above shader source code to vertexShaderObject 
	//give shader source code
	glShaderSource(gVertexShaderObjectRectangle, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	//compile the vertex shader code 
	glCompileShader(gVertexShaderObjectRectangle);

	//error checking code for vertex shader 
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectRectangle, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectRectangle, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectRectangle, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
			}
		}
	}

	//WRITE fragment shader 
	gFragmentShaderObjectRectangle = glCreateShader(GL_FRAGMENT_SHADER);

	//shader code
	const GLchar* fragmentShaderSourceCode =
		"#version 410" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 out_Color;" \
		"uniform int loadD;" \
		"uniform float alphaColor;" \
		"void main(void)" \
		"{" \
		"if(loadD==1)"\
		"{"\
		"FragColor=vec4(out_Color,alphaColor);" \
		"}"
		"else" \
		"{" \
		"FragColor=vec4(out_Color,1.0);" \
		"}" \
		"}";

	//specify above shader source code to fragmentShaderObject
	//give shader source code
	glShaderSource(gFragmentShaderObjectRectangle, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);

	//COMpile fragment shader code
	glCompileShader(gFragmentShaderObjectRectangle);

	//error checking code for shader 
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObjectRectangle, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectRectangle, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectRectangle, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];

			}
		}
	}

	//create shader program object
	gShaderProgramObjectRectangle = glCreateProgram();

	//attach vertex shader to shader program 
	glAttachShader(gShaderProgramObjectRectangle, gVertexShaderObjectRectangle);

	//attach fragment shader to  shader program 
	glAttachShader(gShaderProgramObjectRectangle, gFragmentShaderObjectRectangle);

	glBindAttribLocation(gShaderProgramObjectRectangle, AMC_ATRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectRectangle, AMC_ATTRIBUTE_COLOR, "vColor");

	//NOW Link shader program 
	glLinkProgram(gShaderProgramObjectRectangle);


	//ERROR CHECKING FOR SHADER program 
	GLint iProgramLinkStaus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectRectangle, GL_LINK_STATUS, &iProgramLinkStaus);

	if (iProgramLinkStaus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectRectangle, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectRectangle, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shaderprogramobject   Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
			}
		}
	}

	mvpUniformRectangle = glGetUniformLocation(gShaderProgramObjectRectangle, "u_mvp_matrix");

	GLfloat rectangleVertices[] =
	{
		0.7f,1.0f,0.0f,
		-0.7f,1.0f,0.0f,
		-0.7f,-1.0f,0.0f,
		0.7f,-1.0f,0.0f
	};

	GLfloat rectangleColor[] =
	{
		0.0f,0.0f,0.0f,
		0.0f,0.0f,0.0f,
		0.0f,0.0f,0.0f,
		0.0f,0.0f,0.0f
	};
	glGenVertexArrays(1, &vaoRectangle);
	glBindVertexArray(vaoRectangle);

	glGenBuffers(1, &vboRectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vboRectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);


	glGenBuffers(1, &vboRectangleColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboRectangleColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleColor), rectangleColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindVertexArray(0);

}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}

-(void)drawView
{
    //code
    void draw();
	void update();

    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    [self draw];
   

    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    update();
    
}

-(void) draw
{
	//function declarations
	void drawFirstI(void);
	void drawN(void);
	void drawD(void);
	void drawI(void);
	void drawA(void);
	void drawFlag(void);
	void drawRectangle(void);
	void drawPlane(void);
	void drawFlag(void);
	void drawPlaneA();
	void drawPlaneB();
     //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    [self drawFirstI];
	[self drawA];
	[self drawN ];
	[self drawI];
	[self drawD];

	[self drawPlane];
	[self drawPlaneA];
	[self drawPlaneB];

	 
	if (loadFlag == 1)
	{
		[self drawFlag];
	}
	
	[self drawRectangle];
    //stop using openGL pROGRAM object
    glUseProgram(0);
}
-(void) drawRectangle
{

	glUseProgram(gShaderProgramObjectRectangle);
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	//vertical line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//TRANSLATION
	modelViewMatrix = translate(0.0f, 0.0f, -5.8f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniformRectangle, 1, GL_FALSE, modelViewProjectionMatrix);

	if (loadD == 1)
	{
		glUniform1i(glGetUniformLocation(gShaderProgramObjectRectangle, "loadD"), 1);
		glUniform1f(glGetUniformLocation(gShaderProgramObjectRectangle, "alphaColor"), alphaColor);
	}

	glBindVertexArray(vaoRectangle);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);
}
-(void) drawFirstI
{
	//declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	//vertical line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//TRANSLATION
	modelViewMatrix = translate(translateXForI, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//TRANSLATION
	modelViewMatrix = translate(translateXForI, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glLineWidth(255.0f);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	GLfloat colorHorizontalLine[] =
	{
		1.0f, 0.501f, 0.0f,
		1.0f, 0.501f, 0.0f
	};
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	//TRANSLATION
	modelViewMatrix = translate(translateXForI, -2.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(255.0f);
	//
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

}
-(void) drawN
{
	//VERTICAL LINE FOR N
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	//TRANSLATION
	modelViewMatrix = translate(-1.8f, translateYForN, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//leftSlantLine for N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	//TRANSLATION
	modelViewMatrix = translate(-1.3f, translateYForN, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//vertical line For N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	//TRANSLATION
	modelViewMatrix = translate(-0.8f, translateYForN, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

}
-(void) drawD
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	//vertical line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-0.5f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
	//leftslant line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	////bind with vao
	glBindVertexArray(vaoLeftSlantLineForD);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//rightSlant line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRightSlantLineForD);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


	//vertical line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -12.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}

-(void) drawI
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	GLfloat colorHorizontalLine[6];
	//for second I
	//vertical line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.5f, translateYForI, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.5f, translateYForI, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.5f, translateYForILowerHorizontalLine, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}

-(void) drawA
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	//////RIGHT SLANT LINE FOR A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//modelViewMatrix = translate(2.9f, 0.0f, -6.0f);
	modelViewMatrix = translate(rightSlantX, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRightSlantLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//LEFT SLANT LINE FOR A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//modelViewMatrix = translate(3.9f, 0.0f, -6.0f);
	modelViewMatrix = translate(leftSlantX, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}
-(void) drawFlag
{
	
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	GLfloat colorHorizontalLine[6];
	//flag Line Orange
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(5.1f, -1.0f, -9.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glLineWidth(255.0f);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line white
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.5f, -1.1f, -8.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 1.0f;
	colorHorizontalLine[2] = 1.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 1.0f;
	colorHorizontalLine[5] = 1.0f;
	glLineWidth(155.0f);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line Green
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.4f, -1.2f, -7.8f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;
	glLineWidth(155.0f);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}
-(void) drawPlane
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneRect);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//draw head
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneHead);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//plane upper wing
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneWing);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//plane Lower wing
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneWingA);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//plane upper tail

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneTail);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//plane Lower tail
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneTailA);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

		//Orange Flag Strip 
		

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
		//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glBindVertexArray(vaoFlagOrange);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glBindVertexArray(0);

		//white Flag Strip

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
		//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glBindVertexArray(vaoFlagWhite);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glBindVertexArray(0);

		//Green Flag Strip
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		modelViewMatrix = translate(translatePlane, 0.0f, -6.0f);
		//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniform
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glBindVertexArray(vaoFlagGreen);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glBindVertexArray(0);
	
}
-(void) drawPlaneA
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneRect);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//draw head
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneHead);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//plane upper wing
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneWing);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//plane Lower wing
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneWingA);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//plane upper tail

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneTail);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//plane Lower tail
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneTailA);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//Orange Flag Strip 


	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoFlagOrange);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//white Flag Strip

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoFlagWhite);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//Green Flag Strip
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYA, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoFlagGreen);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

}
-(void) drawPlaneB
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneRect);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//draw head
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneHead);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//plane upper wing
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneWing);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//plane Lower wing
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneWingA);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//plane upper tail

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneTail);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//plane Lower tail
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoPlaneTailA);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//Orange Flag Strip 


	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoFlagOrange);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//white Flag Strip

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoFlagWhite);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//Green Flag Strip
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(translatePlane, translatePlaneYB, -6.0f);
	//modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vaoFlagGreen);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

}
void update(void)
{
	//translation value update for anmation of I
	if (translateXForI <= -3.0f)
	{
		translateXForI = translateXForI + 0.01f;
	}
	else
	{
		loadA = 1;
	}


	 

	

	switch (loadA)
	{
	case 1:
		if (rightSlantX >= 2.9f) {
			rightSlantX = rightSlantX - 0.01f;
		}
		if (leftSlantX >= 3.9f) {
			leftSlantX = leftSlantX - 0.01f;
		}
		else {
			loadN = 2;
		}
		//translation value update for anmation of N
		switch (loadN)
		{
		case 2:
			if (translateYForN > 0.0f) {
				translateYForN = translateYForN - 0.01f;
			}
			else {
				loadI = 3;
			}
			//translation value update for anmation of N
			switch (loadI)
			{
			case 3:
				if (translateYForI < 0.0f)
				{
					translateYForI = translateYForI + 0.01f;
				}
				if (translateYForILowerHorizontalLine <= -2.0f)
				{
					translateYForILowerHorizontalLine = translateYForILowerHorizontalLine + 0.01f;
				}
				else
				{
					loadD = 1;
				}

				switch (loadD)
				{
				case 1:
					if (alphaColor >= 0.0)
					{
						alphaColor = alphaColor - 0.01f;
					}
					else
					{
						loadPlane = 1;
					}

					switch (loadPlane)
					{
					case 1:
						
						translatePlane = translatePlane + 0.01f;
						if (translatePlane >= 3.5f)
							loadFlag = 1;
						if (translatePlaneYA >= 0.0f)
						{
							translatePlaneYA = translatePlaneYA - 0.003f;
						}

						if (translatePlaneYB <= 0.0f)
						{
							translatePlaneYB = translatePlaneYB + 0.003f;
						}
						
						break;
					}
					break; // 1 case break 
				}
				break; //case 3 break
			}
			break; //case 2 break

		}
		break; //case1 break


	}


}
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            [[self window]toggleFullScreen:self];
            break;
            
            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
  if (vboColor)
	{
		glDeleteBuffers(1, &vboColor);
		vboColor = 0;
	}
	if (vboHorizontalLineColor)
	{
		glDeleteBuffers(1, &vboHorizontalLineColor);
		vboHorizontalLineColor = 0;
	}
	if (vboHorizontalLinePosition)
	{
		glDeleteBuffers(1, &vboHorizontalLinePosition);
		vboHorizontalLinePosition = 0;
	}

	if (vboVerticalLinePosition)
	{
		glDeleteBuffers(1, &vboVerticalLinePosition);
		vboVerticalLinePosition = 0;
	}

	if (vboLeftSlantLinePosition)
	{
		glDeleteBuffers(1, &vboLeftSlantLinePosition);
		vboLeftSlantLinePosition = 0;
	}

	if (vboLeftSlantLinePositionForD)
	{
		glDeleteBuffers(1, &vboLeftSlantLinePositionForD);
		vboLeftSlantLinePositionForD = 0;
	}

	if (vboRightSlantLinePosition)
	{
		glDeleteBuffers(1, &vboRightSlantLinePosition);
		vboRightSlantLinePosition = 0;
	}

	if (vboRightSlantLinePositionForD)
	{
		glDeleteBuffers(1, &vboRightSlantLinePositionForD);
		vboRightSlantLinePositionForD = 0;
	}

	if (vaoLeftSlantLine)
	{
		glDeleteVertexArrays(1, &vaoLeftSlantLine);
		vaoLeftSlantLine = 0;
	}
	if (vaoLeftSlantLineForD)
	{
		glDeleteVertexArrays(1, &vaoLeftSlantLineForD);
		vaoLeftSlantLineForD = 0;
	}

	if (vaoRightSlantLine)
	{
		glDeleteVertexArrays(1, &vaoRightSlantLine);
		vaoRightSlantLine = 0;
	}

	if (vaoRightSlantLineForD)
	{
		glDeleteVertexArrays(1, &vaoRightSlantLineForD);
		vaoRightSlantLineForD = 0;
	}

	if (vaoHorizonatalLine)
	{
		glDeleteVertexArrays(1, &vaoHorizonatalLine);
		vaoHorizonatalLine = 0;
	}

	if (vaoVerticalLine)
	{
		glDeleteVertexArrays(1, &vaoVerticalLine);
		vaoVerticalLine = 0;
	}

    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
