//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);

 GLfloat lightAmbient[4] = { 0.25f,0.25f,0.25f,0.25f };
    GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0,1.0f };
    GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
    float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };

    GLfloat materialAmbient[4] = { 0.25f,0.25f,0.25f,0.25f };
    GLfloat materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat materialShininess = 128.0f;

//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS interleave"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint samplerUniform;


//LIGHT Uniforms    
    GLuint lightDiffuseUniform;
    GLuint lightAmbientUniform;
    GLuint lightSpecularUniform;
    GLuint lightPositionUniform;

//Material Uniform
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;
    GLuint lKeyPressUniform;


    GLuint vaoCube;
    GLuint vboVCNT;

   



    //for texture
    GLuint texture_MARBLE;

    //for animation
    GLfloat angleCube;
    bool bLight;


    vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    //fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    //fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    void initInterleaveShadersAndVaoVbo();
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    [self initInterleaveShadersAndVaoVbo];
    
	bLight=false;

    glClearDepth(1.0f);
   
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    //glEnable(GL_CULL_FACE);
    
    glEnable(GL_TEXTURE_2D);
    
    //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}

-(void) initInterleaveShadersAndVaoVbo
{
    //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"in vec3 vNormal;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int lKeyPress;" \
		"out vec3 lightdirection;" \
		"out vec3 tnorm;" \
		"out vec3 viewer_vector;" \
		"out vec4 out_color;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
		"	if(lKeyPress==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;" \
		"tnorm=mat3(u_view_matrix*u_model_matrix)*vNormal;"\
		"lightdirection=vec3(u_light_position - eye_coordinates );" \
		"viewer_vector=vec3(-eye_coordinates.xyz);" \
		"}" \
		"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;" \
		"out_color=vColor;" \
		"out_TexCoord=vTexCoord;" \
		"}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
    "in vec3 lightdirection;" \
		"in vec3 tnorm;" \
		"in vec3 viewer_vector;" \
		"in vec2 out_TexCoord;" \
		"in vec4 out_color;" \
		"uniform sampler2D u_sampler;" \
		"uniform vec3 u_lightambient;" \
		"uniform vec3 u_lightdiffuse;" \
		"uniform vec3 u_lightspecular;" \
		"uniform vec3 u_materialambient;" \
		"uniform vec3 u_materialdiffuse;" \
		"uniform vec3 u_materialspecular;" \
		"uniform float materialshinines;" \
		"uniform int lKeyPress;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ADS_light;" \
		"vec4 tex=texture(u_sampler,out_TexCoord);" \
		"\n" \
		"if(lKeyPress==1)" \
		"{" \
		"vec3 normalizetnorm=normalize(tnorm);" \
		"vec3 normalizelightdirection=normalize(lightdirection);" \
		"vec3 normalizeviewervector=normalize(viewer_vector);" \
		"float tn_dot_ld=max(dot(normalizelightdirection,normalizetnorm),0.0);" \
		"vec3 reflection_vector=reflect(-normalizelightdirection,normalizetnorm);" \
		"vec3 ambient=u_lightambient*u_materialambient;" \
		"vec3 diffuse=u_lightdiffuse*u_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_lightspecular*u_materialspecular*pow(max(dot(reflection_vector,normalizeviewervector),0.0),materialshinines);" \
		"phong_ADS_light=ambient+diffuse+specular;" \
		"}"	\
		"else" \
		"{" \
		"phong_ADS_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"FragColor=vec4(vec3(tex)*vec3(out_color)*phong_ADS_light,1.0);" \
		"}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	
    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");


	lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightambient");
	lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightdiffuse");
	lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightspecular");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialspecular");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "materialshinines");

	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "lKeyPress");

	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

    texture_MARBLE=[self loadTextureFromBMPFile:"marble.bmp"];

    //vertices ,colors, shader attribs, vbo, vao initializations
    
    const GLfloat cubeVCNT[] =
	{
		1.0f,1.0f,-1.0f,
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,

		-1.0f,1.0f,-1.0f,
		1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,0.0f,

			-1.0f,1.0f,1.0f,
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,1.0f,
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			1.0f,1.0f,

			1.0f,-1.0f,-1.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f,
				1.0f,1.0f,

				-1.0f,-1.0f,-1.0f,
				0.0f,1.0f,0.0f,
				0.0f,-1.0f,0.0f,
				0.0f,1.0f,

				-1.0f,-1.0f,1.0f,
				0.0f,1.0f,0.0f,
				0.0f,-1.0f,0.0f,
				0.0f,0.0f,

				1.0f,-1.0f,1.0f,
				0.0f,1.0f,0.0f,
				0.0f,-1.0f,0.0f,
				1.0f,0.0f,

				1.0f,1.0f,1.0f,
				0.0f,0.0f,1.0f,
				0.0f,0.0f,1.0f,
				0.0f,0.0f,


				-1.0f,1.0f,1.0f,
				0.0f,0.0f,1.0f,
					0.0f,0.0f,1.0f,
					1.0f,0.0f,


		-1.0f,-1.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		1.0f,1.0f,

		1.0f,-1.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,1.0f,

		1.0f,1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		1.0f,0.0f,

		-1.0f,1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		1.0f,1.0f,

		-1.0f,-1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		0.0f,1.0f,

		1.0f,-1.0f,-1.0f,
		0.0f,1.0f,1.0f,
		0.0f,0.0f,-1.0f,
		0.0f,0.0f,

		1.0f,1.0f,-1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,


		1.0f,1.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,0.0f,
		1.0f,1.0f,

		1.0f,-1.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,0.0f,
			0.0f,1.0f,


			1.0f, -1.0f, -1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f,

			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
				0.0f, 0.0f,

				-1.0f, 1.0f, -1.0f,
				1.0f, 1.0f, 0.0f,
				-1.0f, 0.0f, 0.0f,
				1.0f, 0.0f,

				-1.0f, -1.0f, -1.0f,
				1.0f, 1.0f, 0.0f,
				-1.0f, 0.0f, 0.0f,
				1.0f, 1.0f,

				-1.0f, -1.0f, 1.0f,
				1.0f, 1.0f, 0.0f,
				-1.0f, 0.0f, 0.0f,
				0.0f, 1.0f

	};

	
	
    //create vao for cube
	glGenVertexArrays(1, &vaoCube);
	glBindVertexArray(vaoCube);

	//create vbo buffer  
	glGenBuffers(1, &vboVCNT);
	glBindBuffer(GL_ARRAY_BUFFER, vboVCNT);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVCNT), cubeVCNT, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*11, (void*)0);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 11, (void*)(3*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 11,(void*)(6*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 11, (void*)(9*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	//unbind cube  buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//unbind vao for cube
	glBindVertexArray(0);

}

-(GLuint)loadTextureFromBMPFile:(const char*)texFileName
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath,texFileName];
    NSImage *bmpImage=[[NSImage alloc]
            initWithContentsOfFile:textureFileNameWithPath];

    if(!bmpImage)
    {
        NSLog(@"cant find %@",textureFileNameWithPath);
        return(0);
    }

    CGImageRef cgImage=[bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider (cgImage));
    void* pixels=(void *)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1,&bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bmpTexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                0,
                 GL_RGBA,
                  w,
                  h,
                  0,
                  GL_RGBA,
                  GL_UNSIGNED_BYTE,
                  pixels);
                  
 //create mipmaps for this texture for better image quality
 glGenerateMipmap(GL_TEXTURE_2D);
 CFRelease(imageData);
 return(bmpTexture);

}
-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawView
{
    //code
    
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    
    void draw();
    [self draw];
   
    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    angleCube=angleCube+0.5f;
}

-(void) draw
{
        //start Using OpenGL program Object

     glUseProgram(gShaderProgramObject);
    

    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix=vmath::mat4::identity();
    vmath::mat4 ViewMatrix=vmath::mat4::identity();
    vmath::mat4 rotationMatrix=vmath::mat4::identity();
    vmath::mat4 translationMatrix=vmath::mat4::identity();
    

    translationMatrix=vmath::translate(0.0f,0.0f,-6.0f);
    rotationMatrix=vmath::rotate(angleCube,angleCube,angleCube);

    modelMatrix=translationMatrix*rotationMatrix;
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE,modelMatrix );
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE,ViewMatrix );
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,perspectiveProjectionMatrix );


    if (bLight == true)
	{
		glUniform1i(lKeyPressUniform, 1);
		glUniform3fv(lightAmbientUniform, 1, lightAmbient);
		glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
		glUniform3fv(lightSpecularUniform, 1, lightSpecular);
		
        glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
		
        glUniform3fv(materialAmbientUniform, 1, materialAmbient);
		glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
		glUniform3fv(materialSpecularUniform, 1, materialSpecular);
		
        glUniform1f(materialShininessUniform, materialShininess);
	}
	else
	{
		glUniform1i(lKeyPressUniform, 0);
	}



    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_MARBLE);
	glUniform1i(samplerUniform, 0);

	glBindVertexArray(vaoCube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

     //stop using openGL pROGRAM object
    glUseProgram(0);

   
}
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            [[self window]toggleFullScreen:self];
            break;

            case 'L':
            case 'l':
            if (bLight == false)
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
			break;
            
            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
   

   

    if(vaoCube)
    {
        glDeleteVertexArrays(1,&vaoCube);
        vaoCube=0;
    }

  

     if(vboVCNT)
    {
        glDeleteBuffers(1,&vboVCNT);
        vboVCNT=0;
    }

    if(texture_MARBLE)
    {
        glDeleteTextures(1,&texture_MARBLE);
        texture_MARBLE=0;
    }

    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
