//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"
#import "createSphere.h"
enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);

GLuint maxElements;
GLuint numElements;
GLuint numVertices;
   
GLfloat model_vertices[1146];
GLfloat model_normals[1146];
GLfloat model_textures[764];
GLushort model_elements[2280];

//global function declarations
void createSphere();
void addTriangle(float single_vertex[3][3] ,float single_normal[3][3], float single_texture[3][2]);
void normalizeVector(float v[3]);
Boolean isFoundIdentical( float val1,  float val2,  float diff);

//for perVertex
GLfloat lightAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0,1.0f };
GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess =  128.0f;

//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS perVertexPerFragment-Light-Sphere"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint Per_Fragment_VertexShaderObject;
    GLuint Per_Fragment_FragmentShaderObject;
    GLuint Per_Fragment_ShaderProgramObject;

    
    GLuint vaoSphere;
    GLuint vboSpherePosition;
    GLuint vboSphereNormals;
    GLuint vboSphereTexture;
    GLuint vboSphereIndices;
   
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;
    //LIGHT Uniforms
GLuint lightDiffuseUniform;
GLuint lightAmbientUniform;
GLuint lightSpecularUniform;
GLuint lightPositionUniform;
   //Material Uniform
GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShininessUniform;

GLuint lKeyPressUniform;

//for per fragment
GLuint  Per_Fragment_modelMatrixUniform;
GLuint  Per_Fragment_viewMatrixUniform;
GLuint Per_Fragment_projectionMatrixUniform;

//LIGHT Uniforms perfragment
GLuint Per_Fragment_lightDiffuseUniform;
GLuint Per_Fragment_lightAmbientUniform;
GLuint Per_Fragment_lightSpecularUniform;
GLuint Per_Fragment_lightPositionUniform;

//Material Uniform perfragment
GLuint Per_Fragment_materialAmbientUniform;
GLuint Per_Fragment_materialDiffuseUniform;
GLuint Per_Fragment_materialSpecularUniform;
GLuint Per_Fragment_materialShininessUniform;
GLuint Per_Fragment_lKeyPressUniform;

   


    bool bLight;
    bool vKeyPress ;
    bool fKeyPress ;
    vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    void perVertexLighting(void);
    void perFragmentLighting(void);
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    [self perVertexLighting];
	[self perFragmentLighting];
	
    //vertex Shader
   

    //create sphere
    createSphere();
    //vertices ,colors, shader attribs, vbo, vao initializations
      

    

    glGenVertexArrays(1, &vaoSphere);
    glBindVertexArray(vaoSphere);
    
    glGenBuffers(1, &vboSpherePosition);
    glBindBuffer(GL_ARRAY_BUFFER, vboSpherePosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vboSphereNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_normals), model_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vboSphereTexture);
    glBindBuffer(GL_ARRAY_BUFFER, vboSphereTexture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_textures), model_textures, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1,&vboSphereIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboSphereIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(model_elements),model_elements,GL_STATIC_DRAW);
    
    glBindVertexArray(0);
    
    glClearDepth(1.0f);
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    glEnable(GL_CULL_FACE);
    
    bLight=false;
     vKeyPress = true;
     fKeyPress = false;
    //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}

-(void) perVertexLighting
{
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 Per_Vertex_vPosition;" \
		"in vec3 Per_Vertex_vNormal;" \
		"uniform mat4 u_Per_Vertex_model_matrix ;" \
		"uniform mat4 u_Per_Vertex_view_matrix ;" \
		"uniform mat4 u_Per_Vertex_projection_matrix ;" \
		"uniform int u_Per_Vertex_lKeyPress ;" \
		"uniform vec3 u_Per_Vertex_lightambient ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse ;" \
		"uniform vec3 u_Per_Vertex_lightspecular ;" \
		"uniform vec4 u_Per_Vertex_light_position ;" \
		"uniform vec3 u_Per_Vertex_materialambient ;" \
		"uniform vec3 u_Per_Vertex_materialdiffuse ;" \
		"uniform vec3 u_Per_Vertex_materialspecular ;" \
		"uniform float u_Per_Vertex_material_shininess ;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_Per_Vertex_lKeyPress==1)"\
		"{"\
		"vec4 eye_coordinates=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"vec3 tnorm=normalize(mat3(u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix)*Per_Vertex_vNormal);" \
		"vec3 lightdirection=normalize(vec3(u_Per_Vertex_light_position-eye_coordinates));" \
		"float tn_dot_ld=max(dot(lightdirection,tnorm),0.0);" \
		"vec3 reflection_vector=reflect(-lightdirection,tnorm);" \
		"vec3 viewer_vector=normalize(vec3(- eye_coordinates.xyz));" \
		"vec3 ambient=u_Per_Vertex_lightambient*u_Per_Vertex_materialambient;" \
		"vec3 diffuse=u_Per_Vertex_lightdiffuse*u_Per_Vertex_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_Per_Vertex_lightspecular*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		"phong_ads_light=ambient+diffuse+specular;" \
		"}"\
		"else" \
		"{" \
		"phong_ads_light=vec3(1.0,1.0,1.0);" \
		"}"\
		"gl_Position=u_Per_Vertex_projection_matrix*u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"}";
	
   glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
    "in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor=vec4(phong_ads_light,1.0);" \
		"}";
    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "Per_Vertex_vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "Per_Vertex_vNormal");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_projection_matrix");


	lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient");
	lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse");
	lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialspecular");

	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_material_shininess");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lKeyPress");



}

-(void) perFragmentLighting
{
	Per_Fragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//vertex shader code 
	const  GLchar* Per_Fragment_vertexShaderSourceCode =
        "#version 410" \
		"\n" \
		"in vec4 Per_Fragment_vPosition;" \
		"in vec3 Per_Fragment_vNormal;" \
		"uniform mat4 u_Per_Fragment_model_matrix;" \
		"uniform mat4 u_Per_Fragment_view_matrix;" \
		"uniform mat4 u_Per_Fragment_projection_matrix;" \
		"uniform vec4 u_Per_Fragment_light_position;" \
		"uniform int Per_Fragment_lKeyPress;" \
		"out vec3 Per_Fragment_lightdirection;" \
		"out vec3 Per_Fragment_tnorm;" \
		"out vec3 Per_Fragment_viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(Per_Fragment_lKeyPress==1)" \
		"{" \
		"vec4 eye_coordinates=u_Per_Fragment_view_matrix*u_Per_Fragment_model_matrix*Per_Fragment_vPosition;" \
		"Per_Fragment_tnorm=mat3(u_Per_Fragment_view_matrix*u_Per_Fragment_model_matrix)*Per_Fragment_vNormal;"\
		"Per_Fragment_lightdirection=vec3(u_Per_Fragment_light_position - eye_coordinates );" \
		"Per_Fragment_viewer_vector=vec3(-eye_coordinates.xyz);" \
		"}" \
		"gl_Position=u_Per_Fragment_projection_matrix*u_Per_Fragment_view_matrix*u_Per_Fragment_model_matrix*Per_Fragment_vPosition;" \
		"}";


	//specify above shader source code to vertexShaderObject 
	//give shader source code
	glShaderSource(Per_Fragment_VertexShaderObject, 1, (const GLchar * *)& Per_Fragment_vertexShaderSourceCode, NULL);

	//compile the vertex shader code 
	glCompileShader(Per_Fragment_VertexShaderObject);

	//error checking code for vertex shader 
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;
	glGetShaderiv(Per_Fragment_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(Per_Fragment_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(Per_Fragment_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Fragment Vertex Shader Compilation Log :%s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	//WRITE fragment shader 
	Per_Fragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//shader code
	const GLchar* Per_Fragment_fragmentShaderSourceCode =
        "#version 410" \
		"\n" \
		"in vec3 Per_Fragment_lightdirection;" \
		"in vec3 Per_Fragment_tnorm;" \
		"in vec3 Per_Fragment_viewer_vector;" \
		"uniform vec3 u_Per_Fragment_lightambient;" \
		"uniform vec3 u_Per_Fragment_lightdiffuse;" \
		"uniform vec3 u_Per_Fragment_lightspecular;" \
		"uniform vec3 u_Per_Fragment_materialambient;" \
		"uniform vec3 u_Per_Fragment_materialdiffuse;" \
		"uniform vec3 u_Per_Fragment_materialspecular;" \
		"uniform float Per_Fragment_materialshinines;" \
		"uniform int Per_Fragment_lKeyPress;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ADS_light;" \
		"if(Per_Fragment_lKeyPress==1)" \
		"{" \
		"vec3 normalizetnorm=normalize(Per_Fragment_tnorm);" \
		"vec3 normalizelightdirection=normalize(Per_Fragment_lightdirection);" \
		"vec3 normalizeviewervector=normalize(Per_Fragment_viewer_vector);" \
		"float tn_dot_ld=max(dot(normalizelightdirection,normalizetnorm),0.0);" \
		"vec3 reflection_vector=reflect(-normalizelightdirection,normalizetnorm);" \
		"vec3 ambient=u_Per_Fragment_lightambient*u_Per_Fragment_materialambient;" \
		"vec3 diffuse=u_Per_Fragment_lightdiffuse*u_Per_Fragment_materialdiffuse*tn_dot_ld;" \
		"vec3 specular=u_Per_Fragment_lightspecular*u_Per_Fragment_materialspecular*pow(max(dot(reflection_vector,normalizeviewervector),0.0),Per_Fragment_materialshinines);" \
		"phong_ADS_light=ambient+diffuse+specular;" \
		"}"	\
		"else" \
		"{" \
		"phong_ADS_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"FragColor=vec4(phong_ADS_light,1.0);" \
		"}";



	//specify above shader source code to fragmentShaderObject
	//give shader source code
	glShaderSource(Per_Fragment_FragmentShaderObject, 1, (const GLchar * *)& Per_Fragment_fragmentShaderSourceCode, NULL);

	//COMpile fragment shader code
	glCompileShader(Per_Fragment_FragmentShaderObject);

	//error checking code for shader 
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(Per_Fragment_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(Per_Fragment_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(Per_Fragment_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "PERFragment Shader Compilation Log:%s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];

			}
		}
	}

	//create shader program object
	Per_Fragment_ShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program 
	glAttachShader(Per_Fragment_ShaderProgramObject, Per_Fragment_VertexShaderObject);

	//attach fragment shader to  shader program 
	glAttachShader(Per_Fragment_ShaderProgramObject, Per_Fragment_FragmentShaderObject);

	glBindAttribLocation(Per_Fragment_ShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "Per_Fragment_vPosition");
	glBindAttribLocation(Per_Fragment_ShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "Per_Fragment_vNormal");

	//NOW Link shader program 
	glLinkProgram(Per_Fragment_ShaderProgramObject);

	//ERROR CHECKING FOR SHADER program 
	GLint iProgramLinkStaus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(Per_Fragment_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStaus);

	if (iProgramLinkStaus == GL_FALSE)
	{
		glGetProgramiv(Per_Fragment_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(Per_Fragment_ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link log:-%s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];

			}
		}
	}

	Per_Fragment_modelMatrixUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_model_matrix");
	Per_Fragment_viewMatrixUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_view_matrix");
	Per_Fragment_projectionMatrixUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_projection_matrix");


	Per_Fragment_lightAmbientUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_lightambient");
	Per_Fragment_lightDiffuseUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_lightdiffuse");
	Per_Fragment_lightSpecularUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_lightspecular");

	Per_Fragment_materialAmbientUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_materialambient");
	Per_Fragment_materialDiffuseUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_materialdiffuse");
	Per_Fragment_materialSpecularUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_materialspecular");

	Per_Fragment_materialShininessUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "Per_Fragment_materialshinines");

	Per_Fragment_lightPositionUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "u_Per_Fragment_light_position");
	Per_Fragment_lKeyPressUniform = glGetUniformLocation(Per_Fragment_ShaderProgramObject, "Per_Fragment_lKeyPress");

}


void createSphere()
{
   // fprintf(gpFile,"Inside Create Sphere \n");
    maxElements=0;
    numElements=0;
    numVertices=0;

   
   
   processSphereData();
  
}



void processSphereData()
{
    
    //fprintf(gpFile,"Inside processSphereData()  \n");
     int numIndices = 760;
        maxElements = numIndices * 3;
        
        GLfloat vert[3][3]; // position co-ordinates of ONE triangle
        GLfloat norm[3][3]; // normal co-ordinates of ONE triangle
       GLfloat tex[3][2]; // texture co-ordinates of ONE triangle
        
        for (int i = 0; i < numIndices; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                vert[j][0] = vertices[indices[i][j]][0];
                vert[j][1] = vertices[indices[i][j]][1];
                vert[j][2] = vertices[indices[i][j]][2];
                
                norm[j][0] = normals[indices[i][j + 3]][0];
                norm[j][1] = normals[indices[i][j + 3]][1];
                norm[j][2] = normals[indices[i][j + 3]][2];
                
                tex[j][0] = textures[indices[i][j + 6]][0];
                tex[j][1] = textures[indices[i][j + 6]][1];
            }
            addTriangle(vert, norm, tex);
        }


}


void addTriangle(float single_vertex[3][3] ,float single_normal[3][3], float single_texture[3][2])
{
     //variable declarations
   // fprintf(gpFile,"Inside addTriangle()  \n");

        const float diff = 0.00001f;
        int i, j;
        
        // code
        // normals should be of unit length
        normalizeVector(single_normal[0]);
        normalizeVector(single_normal[1]);
        normalizeVector(single_normal[2]);

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
        {
                   if (isFoundIdentical(model_vertices[j * 3], single_vertex[i][0], diff) &&
                    isFoundIdentical(model_vertices[(j * 3) + 1], single_vertex[i][1], diff) &&
                    isFoundIdentical(model_vertices[(j * 3) + 2], single_vertex[i][2], diff) &&
                    
                    isFoundIdentical(model_normals[j * 3], single_normal[i][0], diff) &&
                    isFoundIdentical(model_normals[(j * 3) + 1], single_normal[i][1], diff) &&
                    isFoundIdentical(model_normals[(j * 3) + 2], single_normal[i][2], diff) &&
                    
                    isFoundIdentical(model_textures[j * 2], single_texture[i][0], diff) &&
                    isFoundIdentical(model_textures[(j * 2) + 1], single_texture[i][1], diff))
                {
                    model_elements[numElements] = (short)j;
                    numElements++;
                    break;
                }
         }

            //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
            if (j == numVertices && numVertices < maxElements && numElements < maxElements)
            {
                model_vertices[numVertices * 3] = single_vertex[i][0];
                model_vertices[(numVertices * 3) + 1] = single_vertex[i][1];
                model_vertices[(numVertices * 3) + 2] = single_vertex[i][2];
                
                model_normals[numVertices * 3] = single_normal[i][0];
                model_normals[(numVertices * 3) + 1] = single_normal[i][1];
                model_normals[(numVertices * 3) + 2] = single_normal[i][2];
                
                model_textures[numVertices * 2] = single_texture[i][0];
                model_textures[(numVertices * 2) + 1] = single_texture[i][1];
                
                model_elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
                numElements++; //incrementing the 'end' of the list
                numVertices++; //incrementing coun of vertices
            }
    }
}

 Boolean isFoundIdentical( float val1,  float val2,  float diff)
 {
        // code
        //fprintf(gpFile,"Inside isFoundIdentical()  \n");

        if(abs(val1 - val2) < diff)
            return(true);
        else
            return(false);
 }

void normalizeVector(float v[3])
{
        // code
        // fprintf(gpFile,"Inside normalizeVector()  \n");

        // square the vector length
        float squaredVectorLength=(v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
        
        // get square root of above 'squared vector length'
        float squareRootOfSquaredVectorLength=(float)sqrt(squaredVectorLength);
        
        // scale the vector with 1/squareRootOfSquaredVectorLength
        v[0] = v[0] * 1.0f/squareRootOfSquaredVectorLength;
        v[1] = v[1] * 1.0f/squareRootOfSquaredVectorLength;
        v[2] = v[2] * 1.0f/squareRootOfSquaredVectorLength;
}
    

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix=vmath::mat4::identity();
    vmath::mat4 ViewMatrix=vmath::mat4::identity();
    
    modelMatrix=vmath::translate(0.0f,0.0f,-2.0f);

    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
   
   if (vKeyPress == true)
		{
			glUseProgram(gShaderProgramObject);
			if (bLight == true)
			{
				glUniform1i(lKeyPressUniform, 1);
	
				glUniform3fv(lightAmbientUniform, 1, lightAmbient);
				glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
				glUniform3fv(lightSpecularUniform, 1, lightSpecular);
				glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);

				glUniform3fv(materialAmbientUniform, 1, materialAmbient);
				glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
				glUniform3fv(materialSpecularUniform, 1, materialSpecular);

				glUniform1f(materialShininessUniform, materialShininess);
	

			}
			else
			{
				glUniform1i(lKeyPressUniform, 0);
			}
			glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
			glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
			glBindVertexArray(vaoSphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);
			glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
			glUseProgram(0);
		}

		if (fKeyPress == true)
		{
			glUseProgram(Per_Fragment_ShaderProgramObject);
			if (bLight == true)
			{
				glUniform1i(Per_Fragment_lKeyPressUniform, 1);

				glUniform3fv(Per_Fragment_lightAmbientUniform, 1, lightAmbient);
				glUniform3fv(Per_Fragment_lightDiffuseUniform, 1, lightDiffuse);
				glUniform3fv(Per_Fragment_lightSpecularUniform, 1, lightSpecular);
				glUniform4fv(Per_Fragment_lightPositionUniform, 1, (GLfloat*)lightPosition);

				glUniform3fv(Per_Fragment_materialAmbientUniform, 1, materialAmbient);
				glUniform3fv(Per_Fragment_materialDiffuseUniform, 1, materialDiffuse);
				glUniform3fv(Per_Fragment_materialSpecularUniform, 1, materialSpecular);
	
				glUniform1f(Per_Fragment_materialShininessUniform, materialShininess);
			}
			else
			{
			glUniform1i(Per_Fragment_lKeyPressUniform, 0);
			}
			glUniformMatrix4fv(Per_Fragment_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(Per_Fragment_viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
			glUniformMatrix4fv(Per_Fragment_projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
			glBindVertexArray(vaoSphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);
			glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
			glUseProgram(0);
		}


    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [[self window]toggleFullScreen:self];
            break;
            
        case 'Q':
		case 'q':
            [self release];
            [NSApp terminate:self];
            break;

            case 'F':
            case 'f':
            if (fKeyPress == false)
			{
				fKeyPress = true;
				vKeyPress = false;
			}
			else
			{
				fKeyPress = false;
				vKeyPress = true;
			}
            break;
            
            case 'L':
            case 'l':
            if (bLight == false)
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
            break;

            case 'V':
		    case 'v':
			if (vKeyPress == false)
			{
				vKeyPress = true;
				fKeyPress = false;
			}
			else
			{
				vKeyPress = false;
				fKeyPress = true;
			}
			break;

            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
    if(vaoSphere)
    {
        glDeleteVertexArrays(1, &vaoSphere);
        vaoSphere=0;
    }
    
    if(vboSpherePosition)
    {
        glDeleteBuffers(1, &vboSpherePosition);
        vboSpherePosition=0;
    }

     if(vboSphereNormals)
    {
        glDeleteBuffers(1, &vboSphereNormals);
        vboSphereNormals=0;
    }

     if(vboSphereTexture)
    {
        glDeleteBuffers(1, &vboSphereTexture);
        vboSphereTexture=0;
    }

     if(vboSphereIndices)
    {
        glDeleteBuffers(1, &vboSphereIndices);
        vboSphereIndices=0;
    }
    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
