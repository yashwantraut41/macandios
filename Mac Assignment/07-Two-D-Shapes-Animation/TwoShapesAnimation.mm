//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);



//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS Two-Shapes-Animation"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao;
    GLuint vbo;
    GLuint vboTriangleColor;


    GLuint vaoRectangle;
    GLuint vboRectangle;
    GLuint mvpUniform;

    GLfloat angleRectangle;
    GLfloat angleTriangle;


    vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    //vertex Shader
    //create shader
    vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
    "out vec4 out_color;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color=vColor;" \
    "}";
    glShaderSource(vertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
    "in vec4 out_color;" \
    "out vec4 FragColor;"\
    "void main(void)" \
    "{" \
        "FragColor=out_color;" \
    "}";
    //provide fragment shader source code to fragement shader
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    shaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

    //link shader
    glLinkProgram(shaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
   mvpUniform=glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    
    //vertices ,colors, shader attribs, vbo, vao initializations
    const GLfloat triangleVertices[]=
    {
        0.0f,0.5f,0.0f,
        -0.5f,-0.5f,0.0f,
        0.5f,-0.5f,0.0f
    };
    const GLfloat triangleColor[]=
    {
        1.0f,0.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,0.0f,1.0f
    };
    const GLfloat rectangleVertices[]=
    {
        0.5f,0.5f,0.0f,
        -0.5f,0.5f,0.0f,
        -0.5f,-0.5f,0.0f,
        0.5f,-0.5f,0.0f
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    //triangle Position
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
     //trinagle color 
    glGenBuffers(1,&vboTriangleColor);
    glBindBuffer(GL_ARRAY_BUFFER,vboTriangleColor);

    glBufferData(GL_ARRAY_BUFFER,sizeof(triangleColor),triangleColor,GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER,0);

   
    glBindVertexArray(0);

    //VBO For Rectangle 
    glGenVertexArrays(1,&vaoRectangle);
    glBindVertexArray(vaoRectangle);

    //rectangle Position    
    glGenBuffers(1,&vboRectangle);
    glBindBuffer(GL_ARRAY_BUFFER,vboRectangle);
    
    glBufferData(GL_ARRAY_BUFFER,sizeof(rectangleVertices),rectangleVertices,GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    
    //rectangle Color 
   glVertexAttrib3f(AMC_ATTRIBUTE_COLOR,0.0f,0.0f,1.0f);

    glBindVertexArray(0);   
    
    
    glClearDepth(1.0f);
   
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    //glEnable(GL_CULL_FACE);
    
    
    //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
}

-(void)drawView
{
    //code
    
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //start Using OpenGL program Object
    glUseProgram(shaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix=vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix=vmath::mat4::identity();
    vmath::mat4 rotationMatrix=vmath::mat4::identity();
    vmath::mat4 translationMatrix=vmath::mat4::identity();
    

    translationMatrix=vmath::translate(-0.6f,0.0f,-2.0f);
    rotationMatrix=vmath::rotate(angleRectangle,0.0f,1.0f,0.0f);

    modelViewMatrix=translationMatrix*rotationMatrix;
    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    //modelViewProjectionMatrix=modelViewMatrix*rotationMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind vao
    glBindVertexArray(vao);
    //draw eith by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 3);
    //UNBIND VAO
    glBindVertexArray(0);


    //For Rectangle 
    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    rotationMatrix=vmath::mat4::identity();
    translationMatrix=vmath::mat4::identity();
   
    translationMatrix=vmath::translate(0.6f,0.0f,-2.0f);
    rotationMatrix=vmath::rotate(angleTriangle,0.0f,1.0f,0.0f);
    modelViewMatrix=translationMatrix*rotationMatrix;

    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind vao
    glBindVertexArray(vaoRectangle);
    
    //draw eith by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    //UNBIND VAO
    glBindVertexArray(0);

    //stop using openGL pROGRAM object
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    [self update];
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}

-(void)update
{
    //fprintf(gpFile,"Inside Update Function \n");
    if(angleTriangle<=360.0f && angleRectangle<=360.0f)
    {
        angleTriangle=angleTriangle+1.0f;
        angleRectangle=angleRectangle+1.0f;
    }
    else
    {
        angleTriangle=0.0f;
        angleRectangle=0.0f;
    }
}
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            [[self window]toggleFullScreen:self];
            break;
            
            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
    if(vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao=0;
    }
    
    if(vaoRectangle)
    {
        glDeleteVertexArrays(1,&vaoRectangle);
        vaoRectangle=0;
    }

    if(vbo)
    {
        glDeleteBuffers(1, &vbo);
        vbo=0;
    }
    
    if(vboRectangle)
    {
        glDeleteBuffers(1,&vboRectangle);
        vboRectangle=0;
    }

    //detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
