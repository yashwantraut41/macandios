//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"
#import "createSphere.h"
enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);

GLuint maxElements;
GLuint numElements;
GLuint numVertices;
   
GLfloat model_vertices[1146];
GLfloat model_normals[1146];
GLfloat model_textures[764];
GLushort model_elements[2280];

//global function declarations
void createSphere();
void addTriangle(float single_vertex[3][3] ,float single_normal[3][3], float single_texture[3][2]);
void normalizeVector(float v[3]);
Boolean isFoundIdentical( float val1,  float val2,  float diff);

//for perVertex
/*GLfloat lightAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0,1.0f };
GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };*/

GLfloat materialAmbient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;
GLfloat lightAngleZero = 0.0f;
GLfloat lightAngleOne = 0.0f;
GLfloat lightAngleTwo = 0.0f;
//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS threeLightSphere"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    
    GLuint vaoSphere;
    GLuint vboSpherePosition;
    GLuint vboSphereNormals;
    GLuint vboSphereTexture;
    GLuint vboSphereIndices;
   
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;


    GLuint rotationalMatrixUniformred;
    GLuint rotationalMatrixUniformgreen;
    GLuint rotationalMatrixUniformblue;

    //LIGHT Uniforms
    GLuint lightDiffuseUniformRedLight;
    GLuint lightAmbientUniformRedLight;
    GLuint lightSpecularUniformRedLight;
    GLuint lightPositionUniformRedLight;

    GLuint lightDiffuseUniformBlueLight;
    GLuint lightAmbientUniformBlueLight;
    GLuint lightSpecularUniformBlueLight;
    GLuint lightPositionUniformBlueLight;

    GLuint lightDiffuseUniformGreenLight;
    GLuint lightAmbientUniformGreenLight;
    GLuint lightSpecularUniformGreenLight;
    GLuint lightPositionUniformGreenLight;
   //Material Uniform
   GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;
    GLuint lKeyPressUniform;

     struct Light
    {
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
    }light[3];
   
  


    bool bLight;
    vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    //initialize light parameteres for red light

    light[0].Ambient[0] = 0.0f;
	light[0].Ambient[1] = 0.0f;
	light[0].Ambient[2] = 0.0f;
	light[0].Ambient[3] = 0.0f;

	light[0].Diffuse[0] = 1.0f;
	light[0].Diffuse[1] = 0.0f;
	light[0].Diffuse[2] = 0.0f;
	light[0].Diffuse[3] = 1.0f;

	light[0].Specular[0] = 1.0f;
	light[0].Specular[1] = 0.0f;
	light[0].Specular[2] = 0.0f;
	light[0].Specular[3] = 1.0f;

	light[0].Position[0] = 0.0f;
	light[0].Position[1] = 0.0f;
	light[0].Position[2] = 0.0f;
	light[0].Position[3] = 1.0f;

	//initialize light parameteres for Green light
	light[1].Ambient[0] = 0.0f;
	light[1].Ambient[1] = 0.0f;
	light[1].Ambient[2] = 0.0f;
	light[1].Ambient[3] = 0.0f;

	light[1].Diffuse[0] = 0.0f;
	light[1].Diffuse[1] = 1.0f;
	light[1].Diffuse[2] = 0.0f;
	light[1].Diffuse[3] = 1.0f;

	light[1].Specular[0] = 0.0f;
	light[1].Specular[1] = 1.0f;
	light[1].Specular[2] = 0.0f;
	light[1].Specular[3] = 1.0f;

	light[1].Position[0] = 0.0f;
	light[1].Position[1] = 0.0f;
	light[1].Position[2] = 0.0f;
	light[1].Position[3] = 1.0f;

	//initialize light parameteres for Blue light
	light[2].Ambient[0] = 0.0f;
	light[2].Ambient[1] = 0.0f;
	light[2].Ambient[2] = 0.0f;
	light[2].Ambient[3] = 0.0f;

	light[2].Diffuse[0] = 0.0f;
	light[2].Diffuse[1] = 0.0f;
	light[2].Diffuse[2] = 1.0f;
	light[2].Diffuse[3] = 1.0f;

	light[2].Specular[0] = 0.0f;
	light[2].Specular[1] = 0.0f;
	light[2].Specular[2] = 1.0f;
	light[2].Specular[3] = 1.0f;

	light[2].Position[0] = 0.0f;
	light[2].Position[1] = 0.0f;
	light[2].Position[2] = 0.0f;
	light[2].Position[3] = 1.0f;

    //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 Per_Vertex_vPosition;" \
		"in vec3 Per_Vertex_vNormal;" \
		"uniform mat4 u_Per_Vertex_model_matrix ;" \
		"uniform mat4 u_Per_Vertex_view_matrix ;" \
		"uniform mat4 u_Per_Vertex_projection_matrix ;" \
		"uniform mat4 u_Per_Vertex_rotational_matrix_red ;" \
		"uniform mat4 u_Per_Vertex_rotational_matrix_green ;" \
		"uniform mat4 u_Per_Vertex_rotational_matrix_blue ;" \
		"uniform int u_Per_Vertex_lKeyPress ;" \
		"uniform vec3 u_Per_Vertex_lightambient_red ;" \
		"uniform vec3 u_Per_Vertex_lightambient_blue ;" \
		"uniform vec3 u_Per_Vertex_lightambient_green ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse_red ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse_blue ;" \
		"uniform vec3 u_Per_Vertex_lightdiffuse_green ;" \
		"uniform vec3 u_Per_Vertex_lightspecular_red ;" \
		"uniform vec3 u_Per_Vertex_lightspecular_blue ;" \
		"uniform vec3 u_Per_Vertex_lightspecular_green ;" \
		"uniform vec4 u_Per_Vertex_light_position_red ;" \
		"uniform vec4 u_Per_Vertex_light_position_blue ;" \
		"uniform vec4 u_Per_Vertex_light_position_green ;" \
		"uniform vec3 u_Per_Vertex_materialambient ;" \
		"uniform vec3 u_Per_Vertex_materialdiffuse ;" \
		"uniform vec3 u_Per_Vertex_materialspecular ;" \
		"uniform float u_Per_Vertex_material_shininess ;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_Per_Vertex_lKeyPress==1)"\
		"{"\
		"vec4 eye_coordinates=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"vec4 eye_coordinatesred=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition*u_Per_Vertex_rotational_matrix_red;" \
		"vec4 eye_coordinatesgreen=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition*u_Per_Vertex_rotational_matrix_green;" \
		"vec4 eye_coordinatesblue=u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition*u_Per_Vertex_rotational_matrix_blue;" \
		"vec3 tnorm=normalize(mat3(u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix)*Per_Vertex_vNormal);" \
		"vec4 rotationalLightPositionred=u_Per_Vertex_light_position_red*u_Per_Vertex_rotational_matrix_red;"\
		"vec4 rotationalLightPositiongreen=u_Per_Vertex_light_position_green*u_Per_Vertex_rotational_matrix_green;"\
		"vec4 rotationalLightPositionblue=u_Per_Vertex_light_position_blue*u_Per_Vertex_rotational_matrix_blue;"\
		"vec3 lightdirection_red=normalize(vec3(rotationalLightPositionred-eye_coordinatesred));" \
		"vec3 lightdirection_blue=normalize(vec3(rotationalLightPositionblue-eye_coordinatesblue));" \
		"vec3 lightdirection_green=normalize(vec3(rotationalLightPositiongreen-eye_coordinatesgreen));" \
		"float tn_dot_ld_red=max(dot(lightdirection_red,tnorm),0.0);" \
		"float tn_dot_ld_blue=max(dot(lightdirection_blue,tnorm),0.0);" \
		"float tn_dot_ld_green=max(dot(lightdirection_green,tnorm),0.0);" \
		" vec3 reflection_vector_red=reflect(-lightdirection_red,tnorm);" \
		" vec3 reflection_vector_blue=reflect(-lightdirection_blue,tnorm);" \
		" vec3 reflection_vector_green=reflect(-lightdirection_green,tnorm);" \
		" vec3 viewer_vector=normalize(vec3(- eye_coordinates.xyz));" \
		" vec3 ambient=u_Per_Vertex_lightambient_red*u_Per_Vertex_materialambient;" \
		" vec3 diffuse=u_Per_Vertex_lightdiffuse_red*u_Per_Vertex_materialdiffuse*tn_dot_ld_red;" \
		" vec3 specular=u_Per_Vertex_lightspecular_red*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector_red,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		" vec3 ambientblue=u_Per_Vertex_lightambient_blue*u_Per_Vertex_materialambient;" \
		" vec3 diffuseblue=u_Per_Vertex_lightdiffuse_blue*u_Per_Vertex_materialdiffuse*tn_dot_ld_blue;" \
		" vec3 specularblue=u_Per_Vertex_lightspecular_blue*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector_blue,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		" vec3 ambientgreen=u_Per_Vertex_lightambient_green*u_Per_Vertex_materialambient;" \
		" vec3 diffusegreen=u_Per_Vertex_lightdiffuse_green*u_Per_Vertex_materialdiffuse*tn_dot_ld_green;" \
		" vec3 speculargreen=u_Per_Vertex_lightspecular_green*u_Per_Vertex_materialspecular*pow(max(dot(reflection_vector_green,viewer_vector),0.0),u_Per_Vertex_material_shininess);" \
		"phong_ads_light=ambient+ambientblue+ambientgreen+diffuse+diffusegreen+diffuseblue+specular+specularblue+speculargreen;" \
		"}"\
		"else" \
		"{" \
		"phong_ads_light=vec3(1.0,1.0,1.0);" \
		"}"\
		"gl_Position=u_Per_Vertex_projection_matrix*u_Per_Vertex_view_matrix*u_Per_Vertex_model_matrix*Per_Vertex_vPosition;" \
		"}";
	
   glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
    "in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor=vec4(phong_ads_light,1.0);" \
		"}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "Per_Vertex_vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "Per_Vertex_vNormal");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_projection_matrix");
	rotationalMatrixUniformred = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_rotational_matrix_red");
	rotationalMatrixUniformgreen = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_rotational_matrix_green");
	rotationalMatrixUniformblue = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_rotational_matrix_blue");


	lightAmbientUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient_red");
	lightDiffuseUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse_red");
	lightSpecularUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular_red");

	lightAmbientUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient_blue");
	lightDiffuseUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse_blue");
	lightSpecularUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular_blue");

	lightAmbientUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightambient_green");
	lightDiffuseUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightdiffuse_green");
	lightSpecularUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lightspecular_green");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialambient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialdiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_materialspecular");

	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_material_shininess");

	lightPositionUniformRedLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position_red");
	lightPositionUniformBlueLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position_blue");
	lightPositionUniformGreenLight = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_light_position_green");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_Per_Vertex_lKeyPress");


    //create sphere
    createSphere();
    //vertices ,colors, shader attribs, vbo, vao initializations
      

    

    glGenVertexArrays(1, &vaoSphere);
    glBindVertexArray(vaoSphere);
    
    glGenBuffers(1, &vboSpherePosition);
    glBindBuffer(GL_ARRAY_BUFFER, vboSpherePosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vboSphereNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboSphereNormals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_normals), model_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vboSphereTexture);
    glBindBuffer(GL_ARRAY_BUFFER, vboSphereTexture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(model_textures), model_textures, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1,&vboSphereIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboSphereIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(model_elements),model_elements,GL_STATIC_DRAW);
    
    glBindVertexArray(0);
    
    glClearDepth(1.0f);
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    glEnable(GL_CULL_FACE);
    
    bLight=false;
    //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}


void createSphere()
{
   // fprintf(gpFile,"Inside Create Sphere \n");
    maxElements=0;
    numElements=0;
    numVertices=0;
   processSphereData();
  
}



void processSphereData()
{
    
    //fprintf(gpFile,"Inside processSphereData()  \n");
     int numIndices = 760;
        maxElements = numIndices * 3;
        
        GLfloat vert[3][3]; // position co-ordinates of ONE triangle
        GLfloat norm[3][3]; // normal co-ordinates of ONE triangle
       GLfloat tex[3][2]; // texture co-ordinates of ONE triangle
        
        for (int i = 0; i < numIndices; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                vert[j][0] = vertices[indices[i][j]][0];
                vert[j][1] = vertices[indices[i][j]][1];
                vert[j][2] = vertices[indices[i][j]][2];
                
                norm[j][0] = normals[indices[i][j + 3]][0];
                norm[j][1] = normals[indices[i][j + 3]][1];
                norm[j][2] = normals[indices[i][j + 3]][2];
                
                tex[j][0] = textures[indices[i][j + 6]][0];
                tex[j][1] = textures[indices[i][j + 6]][1];
            }
            addTriangle(vert, norm, tex);
        }


}


void addTriangle(float single_vertex[3][3] ,float single_normal[3][3], float single_texture[3][2])
{
     //variable declarations
   // fprintf(gpFile,"Inside addTriangle()  \n");

        const float diff = 0.00001f;
        int i, j;
        
        // code
        // normals should be of unit length
        normalizeVector(single_normal[0]);
        normalizeVector(single_normal[1]);
        normalizeVector(single_normal[2]);

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
        {
                   if (isFoundIdentical(model_vertices[j * 3], single_vertex[i][0], diff) &&
                    isFoundIdentical(model_vertices[(j * 3) + 1], single_vertex[i][1], diff) &&
                    isFoundIdentical(model_vertices[(j * 3) + 2], single_vertex[i][2], diff) &&
                    
                    isFoundIdentical(model_normals[j * 3], single_normal[i][0], diff) &&
                    isFoundIdentical(model_normals[(j * 3) + 1], single_normal[i][1], diff) &&
                    isFoundIdentical(model_normals[(j * 3) + 2], single_normal[i][2], diff) &&
                    
                    isFoundIdentical(model_textures[j * 2], single_texture[i][0], diff) &&
                    isFoundIdentical(model_textures[(j * 2) + 1], single_texture[i][1], diff))
                {
                    model_elements[numElements] = (short)j;
                    numElements++;
                    break;
                }
         }

            //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
            if (j == numVertices && numVertices < maxElements && numElements < maxElements)
            {
                model_vertices[numVertices * 3] = single_vertex[i][0];
                model_vertices[(numVertices * 3) + 1] = single_vertex[i][1];
                model_vertices[(numVertices * 3) + 2] = single_vertex[i][2];
                
                model_normals[numVertices * 3] = single_normal[i][0];
                model_normals[(numVertices * 3) + 1] = single_normal[i][1];
                model_normals[(numVertices * 3) + 2] = single_normal[i][2];
                
                model_textures[numVertices * 2] = single_texture[i][0];
                model_textures[(numVertices * 2) + 1] = single_texture[i][1];
                
                model_elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
                numElements++; //incrementing the 'end' of the list
                numVertices++; //incrementing coun of vertices
            }
    }
}

 Boolean isFoundIdentical( float val1,  float val2,  float diff)
 {
        // code
        //fprintf(gpFile,"Inside isFoundIdentical()  \n");

        if(abs(val1 - val2) < diff)
            return(true);
        else
            return(false);
 }

void normalizeVector(float v[3])
{
        // code
        // fprintf(gpFile,"Inside normalizeVector()  \n");

        // square the vector length
        float squaredVectorLength=(v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
        
        // get square root of above 'squared vector length'
        float squareRootOfSquaredVectorLength=(float)sqrt(squaredVectorLength);
        
        // scale the vector with 1/squareRootOfSquaredVectorLength
        v[0] = v[0] * 1.0f/squareRootOfSquaredVectorLength;
        v[1] = v[1] * 1.0f/squareRootOfSquaredVectorLength;
        v[2] = v[2] * 1.0f/squareRootOfSquaredVectorLength;
}
    

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix=vmath::mat4::identity();
    vmath::mat4 ViewMatrix=vmath::mat4::identity();
    vmath::mat4 translationMatrix=vmath::mat4::identity();
    vmath::mat4 rotationalMatrixred=vmath::mat4::identity();
    vmath::mat4 rotationalMatrixgreen=vmath::mat4::identity();
    vmath::mat4 rotationalMatrixblue=vmath::mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	rotationalMatrixred = vmath::rotate(lightAngleZero, 1.0f, 0.0f, 0.0f);
	rotationalMatrixgreen = vmath::rotate(lightAngleOne, 0.0f, 1.0f, 0.0f);
	rotationalMatrixblue = vmath::rotate(lightAngleTwo, 1.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * translationMatrix;
    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	glUniformMatrix4fv(rotationalMatrixUniformred, 1, GL_FALSE, rotationalMatrixred);
	glUniformMatrix4fv(rotationalMatrixUniformgreen, 1, GL_FALSE, rotationalMatrixgreen);
	glUniformMatrix4fv(rotationalMatrixUniformblue, 1, GL_FALSE, rotationalMatrixblue);

    if (bLight == true)
	{
		glUniform1i(lKeyPressUniform, 1);

		glUniform3fv(lightAmbientUniformRedLight, 1, light[0].Ambient);
		glUniform3fv(lightDiffuseUniformRedLight, 1, light[0].Diffuse);
		glUniform3fv(lightSpecularUniformRedLight, 1, light[0].Specular);
		glUniform4fv(lightPositionUniformRedLight, 1, light[0].Position);


		glUniform3fv(lightAmbientUniformGreenLight, 1, light[1].Ambient);
		glUniform3fv(lightDiffuseUniformGreenLight, 1, light[1].Diffuse);
		glUniform3fv(lightSpecularUniformGreenLight, 1, light[1].Specular);
		glUniform4fv(lightPositionUniformGreenLight, 1, light[1].Position);

		glUniform3fv(lightAmbientUniformBlueLight, 1, light[2].Ambient);
		glUniform3fv(lightDiffuseUniformBlueLight, 1, light[2].Diffuse);
		glUniform3fv(lightSpecularUniformBlueLight, 1, light[2].Specular);
		glUniform4fv(lightPositionUniformBlueLight, 1, light[2].Position);

		glUniform3fv(materialAmbientUniform, 1, materialAmbient);
		glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
		glUniform3fv(materialSpecularUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);

	}
	else
	{
		glUniform1i(lKeyPressUniform, 0);
	}
    //bind vao
    glBindVertexArray(vaoSphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphereIndices);

    //draw eith by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawElements(GL_TRIANGLES,numElements,GL_UNSIGNED_SHORT,0);

    //UNBIND VAO
    glBindVertexArray(0);
    
    //stop using openGL pROGRAM object
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
     update();

    
}

void update(void)
{
	if (lightAngleZero <= 360.0f)
	{
		lightAngleZero = lightAngleZero + 2.5f;
	

		if (lightAngleZero >= 360.0f)
		{
			lightAngleZero = 0.0f;
		}
	}

	if (lightAngleOne <= 360.0f)
	{
		lightAngleOne = lightAngleOne + 2.5f;
		
		if (lightAngleOne >= 360.0f)
		{
			lightAngleOne = 0.0f;
		}
	}

	if (lightAngleTwo <= 360.0f)
	{
		lightAngleTwo = lightAngleTwo + 2.5f;
		
		if (lightAngleTwo >= 360.0f)
		{
			lightAngleTwo = 0.0f;
		}
	}

}
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            [[self window]toggleFullScreen:self];
            break;
            
            case 'L':
            case 'l':
            if (bLight == false)
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
            break;

            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
    if(vaoSphere)
    {
        glDeleteVertexArrays(1, &vaoSphere);
        vaoSphere=0;
    }
    
    if(vboSpherePosition)
    {
        glDeleteBuffers(1, &vboSpherePosition);
        vboSpherePosition=0;
    }

     if(vboSphereNormals)
    {
        glDeleteBuffers(1, &vboSphereNormals);
        vboSphereNormals=0;
    }

     if(vboSphereTexture)
    {
        glDeleteBuffers(1, &vboSphereTexture);
        vboSphereTexture=0;
    }

     if(vboSphereIndices)
    {
        glDeleteBuffers(1, &vboSphereIndices);
        vboSphereIndices=0;
    }
    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
