//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;


enum
{
	AMC_ATRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};


//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);



//global variables
FILE *gpFile=NULL;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,1000.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS StaticIndia"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vaoHorizonatalLine;
    GLuint vaoVerticalLine;
    GLuint vaoLeftSlantLine;
    GLuint vaoRightSlantLine;

    GLuint vaoLeftSlantLineForD;
    GLuint vaoRightSlantLineForD;

    GLuint vboHorizontalLinePosition;
    GLuint vboVerticalLinePosition;
    GLuint vboLeftSlantLinePosition;
    GLuint vboRightSlantLinePosition;
    GLuint vboLeftSlantLinePositionForD;
    GLuint vboRightSlantLinePositionForD;

    GLuint vboHorizontalLineColor;
    GLuint vboLeftSlantLineForDColor;
    GLuint vboRightSlantLineForDColor;

    GLuint vboColor;
    GLuint mvpUniform;

    vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    void initStaticIndiaShaders();
    void initStaticIndiaVaoVbos();
    [self initStaticIndiaShaders];
    [self initStaticIndiaVaoVbos];

    glClearDepth(1.0f);
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    //glEnable(GL_CULL_FACE);
    
    
    //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}

-(void) initStaticIndiaShaders
{
     gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_Color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_Color=vColor;" \
		"}";
    glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
        "out vec4 FragColor;" \
		"in vec4 out_Color;" \
		"void main(void)" \
		"{" \
		"FragColor=out_Color;" \
		"}";
    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
   
}

-(void) initStaticIndiaVaoVbos
{
    //vertices ,colors, shader attribs, vbo, vao initializations
    const GLfloat verticalLine[] =
	{
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};

	const GLfloat horizontalLine[] =
	{
		-0.7f,1.0f,0.0f,
		0.7f,1.0f,0.0f
	};

	const GLfloat leftSlantLine[] =
	{
		-0.5f,1.0f,0.0f,
		0.5f,-1.0f,0.0f
	};

	const GLfloat rightSlantLine[] =
	{
		0.5f,1.0f,0.0f,
		-0.5f,-1.0f,0.0f
	};

	const GLfloat leftSlantLineForD[] =
	{
		-0.5f,1.0f,0.0f,
		0.7f,0.5f,0.0f
	};

	const GLfloat rightSlantLineForD[] =
	{
		0.7f,-0.5f,0.0f,
		-0.5f,-1.0f,0.0f
	};

	const GLfloat color[] =
	{
		1.0f, 0.501f, 0.0f,
		0.0f, 0.501f, 0.0f
	};

	const GLfloat leftSlantLineForDColor[]=
	{
				1.0f, 0.501f, 0.0f,
				1.0f, 0.501f, 0.0f
	};
	
	const GLfloat rightSlantLineForDColor[]=
	{
				0.0f, 0.501f, 0.0f,
				0.0f, 0.501f, 0.0f
	};

    
    //create vao vertical line
	glGenVertexArrays(1, &vaoVerticalLine);
	glBindVertexArray(vaoVerticalLine);

	glGenBuffers(1, &vboVerticalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboVerticalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLine), verticalLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color 
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao horizontal line
	glGenVertexArrays(1, &vaoHorizonatalLine);
	glBindVertexArray(vaoHorizonatalLine);

	glGenBuffers(1, &vboHorizontalLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLine), horizontalLine, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboHorizontalLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);

	glBufferData(GL_ARRAY_BUFFER, 3*2*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	
	glBindVertexArray(0);


	//create vao leftSlantLine
	glGenVertexArrays(1, &vaoLeftSlantLine);
	glBindVertexArray(vaoLeftSlantLine);

	glGenBuffers(1, &vboLeftSlantLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLine), leftSlantLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color 
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao leftSlantLineForD
	glGenVertexArrays(1, &vaoLeftSlantLineForD);
	glBindVertexArray(vaoLeftSlantLineForD);

	glGenBuffers(1, &vboLeftSlantLinePositionForD);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLinePositionForD);
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLineForD), leftSlantLineForD, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboLeftSlantLineForDColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboLeftSlantLineForDColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftSlantLineForDColor),leftSlantLineForDColor , GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//create vao rightSlantLineForD
	glGenVertexArrays(1, &vaoRightSlantLineForD);
	glBindVertexArray(vaoRightSlantLineForD);

	glGenBuffers(1, &vboRightSlantLinePositionForD);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLinePositionForD);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLineForD), rightSlantLineForD, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboRightSlantLineForDColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLineForDColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLineForDColor), rightSlantLineForDColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//create vao for rightSlantLine
	glGenVertexArrays(1, &vaoRightSlantLine);
	glBindVertexArray(vaoRightSlantLine);

	glGenBuffers(1, &vboRightSlantLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboRightSlantLinePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rightSlantLine), rightSlantLine, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);

	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);




}
-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}

-(void)drawView
{
    //code
    void draw();
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    [self draw];
   

    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    
}

-(void) draw
{
     //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    //Set ModelView & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix=vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix=vmath::mat4::identity();
    modelViewMatrix=vmath::translate(-3.0f,0.0f,-6.0f);

    //multiply the modelView matrix and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I

    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    modelViewMatrix=vmath::translate(-3.0f,0.0f,-6.0f);
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glLineWidth(255.0f);
	glBindBuffer( GL_ARRAY_BUFFER, vboHorizontalLineColor);
	 GLfloat colorHorizontalLine[] =
	{
		1.0f, 0.501f, 0.0f,
		1.0f, 0.501f, 0.0f
	};
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


    //horizontal line for I

    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    modelViewMatrix=vmath::translate(-3.0f,-2.0f,-6.0f);
    modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE,modelViewProjectionMatrix );
    
    //bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
		colorHorizontalLine[0] = 0.0f;
		colorHorizontalLine[1] = 0.501f;
		colorHorizontalLine[2] = 0.0f;
		colorHorizontalLine[3] = 0.0f;
		colorHorizontalLine[4] = 0.501f;
		colorHorizontalLine[5] = 0.0f;
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(255.0f);

	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

    //vertical line For N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-1.8f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//leftSlantLine for N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-1.3f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//vertical line For N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-0.8f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//vertical line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(-0.5f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//leftslant line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLineForD);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
	
	//rightSlant line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRightSlantLineForD);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


	//vertical line for D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -12.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//for second I
	//vertical line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoVerticalLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//horizontal line for I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(1.3f, -2.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//right slant line For A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(2.6f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoRightSlantLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//left slant line for A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(3.58f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoLeftSlantLine);
	glLineWidth(255.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line Orange
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.6f, -1.0f, -9.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glLineWidth(255.0f);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;

	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line white
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.1f, -1.1f, -8.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 1.0f;
	colorHorizontalLine[1] = 1.0f;
	colorHorizontalLine[2] = 1.0f;
	colorHorizontalLine[3] = 1.0f;
	colorHorizontalLine[4] = 1.0f;
	colorHorizontalLine[5] = 1.0f;
	glLineWidth(155.0f);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//flag Line Green
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(4.0f, -1.2f, -7.8f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniform
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao
	glBindVertexArray(vaoHorizonatalLine);
	glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalLineColor);
	colorHorizontalLine[0] = 0.0f;
	colorHorizontalLine[1] = 0.501f;
	colorHorizontalLine[2] = 0.0f;
	colorHorizontalLine[3] = 0.0f;
	colorHorizontalLine[4] = 0.501f;
	colorHorizontalLine[5] = 0.0f;
	glLineWidth(155.0f);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorHorizontalLine), colorHorizontalLine, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);





    //stop using openGL pROGRAM object
    glUseProgram(0);
}
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            [[self window]toggleFullScreen:self];
            break;
            
            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
  if (vboColor)
	{
		glDeleteBuffers(1, &vboColor);
		vboColor = 0;
	}
	if (vboHorizontalLineColor)
	{
		glDeleteBuffers(1, &vboHorizontalLineColor);
		vboHorizontalLineColor = 0;
	}
	if (vboHorizontalLinePosition)
	{
		glDeleteBuffers(1, &vboHorizontalLinePosition);
		vboHorizontalLinePosition = 0;
	}

	if (vboVerticalLinePosition)
	{
		glDeleteBuffers(1, &vboVerticalLinePosition);
		vboVerticalLinePosition = 0;
	}

	if (vboLeftSlantLinePosition)
	{
		glDeleteBuffers(1, &vboLeftSlantLinePosition);
		vboLeftSlantLinePosition = 0;
	}

	if (vboLeftSlantLinePositionForD)
	{
		glDeleteBuffers(1, &vboLeftSlantLinePositionForD);
		vboLeftSlantLinePositionForD = 0;
	}

	if (vboRightSlantLinePosition)
	{
		glDeleteBuffers(1, &vboRightSlantLinePosition);
		vboRightSlantLinePosition = 0;
	}

	if (vboRightSlantLinePositionForD)
	{
		glDeleteBuffers(1, &vboRightSlantLinePositionForD);
		vboRightSlantLinePositionForD = 0;
	}

	if (vaoLeftSlantLine)
	{
		glDeleteVertexArrays(1, &vaoLeftSlantLine);
		vaoLeftSlantLine = 0;
	}
	if (vaoLeftSlantLineForD)
	{
		glDeleteVertexArrays(1, &vaoLeftSlantLineForD);
		vaoLeftSlantLineForD = 0;
	}

	if (vaoRightSlantLine)
	{
		glDeleteVertexArrays(1, &vaoRightSlantLine);
		vaoRightSlantLine = 0;
	}

	if (vaoRightSlantLineForD)
	{
		glDeleteVertexArrays(1, &vaoRightSlantLineForD);
		vaoRightSlantLineForD = 0;
	}

	if (vaoHorizonatalLine)
	{
		glDeleteVertexArrays(1, &vaoHorizonatalLine);
		vaoHorizonatalLine = 0;
	}

	if (vaoVerticalLine)
	{
		glDeleteVertexArrays(1, &vaoVerticalLine);
		vaoVerticalLine = 0;
	}

    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
