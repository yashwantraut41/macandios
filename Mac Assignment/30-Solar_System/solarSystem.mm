//
//  AppDelegate.m
//  Basic
//
//  Created by yashwant raut on 06/12/19.
//  Copyright © 2019 Yashwant Raut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>

#import<OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;
#import "createSphere.h"
#import "colorArray.h"
enum
{
    AMC_ATTRIBUTE_VERTEX=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

//c style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef ,const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void *);

GLuint maxElements;
GLuint numElements;
GLuint numVertices;
   
GLfloat model_vertices[1146];
GLfloat model_normals[1146];
GLfloat model_textures[764];
GLushort model_elements[2280];

//global function declarations
void createSphere();
void addTriangle(float single_vertex[3][3] ,float single_normal[3][3], float single_texture[3][2]);
void normalizeVector(float v[3]);
Boolean isFoundIdentical( float val1,  float val2,  float diff);

//for perVertex
 

//global variables
FILE *gpFile=NULL;
int year, day,night;

//interface Declaration
@interface AppDelegate:NSObject<NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//entry point function
int main(int argc,const char *argv[])
{
    //code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//interface implementations
@implementation AppDelegate
{
@private
 NSWindow *window;
 GLView *glView;
}
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    //window
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile==NULL)
    {
        printf("can not create Log File \n Exiting Now ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is Started Succesfully \n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled |
                             NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable|
                                NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
                                defer:NO];

    [window setTitle:@"macOS solar-System"];
    [window center];
    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Succesfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }

}

-(void)windowWillClose:(NSNotification *)notification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    
    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_color;
    GLuint gVbo_sphere_element;

    GLuint gVao_sphere_earth;
    GLuint gVbo_sphere_position_earth;
    GLuint gVbo_sphere_color_earth;
    GLuint gVbo_sphere_element_earth;

    GLuint gVao_sphere_MOON;
    GLuint gVbo_sphere_position_MOON;
    GLuint gVbo_sphere_color_MOON;
    GLuint gVbo_sphere_element_MOON;
   
    GLuint  modelMatrixUniform;
    GLuint  viewMatrixUniform;
    GLuint projectionMatrixUniform;

     vmath::mat4 perspectiveProjectionMatrix;
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify the display ID to associate the GL Context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0};
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease ];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available  Exitting Now \n");;
            [self release];
            [NSApp terminate:self];
            
        }
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; //it automatically releases the older context if present and sets the newer one


        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    //code
    //OpenGL Info
    fprintf(gpFile, "OpenGL Version :%s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    //vertex Shader
    //create shader
    gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
    
    //shader source code
    const GLchar *vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
    "out vec4 outColor;" \
    "uniform mat4 u_model_matrix ;" \
    "uniform mat4 u_view_matrix ;" \
    "uniform mat4 u_projection_matrix ;" \
    "void main(void)" \
    "{" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "outColor = vColor;" \
    "}";
   glShaderSource(gVertexShaderObject, 1, (const GLchar ** )&vertexShaderSourceCode, NULL);
    
    //COMPILE shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfoLog=NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //FRAGMENT SHADER
    //re-initialize
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfoLog=NULL;
    
    //create Shader
    gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    //fragment shader source code
    const GLchar *fragmentShaderSourceCode=
    "#version 410"
    "\n" \
    "in vec4 outColor;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
		"FragColor = outColor;" \
    "}";

    //provide fragment shader source code to fragement shader
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode,NULL);
    
    //compile Shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s \n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
                
            }
        }
    }
    
    //SHADER PROGRAM
    //create
    gShaderProgramObject=glCreateProgram();
    
    //attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    //attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShadeProgramLinkStatus=0;
    //reinitialize
    iInfoLogLength=0;
    szInfoLog=NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShadeProgramLinkStatus);
    if(iShadeProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if(iInfoLogLength>0)
        {
            szInfoLog=(char *)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {   GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n",szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
            
        }
    }
    
    //get mvpUniform location
    modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	
    //create sphere
    createSphere();
    //vertices ,colors, shader attribs, vbo, vao initializations
      

    // Sun Vao Vbo 
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	 //color vbo 
	glGenBuffers(1, &gVbo_sphere_color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphereColor), sphereColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(model_elements), model_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);


	//Earth Vao Vbo
	// vao
	glGenVertexArrays(1, &gVao_sphere_earth);
	glBindVertexArray(gVao_sphere_earth);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position_earth);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position_earth);
	glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color vbo 
	glGenBuffers(1, &gVbo_sphere_color_earth);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_color_earth);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphereColor1), sphereColor1, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element_earth);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_earth);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(model_elements), model_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	// unbind vao
	glBindVertexArray(0);
 
    //MOON 
    //Earth Vao Vbo
	// vao
	glGenVertexArrays(1, &gVao_sphere_MOON);
	glBindVertexArray(gVao_sphere_MOON);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position_MOON);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position_MOON);
	glBufferData(GL_ARRAY_BUFFER, sizeof(model_vertices), model_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//color vbo 
	// glGenBuffers(1, &gVbo_sphere_color_MOON);
	// glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_color_MOON);
	// glBufferData(GL_ARRAY_BUFFER, sizeof(sphereColor1), sphereColor1, GL_STATIC_DRAW);
	// glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	// glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	// glBindBuffer(GL_ARRAY_BUFFER, 0);
    glVertexAttrib3f(AMC_ATTRIBUTE_COLOR,1.0f,1.0f,1.0f);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element_MOON);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_MOON);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(model_elements), model_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //unbind moon vao 
    glBindVertexArray(0);

    glClearDepth(1.0f);
    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    
    //DEPTH test to do following
    glDepthFunc(GL_LEQUAL);
    //we will always cull back faces for better performance
    glEnable(GL_CULL_FACE);
    
     //set background Color
    glClearColor(0.0f, 0.0f, 0.0f,0.0f);
    
    //set projection matrix to identity matrix
    perspectiveProjectionMatrix=vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}


void createSphere()
{
   // fprintf(gpFile,"Inside Create Sphere \n");
    maxElements=0;
    numElements=0;
    numVertices=0;

   
   
   processSphereData();
  
}



void processSphereData()
{
    
    //fprintf(gpFile,"Inside processSphereData()  \n");
     int numIndices = 760;
        maxElements = numIndices * 3;
        
        GLfloat vert[3][3]; // position co-ordinates of ONE triangle
        GLfloat norm[3][3]; // normal co-ordinates of ONE triangle
       GLfloat tex[3][2]; // texture co-ordinates of ONE triangle
        
        for (int i = 0; i < numIndices; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                vert[j][0] = vertices[indices[i][j]][0];
                vert[j][1] = vertices[indices[i][j]][1];
                vert[j][2] = vertices[indices[i][j]][2];
                
                norm[j][0] = normals[indices[i][j + 3]][0];
                norm[j][1] = normals[indices[i][j + 3]][1];
                norm[j][2] = normals[indices[i][j + 3]][2];
                
                tex[j][0] = textures[indices[i][j + 6]][0];
                tex[j][1] = textures[indices[i][j + 6]][1];
            }
            addTriangle(vert, norm, tex);
        }


}


void addTriangle(float single_vertex[3][3] ,float single_normal[3][3], float single_texture[3][2])
{
     //variable declarations
   // fprintf(gpFile,"Inside addTriangle()  \n");

        const float diff = 0.00001f;
        int i, j;
        
        // code
        // normals should be of unit length
        normalizeVector(single_normal[0]);
        normalizeVector(single_normal[1]);
        normalizeVector(single_normal[2]);

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
        {
                   if (isFoundIdentical(model_vertices[j * 3], single_vertex[i][0], diff) &&
                    isFoundIdentical(model_vertices[(j * 3) + 1], single_vertex[i][1], diff) &&
                    isFoundIdentical(model_vertices[(j * 3) + 2], single_vertex[i][2], diff) &&
                    
                    isFoundIdentical(model_normals[j * 3], single_normal[i][0], diff) &&
                    isFoundIdentical(model_normals[(j * 3) + 1], single_normal[i][1], diff) &&
                    isFoundIdentical(model_normals[(j * 3) + 2], single_normal[i][2], diff) &&
                    
                    isFoundIdentical(model_textures[j * 2], single_texture[i][0], diff) &&
                    isFoundIdentical(model_textures[(j * 2) + 1], single_texture[i][1], diff))
                {
                    model_elements[numElements] = (short)j;
                    numElements++;
                    break;
                }
         }

            //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
            if (j == numVertices && numVertices < maxElements && numElements < maxElements)
            {
                model_vertices[numVertices * 3] = single_vertex[i][0];
                model_vertices[(numVertices * 3) + 1] = single_vertex[i][1];
                model_vertices[(numVertices * 3) + 2] = single_vertex[i][2];
                
                model_normals[numVertices * 3] = single_normal[i][0];
                model_normals[(numVertices * 3) + 1] = single_normal[i][1];
                model_normals[(numVertices * 3) + 2] = single_normal[i][2];
                
                model_textures[numVertices * 2] = single_texture[i][0];
                model_textures[(numVertices * 2) + 1] = single_texture[i][1];
                
                model_elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
                numElements++; //incrementing the 'end' of the list
                numVertices++; //incrementing coun of vertices
            }
    }
}

 Boolean isFoundIdentical( float val1,  float val2,  float diff)
 {
        // code
        //fprintf(gpFile,"Inside isFoundIdentical()  \n");

        if(abs(val1 - val2) < diff)
            return(true);
        else
            return(false);
 }

void normalizeVector(float v[3])
{
        // code
        // fprintf(gpFile,"Inside normalizeVector()  \n");

        // square the vector length
        float squaredVectorLength=(v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
        
        // get square root of above 'squared vector length'
        float squareRootOfSquaredVectorLength=(float)sqrt(squaredVectorLength);
        
        // scale the vector with 1/squareRootOfSquaredVectorLength
        v[0] = v[0] * 1.0f/squareRootOfSquaredVectorLength;
        v[1] = v[1] * 1.0f/squareRootOfSquaredVectorLength;
        v[2] = v[2] * 1.0f/squareRootOfSquaredVectorLength;
}
    

-(void)reshape
{
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0, 0, width, height);
   
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,(width/height),0.1f,100.0f);

   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //start Using OpenGL program Object
    glUseProgram(gShaderProgramObject);
    
    

 //draw sun
	mat4 modelMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 viewMatrix;
	mat4 scaleMatrix;
	modelMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
    
	viewMatrix = lookat(vec3(0.0f, 0.0f, 5.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = translate(0.0f, 0.0f, -4.0f);
	scaleMatrix = scale(2.0f, 2.0f, 2.0f);
	modelMatrix = modelMatrix * scaleMatrix;
	
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	glUniformMatrix4fv( viewMatrixUniform, 1, GL_FALSE, viewMatrix);

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//draw earth
	 
	//mat4 modelMatrixEarth  = mat4::identity();
	//mat4 rotationMatrixEarth = mat4::identity();
	//mat4 rotationMatrix1Earth = mat4::identity();
	//mat4 translationMatrixEarth = mat4::identity();


    rotationMatrix = rotate((float)year, 0.0f, 1.0f, 0.0f);
    modelMatrix = modelMatrix * rotationMatrix;

    translationMatrix = translate(2.0f, 0.0f, 0.0f);
    modelMatrix = modelMatrix * translationMatrix ;

    scaleMatrix = scale(0.75f,0.75f,0.75f);
    modelMatrix = modelMatrix * scaleMatrix;



    //rotationMatrix1 = rotate((float)day, 0.0f, 1.0f, 0.0f);
	 

	//modelMatrixEarth = modelMatrixEarth * rotationMatrixEarth * translationMatrixEarth * rotationMatrix1Earth;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere_earth);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_earth);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindVertexArray(0);

    //Draw Moon

    rotationMatrix = rotate((float)day, 0.0f, 1.0f, 0.0f);
    modelMatrix = modelMatrix * rotationMatrix;

    translationMatrix = translate(0.8f,0.0f,0.0f);
    modelMatrix = modelMatrix *  translationMatrix;

    scaleMatrix = scale(0.25f,0.25f,0.25f);
    modelMatrix = modelMatrix *  scaleMatrix;

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere_MOON);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_MOON);
 	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
    //stop using openGL pROGRAM object
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext ]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return (YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
            
            case 'F':
            case 'f':
            [[self window]toggleFullScreen:self];
            break;
            
            case 'Y':
			year = (year + 3) % 360;
			break;

		case 'y':
			year = (year - 3) % 360;
			break;

		case 'D':
			day = (day + 6) % 360;
			break;

		case 'd':
			day = (day - 6) % 360;
			break;


            default:
            break;
            
                
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
        //code
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code
    
    //destroy vao
    if(gVao_sphere)
    {
        glDeleteVertexArrays(1, &gVao_sphere);
        gVao_sphere=0;
    }
    
    if(gVbo_sphere_position)
    {
        glDeleteBuffers(1, &gVbo_sphere_position);
        gVbo_sphere_position=0;
    }


    

     if(gVbo_sphere_element)
    {
        glDeleteBuffers(1, &gVbo_sphere_element);
        gVbo_sphere_element=0;
    }
    
    //detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    //delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject=0;
    
    //delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject=0;
    
    //delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject=0;
    
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return (result);
}
